local frame = CreateFrame("Frame")
local epoch = 0 -- time of the last auto loot

local LOOT_DELAY = 0.3 -- constant interval that prevents rapid looting

-- loots items if auto loot is turned on xor toggle key is pressed
local function LootContents()
  -- slows method calls to once a LOOT_DELAY interval since LOOT_READY event fires twice
  if (GetTime() - epoch >= LOOT_DELAY) then
    epoch = GetTime()
    
    if (GetCVarBool("autoLootDefault") ~= IsModifiedClick("AUTOLOOTTOGGLE")) then -- xor
      for i = GetNumLootItems(), 1, -1 do
        LootSlot(i)
      end
      
      epoch = GetTime() -- update time
    end
  end
end

-- triggering events and actions to fire
frame:RegisterEvent("LOOT_READY")
frame:SetScript("OnEvent", LootContents)