## Interface: 60200
## Title: AutoLootPlus
## Notes: Improves the speed of Blizzard's auto looting.
## Version: 1.1.0
## Author: mjbmitch

AutoLootPlus.lua