local ZygorGuidesViewer=ZygorGuidesViewer
if not ZygorGuidesViewer then return end
if UnitFactionGroup("player")~="Horde" then return end
if ZGV:DoMutex("DailiesHLEGION") then return end

ZygorGuidesViewer.GuideMenuTier = "TRI"

ZygorGuidesViewer:RegisterGuide("Zygor's Horde Event Guides\\Legion\\Pre-Launch Invasion Event",{
	author="support@zygorguides.com",
	condition_suggested="level==100",
	startlevel=100,
	},[[
	step
	label "Choose_Invasion_Zone"
		Open your map:
		|tip Check Eastern Kingdoms and Kalimdor for green portal icons. This is where invasions are taking place currently.
		|tip Click the line below for a zone with an invasion taking place currently.
		_Kalimdor:_
		Azshara |confirm |or |next "Azshara_Invasion"
		Northern Barrens |confirm |or |next "Northern_Barrens_Invasion"
		Tanaris |confirm |or |next "Tanaris_Invasion"
		.
		_Eastern Kingdoms:_
		Dun Morogh |confirm |or |next "Dun_Morogh_Invasion"
		Hillsbrad Foothills |confirm |or |next "Hillsbrad_Foothills_Invasion"
		Westfall |confirm |or |next "Westfall_Invasion"
		|tip You can earn the "Defender of Azeroth: Legion Invasions" achievement by completing an invasion in all 6 zones.

//HILLSBRAD FOOTHILLS INVASION - START
	step
	label "Hillsbrad_Foothills_Invasion"
		Begin the Invasion: Hillsbrad Foothills Scenario |scenariostart |goto Hillsbrad Foothills/0 56.73,46.32
	step
		Kill enemies around this area
		Defend Tarren Mill |scenariostage 1 |goto 56.73,46.32
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 56.73,46.32
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 56.73,46.32
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 56.73,46.32
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 56.73,46.32 |next "Choose_Invasion_Zone"
//HILLSBRAD FOOTHILLS INVASION - END

//TANARIS INVASION - START
	step
	label "Tanaris_Invasion"
		Begin the Invasion: Tanaris Scenario |scenariostart |goto Tanaris/0 51.83,29.18
	step
		Kill enemies around this area
		Defend Gadgetzan |scenariostage 1 |goto 51.83,29.18
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 51.83,29.18
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 51.83,29.18
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 51.83,29.18
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 51.83,29.18 |next "Choose_Invasion_Zone"
//TANARIS INVASION - END

//NORTHERN BARRENS INVASION - START
	step
	label "Northern_Barrens_Invasion"
		Begin the Invasion: Northern Barrens Scenario |scenariostart |goto Northern Barrens/0 49.64,60.30
	step
		Kill enemies around this area
		Defend Crossroads |scenariostage 1 |goto 49.64,60.30
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 49.64,60.30
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 49.64,60.30
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 49.64,60.30
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 49.64,60.30 |next "Choose_Invasion_Zone"
//NORTHERN BARRENS INVASION - END

//AZSHARA INVASION - START
	step
	label "Azshara_Invasion"
		Begin the Invasion: Azshara Scenario |scenariostart |goto Azshara/0 27.02,76.81
	step
		Kill enemies around this area
		Defend Orgrimmar |scenariostage 1 |goto 27.02,76.81
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 27.02,76.81
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 27.02,76.81
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 27.02,76.81
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 27.02,76.81 |next "Choose_Invasion_Zone"
//AZSHARA INVASION - END

//DUN MOROGH INVASION - START
	step
	label "Dun_Morogh_Invasion"
		Begin the Invasion: Dun Morogh Scenario |scenariostart |goto Dun Morogh/0 53.46,50.37
	step
		Kill enemies around this area
		Defend Kharanos |scenariostage 1 |goto 53.46,50.37
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 53.46,50.37
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 53.46,50.37
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 53.46,50.37
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 53.46,50.37 |next "Choose_Invasion_Zone"
//DUN MOROGH INVASION - END

//WESTFALL INVASION - START
	step
	label "Westfall_Invasion"
		Begin the Invasion: Westfall Scenario |scenariostart |goto Westfall/0 56.76,51.07
	step
		Kill enemies around this area
		Defend Sentinel Kill |scenariostage 1 |goto 56.76,51.07
	step
		Kill enemies around this area
		Defeat #3# Doomguards |scenariogoal 2/30993 |goto 56.76,51.07
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 56.76,51.07
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 56.76,51.07
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 56.76,51.07 |next "Choose_Invasion_Zone"
//WESTFALL INVASION - END
]])



ZygorGuidesViewer:RegisterGuide("Zygor's Horde Leveling Guides\\Legion (100-110)\\Legion Intro",{
	author="support@zygorguides.com",
	image=ZGV.DIR.."\\Guides\\Images\\The_Broken_Shore",
	next="path\\guide",
	startlevel=100.0,
	},[[
	step
		accept The Legion Returns##43926 |goto Orgrimmar/1 51.28,76.64
		|tip You will accept the quest automatically.
		|only if not completedq(44663)
	step
		Leave Orgrimmar |goto 49.59,94.67 > 2000
		talk Holgar Stormaxe##4311
		turnin The Legion Returns##43926 |goto Durotar/0 46.00,13.80
		accept To Be Prepared##44281 |goto Durotar/0 46.00,13.80
		|only if not completedq(44663)
	step
		click Ribs
		Eat your Last Meal |q 44281/3 |goto 47.65,13.54
		|only if not completedq(44663)
	step
		click Keg of Armor Polish
		Polish your Armor |q 44281/1 |goto 49.62,14.15
		|only if not completedq(44663)
	step
		click Light-Infused Crystals
		Empower your Weapon |q 44281/2 |goto 51.37,12.28
		|only if not completedq(44663)
	step
		talk Arienne Black##113948
		|tip Fight her.
		Warm Up with a Duel |q 44281/4 |goto 52.82,11.32
		|only if not completedq(44663)
	step
		talk Stone Guard Mukar##113547
		turnin To Be Prepared##44281 |goto 55.63,11.03
		accept The Battle for Broken Shore##40518 |goto 55.63,11.03
		|only if not completedq(44663)
	step
		talk Captain Russo##113118
		Take the Ship to the Broken Shore |q 40518/1 |goto 57.77,10.49
		|only if not completedq(44663)
	step
		Begin the "Battle for Broken Shore" Scenario |scenariostart |q 40518
		|only if not completedq(44663)
	step
		Watch the dialogue
		|tip Ride the boat to shore.
		Travel to the Broken Shore |scenariostage 1 |goto Broken Shore/0 54.17,70.25 |q 40518
		|only if not completedq(44663)
	stickystart "fel_lords1"
	stickystart "spires_of_woe"
	step
		Kill enemies around this area
		|tip Follow the group.
		Kill #33# Demons |scenariogoal 2/27653 |goto 52.46,64.91 |q 40518
		|only if not completedq(44663)
	step
	label "fel_lords1"
		Kill enemies around this area
		|tip Follow the group.
		Kill #3# Fel Lords |scenariogoal 2/29377 |goto 52.46,64.91 |q 40518
		|only if not completedq(44663)
	step
	label "spires_of_woe"
		kill Anchoring Crystal##91704+
		|tip Follow the group.
		Destroy #3# Spires of Woe |scenariogoal 2/27619 |goto 52.46,64.91 |q 40518
		|only if not completedq(44663)
	step
		kill Fel Commander Azgalor##93719 |scenariogoal 3/30883 |goto 52.46,64.91 |q 40518
		|only if not completedq(44663)
	step
		Run up the path |goto 53.36,62.66 < 30
		Watch the dialogue
		|tip Follow the group.
		Find Sylvanas and Baine |scenariostage 4 |goto 53.98,51.09 |q 40518
		|only if not completedq(44663)
	step
		kill Shielded Anchor##101667+
		|tip They look like big floating green crystals.
		Shatter #4# Shielded Anchors |scenariostage 5 |goto 54.43,49.64 |q 40518
		|only if not completedq(44663)
	step
		Watch the dialogue
		|tip Follow the group.
		Kill enemies all around this area
		Assault the Demon City |scenariostage 6 |goto 56.72,50.14 |q 40518
		|only if not completedq(44663)
	step
		Watch the dialogue
		|tip Follow the group.
		Find Tirion |scenariostage 7 |goto 57.18,35.03 |q 40518
		|only if not completedq(44663)
	step
		Kill enemies around this area
		kill Krosus##90544
		Slay Krosus |scenariostage 8 |goto 57.18,35.03 |q 40518
		|only if not completedq(44663)
	step
		Follow the path up |goto 58.08,34.78 < 20
		Follow the path up |goto 64.05,33.37 < 20
		Watch the dialogue
		|tip Follow the group
		Kill the enemies that attack in waves
		Confront Gul'dan |scenarioend |goto 63.31,30.97 |q 40518
		Assault the Broken Shore |q 40518/2 |goto 63.31,30.97
		|only if not completedq(44663)
	step
		talk Eitrigg##100453
		turnin The Battle for Broken Shore##40518 |goto Durotar/0 57.17,10.53
		accept Fate of the Horde##40522 |goto Durotar/0 57.17,10.53
		|only if not completedq(44663)
	step
		Fly into Orgrimmar |goto 48.01,11.63 > 2000
		talk High Overlord Saurfang##100636
		Report to Saurfang |q 40522/1 |goto Orgrimmar/1 50.02,75.96
		|only if not completedq(44663)
	step
		Enter Grommash Hold |q 40522/2 |goto Orgrimmar/1 49.59,74.74
		|only if not completedq(44663)
	step
		Watch the dialogue
		Learn the Fate of the Horde |q 40522/3 |goto 48.35,71.33
		|only if not completedq(44663)
	step
		talk Lady Sylvanas Windrunner##100866
		Pledge to Warchief Sylvanas |q 40522/4 |goto Durotar/0 45.71,15.88
		|only if not completedq(44663)
	step
		talk Lady Sylvanas Windrunner##100866
		turnin Fate of the Horde##40522 |goto 45.71,15.88
		accept Emissary##40760 |goto 45.71,15.88
		|only if not completedq(44663)
	step
		talk Allari the Souleater##100873
		turnin Emissary##40760 |goto 45.82,15.11
		accept Demons Among Us##40607 |goto 45.82,15.11
		|only if not completedq(44663)
	step
		talk Allari the Souleater##100873
		Learn what Allari the Souleater Knows |q 40607/1 |goto 45.82,15.11
		|only if not completedq(44663)
	step
		Kill enemies around this area
		Slay #12# Demons |q 40607/2 |goto 45.94,14.70
		|only if not completedq(44663)
	step
		talk Lady Sylvanas Windrunner##101035
		turnin Demons Among Us##40607 |goto 45.68,15.92
		accept Keep Your Friends Close##40605 |goto 45.68,15.92
		|only if not completedq(44663)
	step
		talk Elthyn Da'rai##95234
		|tip Up on the platform near the flight path, she walks around.
		turnin Keep Your Friends Close##40605 |goto Orgrimmar/1 52.91,56.51
		|only if not completedq(44663)
	step
		Congratulations!  You completed the Legion intro.
		|tip See you for more on Augusth 30th!
		.
		|tip Be ready to take on the Burning Legion with Zygor Elite!
]])