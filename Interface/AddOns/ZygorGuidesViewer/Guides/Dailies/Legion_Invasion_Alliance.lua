local ZygorGuidesViewer=ZygorGuidesViewer
if not ZygorGuidesViewer then return end
if UnitFactionGroup("player")~="Alliance" then return end
if ZGV:DoMutex("DailiesALEGION") then return end

ZygorGuidesViewer.GuideMenuTier = "TRI"

ZygorGuidesViewer:RegisterGuide("Zygor's Alliance Event Guides\\Legion\\Pre-Launch Invasion Event",{
	author="support@zygorguides.com",
	condition_suggested="level==100",
	startlevel=100,
	},[[
	step
	label "Choose_Invasion_Zone"
		Open your map:
		|tip Check Eastern Kingdoms and Kalimdor for green portal icons. This is where invasions are taking place currently.
		|tip Click the line below for a zone with an invasion taking place currently.
		_Kalimdor:_
		Azshara |confirm |or |next "Azshara_Invasion"
		Northern Barrens |confirm |or |next "Northern_Barrens_Invasion"
		Tanaris |confirm |or |next "Tanaris_Invasion"
		.
		_Eastern Kingdoms:_
		Dun Morogh |confirm |or |next "Dun_Morogh_Invasion"
		Hillsbrad Foothills |confirm |or |next "Hillsbrad_Foothills_Invasion"
		Westfall |confirm |or |next "Westfall_Invasion"
		|tip You can earn the "Defender of Azeroth: Legion Invasions" achievement by completing an invasion in all 6 zones.

//HILLSBRAD FOOTHILLS INVASION - START
	step
	label "Hillsbrad_Foothills_Invasion"
		Begin the Invasion: Hillsbrad Foothills Scenario |scenariostart |goto Hillsbrad Foothills/0 56.73,46.32
	step
		Kill enemies around this area
		Defend Tarren Mill |scenariostage 1 |goto 56.73,46.32
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 56.73,46.32
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 56.73,46.32
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 56.73,46.32
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 56.73,46.32 |next "Choose_Invasion_Zone"
//HILLSBRAD FOOTHILLS INVASION - END

//TANARIS INVASION - START
	step
	label "Tanaris_Invasion"
		Begin the Invasion: Tanaris Scenario |scenariostart |goto Tanaris/0 51.83,29.18
	step
		Kill enemies around this area
		Defend Gadgetzan |scenariostage 1 |goto 51.83,29.18
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 51.83,29.18
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 51.83,29.18
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 51.83,29.18
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 51.83,29.18 |next "Choose_Invasion_Zone"
//TANARIS INVASION - END

//NORTHERN BARRENS INVASION - START
	step
	label "Northern_Barrens_Invasion"
		Begin the Invasion: Northern Barrens Scenario |scenariostart |goto Northern Barrens/0 49.64,60.30
	step
		Kill enemies around this area
		Defend Crossroads |scenariostage 1 |goto 49.64,60.30
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 49.64,60.30
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 49.64,60.30
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 49.64,60.30
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 49.64,60.30 |next "Choose_Invasion_Zone"
//NORTHERN BARRENS INVASION - END

//AZSHARA INVASION - START
	step
	label "Azshara_Invasion"
		Begin the Invasion: Azshara Scenario |scenariostart |goto Azshara/0 27.02,76.81
	step
		Kill enemies around this area
		Defend Orgrimmar |scenariostage 1 |goto 27.02,76.81
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 27.02,76.81
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 27.02,76.81
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 27.02,76.81
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 27.02,76.81 |next "Choose_Invasion_Zone"
//AZSHARA INVASION - END

//DUN MOROGH INVASION - START
	step
	label "Dun_Morogh_Invasion"
		Begin the Invasion: Dun Morogh Scenario |scenariostart |goto Dun Morogh/0 53.46,50.37
	step
		Kill enemies around this area
		Defend Kharanos |scenariostage 1 |goto 53.46,50.37
	step
		Kill enemies around this area
		Slay #2# Demon Lieutenants |scenariogoal 2/30993 |goto 53.46,50.37
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 53.46,50.37
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 53.46,50.37
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 53.46,50.37 |next "Choose_Invasion_Zone"
//DUN MOROGH INVASION - END

//WESTFALL INVASION - START
	step
	label "Westfall_Invasion"
		Begin the Invasion: Westfall Scenario |scenariostart |goto Westfall/0 56.76,51.07
	step
		Kill enemies around this area
		Defend Sentinel Kill |scenariostage 1 |goto 56.76,51.07
	step
		Kill enemies around this area
		Defeat #3# Doomguards |scenariogoal 2/30993 |goto 56.76,51.07
	step
		Kill enemies around this area
		Defeat the Demon Commander |scenariostage 2 |goto 56.76,51.07
	step
		Kill enemies around this whole zone
		|tip Open your map and go to the map markers and kill enemies at those locations.
		Repel the Legion Forces |scenariostage 3 |goto 56.76,51.07
	step
		Kill enemies around this area
		|tip Make sure to open the Small Legion Chest and Large Legion Chest in your inventory after you complete the scenario.
		Defeat the Demon Lord |scenarioend |goto 56.76,51.07 |next "Choose_Invasion_Zone"
//WESTFALL INVASION - END
]])



ZygorGuidesViewer:RegisterGuide("Zygor's Alliance Leveling Guides\\Legion (100-110)\\Legion Intro",{
	author="support@zygorguides.com",
	image=ZGV.DIR.."\\Guides\\Images\\The_Broken_Shore",
	next="path\\guide",
	startlevel=100.0,
	},[[
	step
		accept The Legion Returns##40519 |goto Stormwind City/0 62.9,71.6
		|tip You will accept the quest automatically.
		|only if not completedq(44663)
	step
		talk Recruiter Lee##107934
		turnin The Legion Returns##40519 |goto 37.1,43.1
		accept To Be Prepared##42782 |goto 37.1,43.1
		|only if not completedq(44663)
	step
		click Keg of Armor Polish
		Polish your Armor |q 42782/1 |goto 29.8,42.9
		|only if not completedq(44663)
	step
		click Light-Infused Crystals
		Empower your Weapon |q 42782/2 |goto 30.0,34.1
		|only if not completedq(44663)
	step
		click Ribs
		Eat your Last Meal |q 42782/3 |goto 29.7,29.2
		|only if not completedq(44663)
	step
		talk Eunna Young##108750
		|tip Fight her.
		Warm Up with a Duel |q 42782/4 |goto 32.8,33.1
		|only if not completedq(44663)
	step
		talk Knight Dameron##108916
		turnin To Be Prepared##42782 |goto 19.0,26.5	
		accept The Battle for Broken Shore##42740 |goto 19.0,26.5
		|only if not completedq(44663)
	step
		talk Captain Angelica##108920
		Take the Ship to the Broken Shore |q 42740/1 |goto 19.9,29.4
		|only if not completedq(44663)
	step
		Begin the "Battle for Broken Shore" Scenario |scenariostart |q 42740
		|only if not completedq(44663)
	step
		Watch the dialogue
		|tip Ride the boat to shore.
		Assault the Broken Shore |scenariostage 1 |goto Broken Shore/0 49.36,71.79 |q 42740
		|only if not completedq(44663)
	stickystart "fel_lords1"
	stickystart "spires_of_woe"
	step
		Kill enemies around this area
		|tip Follow the group.
		Kill #33# Demons |scenariogoal 2/27653 |goto 48.4,70.5 |q 42740
		|only if not completedq(44663)
	step
	label "fel_lords1"
		Kill enemies around this area
		|tip Follow the group.
		Kill #3# Fel Lords |scenariogoal 2/29377 |goto 48.4,70.5 |q 42740
		|only if not completedq(44663)
	step
	label "spires_of_woe"
		kill Anchoring Crystal##91704+
		|tip Follow the group.
		Destroy #3# Spires of Woe |scenariogoal 2/27619 |goto 48.4,70.5 |q 42740
		|only if not completedq(44663)
	step
		kill Dread Commander Arganoth##90705 |goto 48.4,70.5 |scenariogoal 3/30883 |q 42740
		|only if not completedq(44663)
	step
		Run up the path |goto 49.40,66.41 < 30
		Watch the dialogue
		|tip Follow the group.
		Find Varian |scenariostage 4 |goto 38.07,47.13 |q 42740
		|only if not completedq(44663)
	step
		kill Shielded Anchor##101667+
		|tip They look like big floating green crystals.
		Shatter 4 Shielded Anchors |scenariostage 5 |goto 39.0,43.7 |q 42740
		|only if not completedq(44663)
	step
		Watch the dialogue
		|tip Follow the group.
		Kill enemies all around this area
		Assault the Demon City |scenariostage 6 |goto 44.7,47.5 |q 42740
		|only if not completedq(44663)
	step
		Watch the dialogue
		|tip Follow the group.
		Get to Tirion |scenariostage 7 |goto 55.8,32.2 |q 42740
		|only if not completedq(44663)
	step
		Kill enemies around this area
		kill Krosus##90544
		Slay Krosus |scenariostage 8 |goto 55.8,32.2 |q 42740
		|only if not completedq(44663)
	step
		Follow the path up |goto 55.98,31.21 < 20
		Watch the dialogue
		|tip Follow the group
		Kill the enemies that attack in waves
		Confront Gul'dan |scenarioend |goto 58.3,27.5 |q 42740
		Assault the Broken Shore |q 42740/2 |goto 58.3,27.5
		|only if not completedq(44663)
	step
		talk Genn Greymane##100395
		turnin The Battle for Broken Shore##42740 |goto Stormwind City/0 20.1,34.9
		accept The Fallen Lion##40517 |goto Stormwind City/0 20.1,34.9
		|only if not completedq(44663)
	step
		click Gilnean Gryphon
		Ride a Gryphon to Stormwind Keep |q 40517/1 |goto 21.3,33.7
		|only if not completedq(44663)
	step
		talk Anduin Wrynn##100429
		Watch the dialogue
		Deliver Varian's Letter and Listen to King Anduin |q 40517/2 |goto 85.9,31.6
		|only if not completedq(44663)
	step
		talk Anduin Wrynn##100429
		turnin The Fallen Lion##40517 |goto 85.9,31.6
		|only if not completedq(44663)
	step
		talk Jace Darkweaver##100675
		accept Demons Among Us##40593 |goto 85.3,32.3
		|only if not completedq(44663)
	step
		talk Jace Darkweaver##100675
		Watch the dialogue
		Learn what Jace Darkwhisper Knows about Demons |q 40593/1 |goto 85.3,32.3
		|only if not completedq(44663)
	stickystart "Kill_Infiltrators"
	step
		click Demon Portal
		Destroy the Courtyard Legion Portal |q 40593/3 |goto 82.5,27.9
		|only if not completedq(44663)
	step
		Follow the path |goto 83.94,30.69 < 15
		Go through the doorway |goto 81.55,35.60 < 15
		click Demon Portal
		Destroy the Courtyard Legion Portal |q 40593/4 |goto 80.6,33.1
		|only if not completedq(44663)
	step
	label "Kill_Infiltrators"
		Kill enemies around this area
		Kill #5# Infiltrators |q 40593/2 |goto 84.8,32.5
		|only if not completedq(44663)
	step
		talk Anduin Wrynn##100973
		turnin Demons Among Us##40593 |goto 85.8,31.8
		accept Illidari Allies##44120 |goto 85.8,31.8
		|only if not completedq(44663)
	step
		talk Elerion Bladedancer##101004
		|tip On the balcony of the building.
		turnin Illidari Allies##44120 |goto 40.3,77.7
		|only if not completedq(44663)
	step
		Congratulations!  You completed the Legion intro.
		|tip See you for more on Augusth 30th!
		.
		|tip Be ready to take on the Burning Legion with Zygor Elite!
]])