
RecountPerCharDB = {
	["version"] = "1.3",
	["combatants"] = {
		["Falridan"] = {
			["DeathLogs"] = {
				{
					["MessageIncoming"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						true, -- [6]
						true, -- [7]
						true, -- [8]
						true, -- [9]
						true, -- [10]
						true, -- [11]
						true, -- [12]
					},
					["Messages"] = {
						"Falridan Consume Soul Falridan Hit +75870 (75870 overheal)", -- [1]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -18020 (Shadow)", -- [2]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -22003 (Shadow)", -- [3]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -25891 (Shadow)", -- [4]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -28665 (Shadow)", -- [5]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -33950 (Shadow)", -- [6]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -36462 (Shadow)", -- [7]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -40339 (Shadow)", -- [8]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -43502 (Shadow)", -- [9]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -48356 (Shadow)", -- [10]
						"Darkmagus Falo'reth Eye of Darkness Falridan Hit -50457 (Shadow)", -- [11]
						"Falridan dies.", -- [12]
					},
					["DeathAt"] = 119908.544,
					["HealthNum"] = {
						100, -- [1]
						94.06221167787, -- [2]
						86.8119810201661, -- [3]
						78.2806115724265, -- [4]
						68.8351785949651, -- [5]
						57.6482799525504, -- [6]
						45.6336496638988, -- [7]
						32.3415052062739, -- [8]
						18.0071174377224, -- [9]
						2.07328324766047, -- [10]
						0.000329511005667589, -- [11]
						0, -- [12]
					},
					["MessageTimes"] = {
						-12.8960000000079, -- [1]
						-4.54499999999825, -- [2]
						-4.03100000000268, -- [3]
						-3.50299999999697, -- [4]
						-2.98700000000827, -- [5]
						-2.47200000000885, -- [6]
						-1.96600000000035, -- [7]
						-1.52999999999884, -- [8]
						-1.02900000000955, -- [9]
						-0.513000000006286, -- [10]
						0, -- [11]
						0, -- [12]
					},
					["KilledBy"] = "Darkmagus Falo'reth",
					["Health"] = {
						"303480 (100%)", -- [1]
						"285460 (94%)", -- [2]
						"263457 (86%)", -- [3]
						"237566 (78%)", -- [4]
						"208901 (68%)", -- [5]
						"174951 (57%)", -- [6]
						"138489 (45%)", -- [7]
						"98150 (32%)", -- [8]
						"54648 (18%)", -- [9]
						"6292 (2%)", -- [10]
						"1 (0%)", -- [11]
						"0 (0%)", -- [12]
					},
					["EventNum"] = {
						25, -- [1]
						5.93778832212996, -- [2]
						7.25023065770397, -- [3]
						8.53136944773955, -- [4]
						9.44543297746145, -- [5]
						11.1868986424147, -- [6]
						12.0146302886516, -- [7]
						13.2921444576249, -- [8]
						14.3343877685515, -- [9]
						15.933834190062, -- [10]
						16.6261368129696, -- [11]
						0, -- [12]
					},
					["MessageType"] = {
						"HEAL", -- [1]
						"DAMAGE", -- [2]
						"DAMAGE", -- [3]
						"DAMAGE", -- [4]
						"DAMAGE", -- [5]
						"DAMAGE", -- [6]
						"DAMAGE", -- [7]
						"DAMAGE", -- [8]
						"DAMAGE", -- [9]
						"DAMAGE", -- [10]
						"DAMAGE", -- [11]
						"MISC", -- [12]
					},
				}, -- [1]
				{
					["MessageIncoming"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						true, -- [6]
					},
					["Messages"] = {
						"Fel Screecher Melee Falridan Hit -7868 (Physical)", -- [1]
						"Environment Falling Falridan Hit -177764 (Physical)", -- [2]
						"Hillarytrump-Archimonde Auto Shot Falridan Hit -4238 (Physical)", -- [3]
						"Hillarytrump-Archimonde Cobra Shot Falridan Hit -11284 (Physical)", -- [4]
						"Hillarytrump-Archimonde Cobra Shot Falridan Hit -10126 (Physical)", -- [5]
						"Falridan dies.", -- [6]
					},
					["DeathAt"] = 119353.531,
					["HealthNum"] = {
						67.8357717147753, -- [1]
						6.66798471068934, -- [2]
						5.27151706867009, -- [3]
						1.55331488071702, -- [4]
						0.000329511005667589, -- [5]
						0, -- [6]
					},
					["MessageTimes"] = {
						-6.06899999998859, -- [1]
						-3.74899999999616, -- [2]
						-1.25299999999697, -- [3]
						-0.943999999988591, -- [4]
						0, -- [5]
						0, -- [6]
					},
					["KilledBy"] = "Hillarytrump-Archimonde",
					["Health"] = {
						"205868 (67%)", -- [1]
						"20236 (6%)", -- [2]
						"15998 (5%)", -- [3]
						"4714 (1%)", -- [4]
						"1 (0%)", -- [5]
						"0 (0%)", -- [6]
					},
					["EventNum"] = {
						2.59259259259259, -- [1]
						58.5751944114934, -- [2]
						1.39646764201924, -- [3]
						3.71820218795308, -- [4]
						3.33662844339001, -- [5]
						0, -- [6]
					},
					["MessageType"] = {
						"DAMAGE", -- [1]
						"DAMAGE", -- [2]
						"DAMAGE", -- [3]
						"DAMAGE", -- [4]
						"DAMAGE", -- [5]
						"MISC", -- [6]
					},
				}, -- [2]
				{
					["MessageIncoming"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						true, -- [6]
						true, -- [7]
						true, -- [8]
						true, -- [9]
						true, -- [10]
						true, -- [11]
						true, -- [12]
						true, -- [13]
						true, -- [14]
						true, -- [15]
						true, -- [16]
						true, -- [17]
						true, -- [18]
						true, -- [19]
						true, -- [20]
						true, -- [21]
						true, -- [22]
						true, -- [23]
						true, -- [24]
						true, -- [25]
						true, -- [26]
						true, -- [27]
						true, -- [28]
						true, -- [29]
						true, -- [30]
					},
					["Messages"] = {
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -846 (Shadow)", -- [1]
						"Bogyshu-Illidan Immolate Falridan Hit -11115 (Fire)", -- [2]
						"Bogyshu-Illidan Conflagrate Falridan Hit -14392 (Fire)", -- [3]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -1269 (Shadow)", -- [4]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5850 (Shadow)", -- [5]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [6]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -5257 (Fire)", -- [7]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -1692 (Shadow)", -- [8]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [9]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5849 (Shadow)", -- [10]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2115 (Shadow)", -- [11]
						"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Tick -9360 (Shadow)", -- [12]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -6907 (Fire)", -- [13]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [14]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2538 (Shadow)", -- [15]
						"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Tick -9360 (Shadow)", -- [16]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5849 (Shadow)", -- [17]
						"Creepyhollow-BleedingHollow Shadowstrike Falridan Crit -19120 (Physical)", -- [18]
						"Creepyhollow-BleedingHollow Melee Falridan Hit -2135 (Physical)", -- [19]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [20]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -4501 (Fire)", -- [21]
						"Creepyhollow-BleedingHollow Melee Falridan Hit -1104 (Physical)", -- [22]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2960 (Shadow)", -- [23]
						"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Tick -9360 (Shadow)", -- [24]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4826 (Shadow)", -- [25]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -6434 (Shadow)", -- [26]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -3722 (Shadow)", -- [27]
						"Creepyhollow-BleedingHollow Melee Falridan Miss (1)", -- [28]
						"Creepyhollow-BleedingHollow Shadowstrike Falridan Crit -19953 (Physical)", -- [29]
						"Falridan dies.", -- [30]
					},
					["DeathAt"] = 119209.53,
					["HealthNum"] = {
						41.9184130749967, -- [1]
						41.9184130749967, -- [2]
						41.9184130749967, -- [3]
						41.9184130749967, -- [4]
						39.9907736918413, -- [5]
						38.5452089099776, -- [6]
						36.8129695531831, -- [7]
						36.2554369315935, -- [8]
						34.8098721497298, -- [9]
						32.8825622775801, -- [10]
						32.1856465005931, -- [11]
						29.1014234875445, -- [12]
						26.8254909713984, -- [13]
						25.3799261895347, -- [14]
						24.5436272571504, -- [15]
						21.4594042441018, -- [16]
						19.532094371952, -- [17]
						13.2318439435877, -- [18]
						12.5283379464874, -- [19]
						11.0827731646237, -- [20]
						9.59964412811388, -- [21]
						9.59964412811388, -- [22]
						8.2605114010808, -- [23]
						5.17628838803216, -- [24]
						3.58606827468037, -- [25]
						1.4659944642151, -- [26]
						0.239554501120337, -- [27]
						0.239554501120337, -- [28]
						0.000329511005667589, -- [29]
						0, -- [30]
					},
					["MessageTimes"] = {
						-8.51699999999255, -- [1]
						-8.51699999999255, -- [2]
						-8.51699999999255, -- [3]
						-8.51699999999255, -- [4]
						-8.11699999999837, -- [5]
						-7.70100000000093, -- [6]
						-7.18600000000151, -- [7]
						-6.87199999998848, -- [8]
						-6.04499999999825, -- [9]
						-5.63799999999174, -- [10]
						-5.11800000000221, -- [11]
						-4.97400000000198, -- [12]
						-4.78800000000047, -- [13]
						-4.26200000000245, -- [14]
						-3.45100000000093, -- [15]
						-3.25099999998929, -- [16]
						-3.00599999999395, -- [17]
						-2.90999999998894, -- [18]
						-2.90999999998894, -- [19]
						-2.59900000000198, -- [20]
						-2.28800000000047, -- [21]
						-2.15200000000186, -- [22]
						-1.7219999999943, -- [23]
						-1.5, -- [24]
						-0.85899999999674, -- [25]
						-0.440000000002328, -- [26]
						0, -- [27]
						0, -- [28]
						0, -- [29]
						0, -- [30]
					},
					["KilledBy"] = "Creepyhollow-BleedingHollow",
					["Health"] = {
						"127214 (41%)", -- [1]
						"127214 (41%)", -- [2]
						"127214 (41%)", -- [3]
						"127214 (41%)", -- [4]
						"121364 (39%)", -- [5]
						"116977 (38%)", -- [6]
						"111720 (36%)", -- [7]
						"110028 (36%)", -- [8]
						"105641 (34%)", -- [9]
						"99792 (32%)", -- [10]
						"97677 (32%)", -- [11]
						"88317 (29%)", -- [12]
						"81410 (26%)", -- [13]
						"77023 (25%)", -- [14]
						"74485 (24%)", -- [15]
						"65125 (21%)", -- [16]
						"59276 (19%)", -- [17]
						"40156 (13%)", -- [18]
						"38021 (12%)", -- [19]
						"33634 (11%)", -- [20]
						"29133 (9%)", -- [21]
						"29133 (9%)", -- [22]
						"25069 (8%)", -- [23]
						"15709 (5%)", -- [24]
						"10883 (3%)", -- [25]
						"4449 (1%)", -- [26]
						"727 (0%)", -- [27]
						"727 (0%)", -- [28]
						"1 (0%)", -- [29]
						"0 (0%)", -- [30]
					},
					["EventNum"] = {
						0.278766310794781, -- [1]
						3.66251482799526, -- [2]
						4.74232239356795, -- [3]
						0.418149466192171, -- [4]
						1.9276393831554, -- [5]
						1.44556478186371, -- [6]
						1.73223935679452, -- [7]
						0.557532621589561, -- [8]
						1.44556478186371, -- [9]
						1.92730987214973, -- [10]
						0.696915776986951, -- [11]
						3.08422301304864, -- [12]
						2.27593251614604, -- [13]
						1.44556478186371, -- [14]
						0.836298932384342, -- [15]
						3.08422301304864, -- [16]
						1.92730987214973, -- [17]
						6.30025042836431, -- [18]
						0.703505997100303, -- [19]
						1.44556478186371, -- [20]
						1.48312903650982, -- [21]
						0.363780150257019, -- [22]
						0.975352576776064, -- [23]
						3.08422301304864, -- [24]
						1.59022011335179, -- [25]
						2.12007381046527, -- [26]
						1.22643996309477, -- [27]
						0, -- [28]
						6.57473309608541, -- [29]
						0, -- [30]
					},
					["MessageType"] = {
						"DAMAGE", -- [1]
						"DAMAGE", -- [2]
						"DAMAGE", -- [3]
						"DAMAGE", -- [4]
						"DAMAGE", -- [5]
						"DAMAGE", -- [6]
						"DAMAGE", -- [7]
						"DAMAGE", -- [8]
						"DAMAGE", -- [9]
						"DAMAGE", -- [10]
						"DAMAGE", -- [11]
						"DAMAGE", -- [12]
						"DAMAGE", -- [13]
						"DAMAGE", -- [14]
						"DAMAGE", -- [15]
						"DAMAGE", -- [16]
						"DAMAGE", -- [17]
						"DAMAGE", -- [18]
						"DAMAGE", -- [19]
						"DAMAGE", -- [20]
						"DAMAGE", -- [21]
						"DAMAGE", -- [22]
						"DAMAGE", -- [23]
						"DAMAGE", -- [24]
						"DAMAGE", -- [25]
						"DAMAGE", -- [26]
						"DAMAGE", -- [27]
						"DAMAGE", -- [28]
						"DAMAGE", -- [29]
						"MISC", -- [30]
					},
				}, -- [3]
				{
					["MessageIncoming"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						true, -- [6]
						true, -- [7]
						true, -- [8]
						true, -- [9]
						true, -- [10]
						true, -- [11]
						true, -- [12]
						true, -- [13]
						true, -- [14]
						true, -- [15]
						true, -- [16]
						true, -- [17]
						true, -- [18]
						true, -- [19]
						true, -- [20]
						true, -- [21]
						true, -- [22]
						true, -- [23]
						true, -- [24]
						true, -- [25]
						true, -- [26]
						true, -- [27]
						true, -- [28]
						true, -- [29]
						true, -- [30]
						true, -- [31]
						true, -- [32]
						true, -- [33]
						true, -- [34]
						true, -- [35]
						true, -- [36]
						true, -- [37]
						true, -- [38]
						true, -- [39]
						true, -- [40]
					},
					["Messages"] = {
						"Bogyshu-Illidan Immolate (DoT) Falridan Crit -10903 (Fire)", -- [1]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [2]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5850 (Shadow)", -- [3]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2115 (Shadow)", -- [4]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4388 (Shadow)", -- [5]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -5657 (Fire)", -- [6]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2538 (Shadow)", -- [7]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5850 (Shadow)", -- [8]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [9]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Crit -5075 (Shadow)", -- [10]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -7330 (Fire)", -- [11]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [12]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5849 (Shadow)", -- [13]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Crit -6344 (Shadow)", -- [14]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [15]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -6398 (Fire)", -- [16]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -4653 (Shadow)", -- [17]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5849 (Shadow)", -- [18]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [19]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -5075 (Shadow)", -- [20]
						"Bogyshu-Illidan Immolate (DoT) Falridan Crit -8789 (Fire)", -- [21]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [22]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5850 (Shadow)", -- [23]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Crit -8248 (Shadow)", -- [24]
						"Slipperyass-Mal'Ganis Drain Life (DoT) Falridan Tick -6302 (Shadow)", -- [25]
						"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Tick -9359 (Shadow)", -- [26]
						"Bogyshu-Illidan Conflagrate Falridan Crit -29366 (Fire)", -- [27]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [28]
						"Slipperyass-Mal'Ganis Drain Life (DoT) Falridan Tick -6302 (Shadow)", -- [29]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -4732 (Fire)", -- [30]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -5922 (Shadow)", -- [31]
						"Slipperyass-Mal'Ganis Drain Life (DoT) Falridan Tick -6302 (Shadow)", -- [32]
						"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5849 (Shadow)", -- [33]
						"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [34]
						"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Crit -14039 (Shadow)", -- [35]
						"Slipperyass-Mal'Ganis Drain Life (DoT) Falridan Tick -6302 (Shadow)", -- [36]
						"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -6344 (Shadow)", -- [37]
						"Slipperyass-Mal'Ganis Drain Life (DoT) Falridan Tick -6303 (Shadow)", -- [38]
						"Bogyshu-Illidan Immolate (DoT) Falridan Tick -4601 (Fire)", -- [39]
						"Falridan dies.", -- [40]
					},
					["DeathAt"] = 119081.524,
					["HealthNum"] = {
						77.5988533017003, -- [1]
						76.1532885198366, -- [2]
						75.2405430341374, -- [3]
						74.5436272571504, -- [4]
						73.097732964281, -- [5]
						72.251219190721, -- [6]
						71.4149202583366, -- [7]
						69.4872808751812, -- [8]
						68.0417160933175, -- [9]
						66.3694477395545, -- [10]
						63.9541320680111, -- [11]
						62.5085672861474, -- [12]
						60.5812574139976, -- [13]
						58.4908395940424, -- [14]
						57.0452748121787, -- [15]
						54.9370633979175, -- [16]
						53.4038486885462, -- [17]
						51.4765388163965, -- [18]
						50.0309740345328, -- [19]
						48.3587056807697, -- [20]
						45.4626334519573, -- [21]
						44.0170686700936, -- [22]
						42.0894292869382, -- [23]
						39.3716225121919, -- [24]
						37.2950441544748, -- [25]
						34.2111506524318, -- [26]
						24.5347304599974, -- [27]
						23.0891656781337, -- [28]
						21.0125873204165, -- [29]
						19.4533412415975, -- [30]
						17.501977066034, -- [31]
						15.4253987083169, -- [32]
						13.4980888361671, -- [33]
						12.0525240543034, -- [34]
						7.42651904573613, -- [35]
						5.34994068801898, -- [36]
						3.25952286806379, -- [37]
						1.18261499934098, -- [38]
						0.000329511005667589, -- [39]
						0, -- [40]
					},
					["MessageTimes"] = {
						-14.7829999999958, -- [1]
						-14.6849999999977, -- [2]
						-14.3679999999877, -- [3]
						-13.9239999999991, -- [4]
						-13.0169999999926, -- [5]
						-12.2559999999939, -- [6]
						-12.153999999995, -- [7]
						-11.7099999999919, -- [8]
						-11.2920000000013, -- [9]
						-10.4529999999941, -- [10]
						-9.81499999998778, -- [11]
						-9.59899999998743, -- [12]
						-9.18799999999465, -- [13]
						-8.76799999999639, -- [14]
						-7.86000000000058, -- [15]
						-7.33799999998882, -- [16]
						-7.04799999999523, -- [17]
						-6.61799999998766, -- [18]
						-6.12299999999232, -- [19]
						-5.38799999999173, -- [20]
						-4.96399999999267, -- [21]
						-4.45799999999872, -- [22]
						-4.05899999999383, -- [23]
						-3.64800000000105, -- [24]
						-3.5399999999936, -- [25]
						-2.90199999998731, -- [26]
						-2.79999999998836, -- [27]
						-2.69099999999162, -- [28]
						-2.69099999999162, -- [29]
						-2.50199999999313, -- [30]
						-1.93899999999849, -- [31]
						-1.81899999998859, -- [32]
						-1.52499999999418, -- [33]
						-1.13199999999779, -- [34]
						-1.13199999999779, -- [35]
						-1.03199999999197, -- [36]
						-0.414999999993597, -- [37]
						-0.197999999989406, -- [38]
						0, -- [39]
						0, -- [40]
					},
					["KilledBy"] = "Bogyshu-Illidan",
					["Health"] = {
						"235497 (77%)", -- [1]
						"231110 (76%)", -- [2]
						"228340 (75%)", -- [3]
						"226225 (74%)", -- [4]
						"221837 (73%)", -- [5]
						"219268 (72%)", -- [6]
						"216730 (71%)", -- [7]
						"210880 (69%)", -- [8]
						"206493 (68%)", -- [9]
						"201418 (66%)", -- [10]
						"194088 (63%)", -- [11]
						"189701 (62%)", -- [12]
						"183852 (60%)", -- [13]
						"177508 (58%)", -- [14]
						"173121 (57%)", -- [15]
						"166723 (54%)", -- [16]
						"162070 (53%)", -- [17]
						"156221 (51%)", -- [18]
						"151834 (50%)", -- [19]
						"146759 (48%)", -- [20]
						"137970 (45%)", -- [21]
						"133583 (44%)", -- [22]
						"127733 (42%)", -- [23]
						"119485 (39%)", -- [24]
						"113183 (37%)", -- [25]
						"103824 (34%)", -- [26]
						"74458 (24%)", -- [27]
						"70071 (23%)", -- [28]
						"63769 (21%)", -- [29]
						"59037 (19%)", -- [30]
						"53115 (17%)", -- [31]
						"46813 (15%)", -- [32]
						"40964 (13%)", -- [33]
						"36577 (12%)", -- [34]
						"22538 (7%)", -- [35]
						"16236 (5%)", -- [36]
						"9892 (3%)", -- [37]
						"3589 (1%)", -- [38]
						"1 (0%)", -- [39]
						"0 (0%)", -- [40]
					},
					["EventNum"] = {
						3.59265849479373, -- [1]
						1.44556478186371, -- [2]
						1.9276393831554, -- [3]
						0.696915776986951, -- [4]
						1.44589429286938, -- [5]
						1.86404375906155, -- [6]
						0.836298932384342, -- [7]
						1.9276393831554, -- [8]
						1.44556478186371, -- [9]
						1.67226835376302, -- [10]
						2.41531567154343, -- [11]
						1.44556478186371, -- [12]
						1.92730987214973, -- [13]
						2.09041781995519, -- [14]
						1.44556478186371, -- [15]
						2.10821141426124, -- [16]
						1.53321470937129, -- [17]
						1.92730987214973, -- [18]
						1.44556478186371, -- [19]
						1.67226835376302, -- [20]
						2.89607222881244, -- [21]
						1.44556478186371, -- [22]
						1.9276393831554, -- [23]
						2.71780677474628, -- [24]
						2.07657835771715, -- [25]
						3.08389350204297, -- [26]
						9.67642019243443, -- [27]
						1.44556478186371, -- [28]
						2.07657835771715, -- [29]
						1.55924607881903, -- [30]
						1.95136417556346, -- [31]
						2.07657835771715, -- [32]
						1.92730987214973, -- [33]
						1.44556478186371, -- [34]
						4.62600500856729, -- [35]
						2.07657835771715, -- [36]
						2.09041781995519, -- [37]
						2.07690786872282, -- [38]
						1.51608013707658, -- [39]
						0, -- [40]
					},
					["MessageType"] = {
						"DAMAGE", -- [1]
						"DAMAGE", -- [2]
						"DAMAGE", -- [3]
						"DAMAGE", -- [4]
						"DAMAGE", -- [5]
						"DAMAGE", -- [6]
						"DAMAGE", -- [7]
						"DAMAGE", -- [8]
						"DAMAGE", -- [9]
						"DAMAGE", -- [10]
						"DAMAGE", -- [11]
						"DAMAGE", -- [12]
						"DAMAGE", -- [13]
						"DAMAGE", -- [14]
						"DAMAGE", -- [15]
						"DAMAGE", -- [16]
						"DAMAGE", -- [17]
						"DAMAGE", -- [18]
						"DAMAGE", -- [19]
						"DAMAGE", -- [20]
						"DAMAGE", -- [21]
						"DAMAGE", -- [22]
						"DAMAGE", -- [23]
						"DAMAGE", -- [24]
						"DAMAGE", -- [25]
						"DAMAGE", -- [26]
						"DAMAGE", -- [27]
						"DAMAGE", -- [28]
						"DAMAGE", -- [29]
						"DAMAGE", -- [30]
						"DAMAGE", -- [31]
						"DAMAGE", -- [32]
						"DAMAGE", -- [33]
						"DAMAGE", -- [34]
						"DAMAGE", -- [35]
						"DAMAGE", -- [36]
						"DAMAGE", -- [37]
						"DAMAGE", -- [38]
						"DAMAGE", -- [39]
						"MISC", -- [40]
					},
				}, -- [4]
			},
			["TimeLast"] = {
				["DeathCount"] = 119906.534,
				["Overhealing"] = 119894.53,
				["ActiveTime"] = 120185.532,
				["TimeDamage"] = 120185.532,
				["OVERALL"] = 120185.532,
				["DamageTaken"] = 120141.53,
				["Damage"] = 120185.532,
			},
			["LastAttackedBy"] = "Environment",
			["LastEventType"] = {
				"DAMAGE", -- [1]
				"DAMAGE", -- [2]
				"DAMAGE", -- [3]
				"DAMAGE", -- [4]
				"MISC", -- [5]
				"DAMAGE", -- [6]
				"DAMAGE", -- [7]
				"DAMAGE", -- [8]
				"DAMAGE", -- [9]
				"DAMAGE", -- [10]
				"MISC", -- [11]
				"DAMAGE", -- [12]
				"HEAL", -- [13]
				"DAMAGE", -- [14]
				"DAMAGE", -- [15]
				"DAMAGE", -- [16]
				"DAMAGE", -- [17]
				"DAMAGE", -- [18]
				"DAMAGE", -- [19]
				"DAMAGE", -- [20]
				"DAMAGE", -- [21]
				"DAMAGE", -- [22]
				"DAMAGE", -- [23]
				"MISC", -- [24]
				"DAMAGE", -- [25]
				"DAMAGE", -- [26]
				"DAMAGE", -- [27]
				"DAMAGE", -- [28]
				"DAMAGE", -- [29]
				"DAMAGE", -- [30]
				"DAMAGE", -- [31]
				"DAMAGE", -- [32]
				"DAMAGE", -- [33]
				"DAMAGE", -- [34]
				"DAMAGE", -- [35]
				"DAMAGE", -- [36]
				"DAMAGE", -- [37]
				"DAMAGE", -- [38]
				"DAMAGE", -- [39]
				"DAMAGE", -- [40]
				"DAMAGE", -- [41]
				"DAMAGE", -- [42]
				"DAMAGE", -- [43]
				"DAMAGE", -- [44]
				"DAMAGE", -- [45]
				"DAMAGE", -- [46]
				"DAMAGE", -- [47]
				"DAMAGE", -- [48]
				"DAMAGE", -- [49]
				"DAMAGE", -- [50]
			},
			["TimeWindows"] = {
				["DeathCount"] = {
					4, -- [1]
				},
				["Overhealing"] = {
					75870, -- [1]
				},
				["ActiveTime"] = {
					42.97, -- [1]
				},
				["TimeDamage"] = {
					42.97, -- [1]
				},
				["DamageTaken"] = {
					1107665, -- [1]
				},
				["Damage"] = {
					3062669, -- [1]
				},
			},
			["enClass"] = "DEMONHUNTER",
			["LastDamageTaken"] = 64018,
			["GUID"] = "Player-76-09050976",
			["level"] = 100,
			["LastDamageAbility"] = "Falling",
			["LastFightIn"] = 5,
			["LastEventNum"] = {
				2.12007381046527, -- [1]
				1.22643996309477, -- [2]
				nil, -- [3]
				6.57473309608541, -- [4]
				nil, -- [5]
				2.59259259259259, -- [6]
				58.5751944114934, -- [7]
				1.39646764201924, -- [8]
				3.71820218795308, -- [9]
				3.33662844339001, -- [10]
				nil, -- [11]
				nil, -- [12]
				25, -- [13]
				5.93778832212996, -- [14]
				7.25023065770397, -- [15]
				8.53136944773955, -- [16]
				9.44543297746145, -- [17]
				11.1868986424147, -- [18]
				12.0146302886516, -- [19]
				13.2921444576249, -- [20]
				14.3343877685515, -- [21]
				15.933834190062, -- [22]
				16.6261368129696, -- [23]
				[25] = 21.0946355608277,
				[35] = 1.92730987214973,
				[36] = 0.696915776986951,
				[37] = 3.08422301304864,
				[38] = 2.27593251614604,
				[39] = 1.44556478186371,
				[40] = 0.836298932384342,
				[41] = 3.08422301304864,
				[42] = 1.92730987214973,
				[43] = 6.30025042836431,
				[44] = 0.703505997100303,
				[45] = 1.44556478186371,
				[46] = 1.48312903650982,
				[47] = 0.363780150257019,
				[48] = 0.975352576776064,
				[49] = 3.08422301304864,
				[50] = 1.59022011335179,
			},
			["type"] = "Self",
			["FightsSaved"] = 5,
			["LastEventHealth"] = {
				"4449 (1%)", -- [1]
				"727 (0%)", -- [2]
				"727 (0%)", -- [3]
				"1 (0%)", -- [4]
				"0 (0%)", -- [5]
				"205868 (67%)", -- [6]
				"20236 (6%)", -- [7]
				"15998 (5%)", -- [8]
				"4714 (1%)", -- [9]
				"1 (0%)", -- [10]
				"0 (0%)", -- [11]
				"303480 (100%)", -- [12]
				"303480 (100%)", -- [13]
				"285460 (94%)", -- [14]
				"263457 (86%)", -- [15]
				"237566 (78%)", -- [16]
				"208901 (68%)", -- [17]
				"174951 (57%)", -- [18]
				"138489 (45%)", -- [19]
				"98150 (32%)", -- [20]
				"54648 (18%)", -- [21]
				"6292 (2%)", -- [22]
				"1 (0%)", -- [23]
				"0 (0%)", -- [24]
				"239462 (78%)", -- [25]
				"264116 (87%)", -- [26]
				"267182 (88%)", -- [27]
				"279512 (92%)", -- [28]
				"279512 (92%)", -- [29]
				"285670 (94%)", -- [30]
				"285670 (94%)", -- [31]
				"285670 (94%)", -- [32]
				"294918 (97%)", -- [33]
				"294918 (97%)", -- [34]
				"99792 (32%)", -- [35]
				"97677 (32%)", -- [36]
				"88317 (29%)", -- [37]
				"81410 (26%)", -- [38]
				"77023 (25%)", -- [39]
				"74485 (24%)", -- [40]
				"65125 (21%)", -- [41]
				"59276 (19%)", -- [42]
				"40156 (13%)", -- [43]
				"38021 (12%)", -- [44]
				"33634 (11%)", -- [45]
				"29133 (9%)", -- [46]
				"29133 (9%)", -- [47]
				"25069 (8%)", -- [48]
				"15709 (5%)", -- [49]
				"10883 (3%)", -- [50]
			},
			["unit"] = "Falridan",
			["Owner"] = false,
			["LastAbility"] = 120186.058,
			["NextEventNum"] = 35,
			["LastEventHealthNum"] = {
				1.4659944642151, -- [1]
				0.239554501120337, -- [2]
				0.239554501120337, -- [3]
				0.000329511005667589, -- [4]
				0, -- [5]
				67.8357717147753, -- [6]
				6.66798471068934, -- [7]
				5.27151706867009, -- [8]
				1.55331488071702, -- [9]
				0.000329511005667589, -- [10]
				0, -- [11]
				100, -- [12]
				100, -- [13]
				94.06221167787, -- [14]
				86.8119810201661, -- [15]
				78.2806115724265, -- [16]
				68.8351785949651, -- [17]
				57.6482799525504, -- [18]
				45.6336496638988, -- [19]
				32.3415052062739, -- [20]
				18.0071174377224, -- [21]
				2.07328324766047, -- [22]
				0.000329511005667589, -- [23]
				0, -- [24]
				78.9053644391723, -- [25]
				87.029128772901, -- [26]
				88.0394095162779, -- [27]
				92.1022802161592, -- [28]
				92.1022802161592, -- [29]
				94.1314089890602, -- [30]
				94.1314089890602, -- [31]
				94.1314089890602, -- [32]
				97.1787267694741, -- [33]
				97.1787267694741, -- [34]
				32.8825622775801, -- [35]
				32.1856465005931, -- [36]
				29.1014234875445, -- [37]
				26.8254909713984, -- [38]
				25.3799261895347, -- [39]
				24.5436272571504, -- [40]
				21.4594042441018, -- [41]
				19.532094371952, -- [42]
				13.2318439435877, -- [43]
				12.5283379464874, -- [44]
				11.0827731646237, -- [45]
				9.59964412811388, -- [46]
				9.59964412811388, -- [47]
				8.2605114010808, -- [48]
				5.17628838803216, -- [49]
				3.58606827468037, -- [50]
			},
			["LastEvents"] = {
				"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -6434 (Shadow)", -- [1]
				"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -3722 (Shadow)", -- [2]
				"Creepyhollow-BleedingHollow Melee Falridan Miss (1)", -- [3]
				"Creepyhollow-BleedingHollow Shadowstrike Falridan Crit -19953 (Physical)", -- [4]
				"Falridan dies.", -- [5]
				"Fel Screecher Melee Falridan Hit -7868 (Physical)", -- [6]
				"Environment Falling Falridan Hit -177764 (Physical)", -- [7]
				"Hillarytrump-Archimonde Auto Shot Falridan Hit -4238 (Physical)", -- [8]
				"Hillarytrump-Archimonde Cobra Shot Falridan Hit -11284 (Physical)", -- [9]
				"Hillarytrump-Archimonde Cobra Shot Falridan Hit -10126 (Physical)", -- [10]
				"Falridan dies.", -- [11]
				"Falridan Fel Rush Imagination-Auchindoun Crit -28340", -- [12]
				"Falridan Consume Soul Falridan Hit +75870 (75870 overheal)", -- [13]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -18020 (Shadow)", -- [14]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -22003 (Shadow)", -- [15]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -25891 (Shadow)", -- [16]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -28665 (Shadow)", -- [17]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -33950 (Shadow)", -- [18]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -36462 (Shadow)", -- [19]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -40339 (Shadow)", -- [20]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -43502 (Shadow)", -- [21]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -48356 (Shadow)", -- [22]
				"Darkmagus Falo'reth Eye of Darkness Falridan Hit -50457 (Shadow)", -- [23]
				"Falridan dies.", -- [24]
				"Environment Falling Falridan Hit -64018 (Physical)", -- [25]
				"Falridan Throw Glaive Emeraldon Oracle Crit -1185601 (Physical)", -- [26]
				"Falridan Fel Rush Emeraldon Oracle Crit -1033314", -- [27]
				"Falridan Melee Emeraldon Oracle Hit -65518 (Physical)", -- [28]
				"Emeraldon Oracle Melee Falridan Miss (1)", -- [29]
				"Emeraldon Oracle Melee Falridan Miss (1)", -- [30]
				"Falridan Melee Emeraldon Oracle Hit -62962 (Physical)", -- [31]
				"Falridan Melee Emeraldon Oracle Hit -125881 (Physical)", -- [32]
				"Phantim Melee Falridan Miss (1)", -- [33]
				"Falridan Demon's Bite Phantim Hit -313174 (Physical)", -- [34]
				"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5849 (Shadow)", -- [35]
				"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2115 (Shadow)", -- [36]
				"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Tick -9360 (Shadow)", -- [37]
				"Bogyshu-Illidan Immolate (DoT) Falridan Tick -6907 (Fire)", -- [38]
				"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [39]
				"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2538 (Shadow)", -- [40]
				"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Tick -9360 (Shadow)", -- [41]
				"Slipperyass-Mal'Ganis Siphon Life (DoT) Falridan Tick -5849 (Shadow)", -- [42]
				"Creepyhollow-BleedingHollow Shadowstrike Falridan Crit -19120 (Physical)", -- [43]
				"Creepyhollow-BleedingHollow Melee Falridan Hit -2135 (Physical)", -- [44]
				"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4387 (Shadow)", -- [45]
				"Bogyshu-Illidan Immolate (DoT) Falridan Tick -4501 (Fire)", -- [46]
				"Creepyhollow-BleedingHollow Melee Falridan Hit -1104 (Physical)", -- [47]
				"Slipperyass-Mal'Ganis Agony (DoT) Falridan Tick -2960 (Shadow)", -- [48]
				"Slipperyass-Mal'Ganis Unstable Affliction (DoT) Falridan Tick -9360 (Shadow)", -- [49]
				"Slipperyass-Mal'Ganis Corruption (DoT) Falridan Tick -4826 (Shadow)", -- [50]
			},
			["LastEventIncoming"] = {
				true, -- [1]
				true, -- [2]
				true, -- [3]
				true, -- [4]
				true, -- [5]
				true, -- [6]
				true, -- [7]
				true, -- [8]
				true, -- [9]
				true, -- [10]
				true, -- [11]
				false, -- [12]
				true, -- [13]
				true, -- [14]
				true, -- [15]
				true, -- [16]
				true, -- [17]
				true, -- [18]
				true, -- [19]
				true, -- [20]
				true, -- [21]
				true, -- [22]
				true, -- [23]
				true, -- [24]
				true, -- [25]
				false, -- [26]
				false, -- [27]
				false, -- [28]
				true, -- [29]
				true, -- [30]
				false, -- [31]
				false, -- [32]
				true, -- [33]
				false, -- [34]
				true, -- [35]
				true, -- [36]
				true, -- [37]
				true, -- [38]
				true, -- [39]
				true, -- [40]
				true, -- [41]
				true, -- [42]
				true, -- [43]
				true, -- [44]
				true, -- [45]
				true, -- [46]
				true, -- [47]
				true, -- [48]
				true, -- [49]
				true, -- [50]
			},
			["Name"] = "Falridan",
			["LastEventTimes"] = {
				119207.442, -- [1]
				119207.882, -- [2]
				119207.882, -- [3]
				119207.882, -- [4]
				119207.882, -- [5]
				119345.832, -- [6]
				119348.152, -- [7]
				119350.648, -- [8]
				119350.957, -- [9]
				119351.901, -- [10]
				119351.901, -- [11]
				119886.706, -- [12]
				119894.541, -- [13]
				119902.892, -- [14]
				119903.406, -- [15]
				119903.934, -- [16]
				119904.45, -- [17]
				119904.965, -- [18]
				119905.471, -- [19]
				119905.907, -- [20]
				119906.408, -- [21]
				119906.924, -- [22]
				119907.437, -- [23]
				119907.437, -- [24]
				120141.963, -- [25]
				120157.423, -- [26]
				120161.107, -- [27]
				120172.962, -- [28]
				120172.962, -- [29]
				120179.898, -- [30]
				120179.994, -- [31]
				120179.994, -- [32]
				120186.058, -- [33]
				120186.058, -- [34]
				119202.244, -- [35]
				119202.764, -- [36]
				119202.908, -- [37]
				119203.094, -- [38]
				119203.62, -- [39]
				119204.431, -- [40]
				119204.631, -- [41]
				119204.876, -- [42]
				119204.972, -- [43]
				119204.972, -- [44]
				119205.283, -- [45]
				119205.594, -- [46]
				119205.73, -- [47]
				119206.16, -- [48]
				119206.382, -- [49]
				119207.023, -- [50]
			},
			["Fights"] = {
				["Fight5"] = {
					["Attacks"] = {
						["Vengeful Retreat"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 3716,
									["min"] = 3716,
									["count"] = 1,
									["amount"] = 3716,
								},
							},
							["count"] = 1,
							["amount"] = 3716,
						},
						["Blade Dance"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 7647,
									["min"] = 7647,
									["count"] = 1,
									["amount"] = 7647,
								},
								["Hit"] = {
									["max"] = 3734,
									["min"] = 3734,
									["count"] = 1,
									["amount"] = 3734,
								},
							},
							["count"] = 2,
							["amount"] = 11381,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 4233,
									["min"] = 2117,
									["count"] = 2,
									["amount"] = 6350,
								},
								["Crit"] = {
									["max"] = 8309,
									["min"] = 7691,
									["count"] = 3,
									["amount"] = 23820,
								},
								["Miss"] = {
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 9,
							["amount"] = 30170,
						},
						["Fel Rush"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 22425,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 22425,
								},
							},
							["count"] = 1,
							["amount"] = 22425,
						},
						["Throw Glaive"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 18238,
									["min"] = 18238,
									["count"] = 1,
									["amount"] = 18238,
								},
							},
							["count"] = 1,
							["amount"] = 18238,
						},
						["Demon's Bite"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 20315,
									["min"] = 20315,
									["count"] = 1,
									["amount"] = 20315,
								},
								["Hit"] = {
									["max"] = 10136,
									["min"] = 9632,
									["count"] = 3,
									["amount"] = 29509,
								},
							},
							["count"] = 4,
							["amount"] = 49824,
						},
					},
					["WhoDamaged"] = {
						["Slipperyass-Mal'Ganis"] = {
							["Details"] = {
								["Corruption (DoT)"] = {
									["count"] = 6581,
								},
								["Siphon Life (DoT)"] = {
									["count"] = 5849,
								},
								["Agony (DoT)"] = {
									["count"] = 2115,
								},
							},
							["amount"] = 14545,
						},
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Fireball"] = {
									["count"] = 8475,
								},
								["Fel Fireball (DoT)"] = {
									["count"] = 2987,
								},
							},
							["amount"] = 11462,
						},
						["Bogyshu-Illidan"] = {
							["Details"] = {
								["Immolate"] = {
									["count"] = 12738,
								},
							},
							["amount"] = 12738,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Destructive Whirlwind (DoT)"] = {
									["count"] = 16644,
								},
							},
							["amount"] = 16644,
						},
					},
					["PartialAbsorb"] = {
						["Immolate"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Fel Fireball"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Fel Fireball (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 4,
							["amount"] = 0,
						},
						["Destructive Whirlwind (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
					},
					["ElementHitsTaken"] = {
						["Shadow"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 1,
								},
								["Tick"] = {
									["count"] = 3,
								},
							},
							["amount"] = 4,
						},
						["Physical"] = {
							["Details"] = {
								["Tick"] = {
									["count"] = 3,
								},
							},
							["amount"] = 3,
						},
						["Melee"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
						["Fire"] = {
							["Details"] = {
								["Tick"] = {
									["count"] = 4,
								},
								["Hit"] = {
									["count"] = 2,
								},
							},
							["amount"] = 6,
						},
					},
					["TimeSpent"] = {
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Shadowsworn Defiler"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Shadowsworn Ritualist"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Vengeful Retreat"] = {
									["count"] = 0.91,
								},
								["Blade Dance"] = {
									["count"] = 1.78,
								},
								["Melee"] = {
									["count"] = 7.28,
								},
								["Throw Glaive"] = {
									["count"] = 1.65,
								},
								["Demon's Bite"] = {
									["count"] = 3.35,
								},
							},
							["amount"] = 14.97,
						},
					},
					["DamageTaken"] = 55389,
					["PartialResist"] = {
						["Immolate"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Fel Fireball"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Fel Fireball (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 4,
						},
						["Destructive Whirlwind (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
					},
					["ElementDone"] = {
						["Melee"] = 30170,
						["Physical"] = 83159,
					},
					["ElementHitsDone"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 2,
								},
								["Crit"] = {
									["count"] = 3,
								},
								["Miss"] = {
									["count"] = 4,
								},
							},
							["amount"] = 9,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 2,
								},
								["Hit"] = {
									["count"] = 6,
								},
							},
							["amount"] = 8,
						},
					},
					["DamagedWho"] = {
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 22425,
								},
							},
							["amount"] = 22425,
						},
						["Shadowsworn Defiler"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Shadowsworn Ritualist"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Vengeful Retreat"] = {
									["count"] = 3716,
								},
								["Blade Dance"] = {
									["count"] = 11381,
								},
								["Melee"] = {
									["count"] = 30170,
								},
								["Throw Glaive"] = {
									["count"] = 18238,
								},
								["Demon's Bite"] = {
									["count"] = 49824,
								},
							},
							["amount"] = 113329,
						},
					},
					["TimeDamage"] = 18.47,
					["TimeDamaging"] = {
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Shadowsworn Defiler"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Shadowsworn Ritualist"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Vengeful Retreat"] = {
									["count"] = 0.91,
								},
								["Blade Dance"] = {
									["count"] = 1.78,
								},
								["Melee"] = {
									["count"] = 7.28,
								},
								["Throw Glaive"] = {
									["count"] = 1.65,
								},
								["Demon's Bite"] = {
									["count"] = 3.35,
								},
							},
							["amount"] = 14.97,
						},
					},
					["ElementTaken"] = {
						["Physical"] = 16644,
						["Shadow"] = 14545,
						["Fire"] = 24200,
					},
					["ActiveTime"] = 18.47,
					["Damage"] = 135754,
				},
				["Fight2"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 211280,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
						["Physical"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 4,
								},
							},
							["amount"] = 4,
						},
					},
					["DeathCount"] = 1,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Melee"] = 7868,
						["Physical"] = 203412,
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
						["Falling"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Cobra Shot"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Auto Shot"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Fel Screecher"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 7868,
								},
							},
							["amount"] = 7868,
						},
						["Hillarytrump-Archimonde"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 4238,
								},
								["Cobra Shot"] = {
									["count"] = 21410,
								},
							},
							["amount"] = 25648,
						},
						["Environment"] = {
							["Details"] = {
								["Falling"] = {
									["count"] = 177764,
								},
							},
							["amount"] = 177764,
						},
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
						["Falling"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Cobra Shot"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Auto Shot"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Melee"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Physical"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 0,
								},
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Physical"] = 0,
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
						["Melee"] = 0,
						["Physical"] = 0,
					},
					["PartialAbsorb"] = {
						["Falling"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Phantim"] = {
							["Details"] = {
								["Demon's Bite"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Emeraldon Oracle"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
								["Throw Glaive"] = {
									["count"] = 0,
								},
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Environment"] = {
							["Details"] = {
								["Falling"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
						["Falling"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
						["Phantim"] = {
							["Details"] = {
								["Demon's Bite"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Emeraldon Oracle"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
								["Throw Glaive"] = {
									["count"] = 0,
								},
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Demon's Bite"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Throw Glaive"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Fel Rush"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
						["Phantim"] = {
							["Details"] = {
								["Demon's Bite"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Emeraldon Oracle"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
								["Throw Glaive"] = {
									["count"] = 0,
								},
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["Fight3"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 174062,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 2,
								},
							},
							["amount"] = 2,
						},
						["Shadow"] = {
							["Details"] = {
								["Tick"] = {
									["count"] = 19,
								},
							},
							["amount"] = 19,
						},
						["Melee"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 2,
								},
							},
							["amount"] = 3,
						},
						["Fire"] = {
							["Details"] = {
								["Tick"] = {
									["count"] = 3,
								},
								["Hit"] = {
									["count"] = 2,
								},
							},
							["amount"] = 5,
						},
					},
					["DeathCount"] = 1,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Physical"] = 39073,
						["Shadow"] = 89578,
						["Melee"] = 3239,
						["Fire"] = 42172,
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
						["Immolate"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 7,
									["amount"] = 0,
								},
							},
							["count"] = 7,
							["amount"] = 0,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 4,
							["amount"] = 0,
						},
						["Shadowstrike"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Immolate (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
						["Unstable Affliction (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
						["Conflagrate"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Bogyshu-Illidan"] = {
							["Details"] = {
								["Immolate (DoT)"] = {
									["count"] = 16665,
								},
								["Immolate"] = {
									["count"] = 11115,
								},
								["Conflagrate"] = {
									["count"] = 14392,
								},
							},
							["amount"] = 42172,
						},
						["Creepyhollow-BleedingHollow"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3239,
								},
								["Shadowstrike"] = {
									["count"] = 39073,
								},
							},
							["amount"] = 42312,
						},
						["Slipperyass-Mal'Ganis"] = {
							["Details"] = {
								["Siphon Life (DoT)"] = {
									["count"] = 23982,
								},
								["Corruption (DoT)"] = {
									["count"] = 22374,
								},
								["Unstable Affliction (DoT)"] = {
									["count"] = 28080,
								},
								["Agony (DoT)"] = {
									["count"] = 15142,
								},
							},
							["amount"] = 89578,
						},
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
						["Immolate"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 7,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 7,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 4,
						},
						["Shadowstrike"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Immolate (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
						["Unstable Affliction (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
						["Conflagrate"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["Fight4"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 217742,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Fire"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 2,
								},
								["Tick"] = {
									["count"] = 4,
								},
							},
							["amount"] = 6,
						},
						["Shadow"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 4,
								},
								["Tick"] = {
									["count"] = 22,
								},
							},
							["amount"] = 26,
						},
					},
					["DeathCount"] = 1,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Fire"] = 61216,
						["Shadow"] = 156526,
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 7,
									["amount"] = 0,
								},
							},
							["count"] = 7,
							["amount"] = 0,
						},
						["Conflagrate"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Drain Life (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Immolate (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Unstable Affliction (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 7,
									["amount"] = 0,
								},
							},
							["count"] = 7,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Slipperyass-Mal'Ganis"] = {
							["Details"] = {
								["Corruption (DoT)"] = {
									["count"] = 30709,
								},
								["Siphon Life (DoT)"] = {
									["count"] = 29247,
								},
								["Drain Life (DoT)"] = {
									["count"] = 31511,
								},
								["Unstable Affliction (DoT)"] = {
									["count"] = 23398,
								},
								["Agony (DoT)"] = {
									["count"] = 41661,
								},
							},
							["amount"] = 156526,
						},
						["Bogyshu-Illidan"] = {
							["Details"] = {
								["Conflagrate"] = {
									["count"] = 29366,
								},
								["Immolate (DoT)"] = {
									["count"] = 31850,
								},
							},
							["amount"] = 61216,
						},
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 7,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 7,
						},
						["Conflagrate"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Drain Life (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Immolate (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Unstable Affliction (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 7,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 7,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 347645,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Shadow"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 10,
								},
							},
							["amount"] = 10,
						},
					},
					["DeathCount"] = 1,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Shadow"] = 347645,
					},
					["DOT_Time"] = 0,
					["Damage"] = 28340,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
						["Eye of Darkness"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 10,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 28340,
								},
							},
							["amount"] = 28340,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Darkmagus Falo'reth"] = {
							["Details"] = {
								["Eye of Darkness"] = {
									["count"] = 347645,
								},
							},
							["amount"] = 347645,
						},
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
						["Eye of Darkness"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 10,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
						["Consume Soul"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 75870,
									["min"] = 75870,
									["count"] = 1,
									["amount"] = 75870,
								},
							},
							["count"] = 1,
							["amount"] = 75870,
						},
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 75870,
					["TimeSpent"] = {
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 3.5,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Fel Rush"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 28340,
									["min"] = 28340,
									["count"] = 1,
									["amount"] = 28340,
								},
							},
							["count"] = 1,
							["amount"] = 28340,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 3.5,
					["TimeDamaging"] = {
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["Fight1"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 347645,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Shadow"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 10,
								},
							},
							["amount"] = 10,
						},
					},
					["DeathCount"] = 1,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Shadow"] = 347645,
					},
					["DOT_Time"] = 0,
					["Damage"] = 28340,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
						["Eye of Darkness"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 10,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 28340,
								},
							},
							["amount"] = 28340,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Darkmagus Falo'reth"] = {
							["Details"] = {
								["Eye of Darkness"] = {
									["count"] = 347645,
								},
							},
							["amount"] = 347645,
						},
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
						["Eye of Darkness"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 10,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
						["Consume Soul"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 75870,
									["min"] = 75870,
									["count"] = 1,
									["amount"] = 75870,
								},
							},
							["count"] = 1,
							["amount"] = 75870,
						},
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 75870,
					["TimeSpent"] = {
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 3.5,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Fel Rush"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 28340,
									["min"] = 28340,
									["count"] = 1,
									["amount"] = 28340,
								},
							},
							["count"] = 1,
							["amount"] = 28340,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 3.5,
					["TimeDamaging"] = {
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["OverallData"] = {
					["OverHeals"] = {
						["Consume Soul"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 75870,
									["min"] = 75870,
									["count"] = 1,
									["amount"] = 75870,
								},
							},
							["count"] = 1,
							["amount"] = 75870,
						},
					},
					["TimeSpent"] = {
						["Shadowsworn Ritualist"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Phantim"] = {
							["Details"] = {
								["Demon's Bite"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Vengeful Retreat"] = {
									["count"] = 0.91,
								},
								["Blade Dance"] = {
									["count"] = 1.78,
								},
								["Melee"] = {
									["count"] = 7.28,
								},
								["Throw Glaive"] = {
									["count"] = 1.65,
								},
								["Demon's Bite"] = {
									["count"] = 3.35,
								},
							},
							["amount"] = 14.97,
						},
						["Emeraldon Oracle"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 7,
								},
								["Throw Glaive"] = {
									["count"] = 3.5,
								},
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 14,
						},
						["Shadowsworn Defiler"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["DamageTaken"] = 1107665,
					["PartialResist"] = {
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 15,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 15,
						},
						["Cobra Shot"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Auto Shot"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Immolate"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 19,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 19,
						},
						["Shadowstrike"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Unstable Affliction (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Drain Life (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 8,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 8,
						},
						["Eye of Darkness"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 10,
						},
						["Destructive Whirlwind (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
						["Falling"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 11,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 11,
						},
						["Fel Fireball"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Immolate (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 10,
						},
						["Conflagrate"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Fel Fireball (DoT)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 4,
						},
					},
					["DeathCount"] = 4,
					["PartialAbsorb"] = {
						["Corruption (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 15,
									["amount"] = 0,
								},
							},
							["count"] = 15,
							["amount"] = 0,
						},
						["Cobra Shot"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Auto Shot"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Immolate"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Agony (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 19,
									["amount"] = 0,
								},
							},
							["count"] = 19,
							["amount"] = 0,
						},
						["Shadowstrike"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Unstable Affliction (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Drain Life (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 8,
									["amount"] = 0,
								},
							},
							["count"] = 8,
							["amount"] = 0,
						},
						["Eye of Darkness"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 10,
							["amount"] = 0,
						},
						["Destructive Whirlwind (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
						["Falling"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Siphon Life (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 11,
									["amount"] = 0,
								},
							},
							["count"] = 11,
							["amount"] = 0,
						},
						["Fel Fireball"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Immolate (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 10,
									["amount"] = 0,
								},
							},
							["count"] = 10,
							["amount"] = 0,
						},
						["Conflagrate"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Fel Fireball (DoT)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 4,
							["amount"] = 0,
						},
					},
					["ActiveTime"] = 42.97,
					["ElementTaken"] = {
						["Shadow"] = 629263,
						["Physical"] = 323147,
						["Melee"] = 11107,
						["Fire"] = 144148,
					},
					["Damage"] = 3062669,
					["Overhealing"] = 75870,
					["ElementDone"] = {
						["Melee"] = 284531,
						["Physical"] = 1581934,
					},
					["ElementHitsDone"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 5,
								},
								["Crit"] = {
									["count"] = 3,
								},
								["Miss"] = {
									["count"] = 4,
								},
							},
							["amount"] = 12,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 3,
								},
								["Hit"] = {
									["count"] = 7,
								},
							},
							["amount"] = 10,
						},
					},
					["DamagedWho"] = {
						["Shadowsworn Ritualist"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 22425,
								},
							},
							["amount"] = 22425,
						},
						["Phantim"] = {
							["Details"] = {
								["Demon's Bite"] = {
									["count"] = 313174,
								},
							},
							["amount"] = 313174,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Vengeful Retreat"] = {
									["count"] = 3716,
								},
								["Blade Dance"] = {
									["count"] = 11381,
								},
								["Melee"] = {
									["count"] = 30170,
								},
								["Throw Glaive"] = {
									["count"] = 18238,
								},
								["Demon's Bite"] = {
									["count"] = 49824,
								},
							},
							["amount"] = 113329,
						},
						["Emeraldon Oracle"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 254361,
								},
								["Throw Glaive"] = {
									["count"] = 1185601,
								},
								["Fel Rush"] = {
									["count"] = 1033314,
								},
							},
							["amount"] = 2473276,
						},
						["Shadowsworn Defiler"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 89700,
								},
							},
							["amount"] = 89700,
						},
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 22425,
								},
							},
							["amount"] = 22425,
						},
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 28340,
								},
							},
							["amount"] = 28340,
						},
					},
					["TimeDamage"] = 42.97,
					["TimeDamaging"] = {
						["Shadowsworn Ritualist"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Phantim"] = {
							["Details"] = {
								["Demon's Bite"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Vengeful Retreat"] = {
									["count"] = 0.91,
								},
								["Blade Dance"] = {
									["count"] = 1.78,
								},
								["Melee"] = {
									["count"] = 7.28,
								},
								["Throw Glaive"] = {
									["count"] = 1.65,
								},
								["Demon's Bite"] = {
									["count"] = 3.35,
								},
							},
							["amount"] = 14.97,
						},
						["Emeraldon Oracle"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 7,
								},
								["Throw Glaive"] = {
									["count"] = 3.5,
								},
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 14,
						},
						["Shadowsworn Defiler"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Imagination-Auchindoun"] = {
							["Details"] = {
								["Fel Rush"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["Attacks"] = {
						["Vengeful Retreat"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 3716,
									["min"] = 3716,
									["count"] = 1,
									["amount"] = 3716,
								},
							},
							["count"] = 1,
							["amount"] = 3716,
						},
						["Blade Dance"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 7647,
									["min"] = 7647,
									["count"] = 1,
									["amount"] = 7647,
								},
								["Hit"] = {
									["max"] = 3734,
									["min"] = 3734,
									["count"] = 1,
									["amount"] = 3734,
								},
							},
							["count"] = 2,
							["amount"] = 11381,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 125881,
									["min"] = 2117,
									["count"] = 5,
									["amount"] = 260711,
								},
								["Crit"] = {
									["max"] = 8309,
									["min"] = 7691,
									["count"] = 3,
									["amount"] = 23820,
								},
								["Miss"] = {
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 12,
							["amount"] = 284531,
						},
						["Fel Rush"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 1033314,
									["min"] = 28340,
									["count"] = 3,
									["amount"] = 1151354,
								},
								["Hit"] = {
									["max"] = 22425,
									["min"] = 22425,
									["count"] = 2,
									["amount"] = 44850,
								},
							},
							["count"] = 5,
							["amount"] = 1196204,
						},
						["Throw Glaive"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 1185601,
									["min"] = 1185601,
									["count"] = 1,
									["amount"] = 1185601,
								},
								["Hit"] = {
									["max"] = 18238,
									["min"] = 18238,
									["count"] = 1,
									["amount"] = 18238,
								},
							},
							["count"] = 2,
							["amount"] = 1203839,
						},
						["Demon's Bite"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 20315,
									["min"] = 20315,
									["count"] = 1,
									["amount"] = 20315,
								},
								["Hit"] = {
									["max"] = 313174,
									["min"] = 9632,
									["count"] = 4,
									["amount"] = 342683,
								},
							},
							["count"] = 5,
							["amount"] = 362998,
						},
					},
					["WhoDamaged"] = {
						["Creepyhollow-BleedingHollow"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3239,
								},
								["Shadowstrike"] = {
									["count"] = 39073,
								},
							},
							["amount"] = 42312,
						},
						["Hillarytrump-Archimonde"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 4238,
								},
								["Cobra Shot"] = {
									["count"] = 21410,
								},
							},
							["amount"] = 25648,
						},
						["Darkmagus Falo'reth"] = {
							["Details"] = {
								["Eye of Darkness"] = {
									["count"] = 347645,
								},
							},
							["amount"] = 347645,
						},
						["Fel Screecher"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 7868,
								},
							},
							["amount"] = 7868,
						},
						["Bogyshu-Illidan"] = {
							["Details"] = {
								["Immolate (DoT)"] = {
									["count"] = 65075,
								},
								["Immolate"] = {
									["count"] = 23853,
								},
								["Conflagrate"] = {
									["count"] = 43758,
								},
							},
							["amount"] = 132686,
						},
						["Felguard Punisher"] = {
							["Details"] = {
								["Destructive Whirlwind (DoT)"] = {
									["count"] = 16644,
								},
							},
							["amount"] = 16644,
						},
						["Slipperyass-Mal'Ganis"] = {
							["Details"] = {
								["Corruption (DoT)"] = {
									["count"] = 68439,
								},
								["Siphon Life (DoT)"] = {
									["count"] = 64928,
								},
								["Drain Life (DoT)"] = {
									["count"] = 31511,
								},
								["Unstable Affliction (DoT)"] = {
									["count"] = 51478,
								},
								["Agony (DoT)"] = {
									["count"] = 65262,
								},
							},
							["amount"] = 281618,
						},
						["Felsoul Magus"] = {
							["Details"] = {
								["Fel Fireball"] = {
									["count"] = 8475,
								},
								["Fel Fireball (DoT)"] = {
									["count"] = 2987,
								},
							},
							["amount"] = 11462,
						},
						["Environment"] = {
							["Details"] = {
								["Falling"] = {
									["count"] = 241782,
								},
							},
							["amount"] = 241782,
						},
					},
					["ElementHitsTaken"] = {
						["Shadow"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 10,
								},
								["Crit"] = {
									["count"] = 5,
								},
								["Tick"] = {
									["count"] = 50,
								},
							},
							["amount"] = 65,
						},
						["Physical"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 5,
								},
								["Crit"] = {
									["count"] = 2,
								},
								["Tick"] = {
									["count"] = 3,
								},
							},
							["amount"] = 10,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 3,
								},
								["Miss"] = {
									["count"] = 5,
								},
							},
							["amount"] = 8,
						},
						["Fire"] = {
							["Details"] = {
								["Tick"] = {
									["count"] = 12,
								},
								["Crit"] = {
									["count"] = 3,
								},
								["Hit"] = {
									["count"] = 4,
								},
							},
							["amount"] = 19,
						},
					},
				},
			},
			["UnitLockout"] = 119041.533,
			["LastActive"] = 120185.532,
		},
	},
	["FightNum"] = 5,
	["CombatTimes"] = {
		{
			119046.532, -- [1]
			119064.532, -- [2]
			"01:05:01", -- [3]
			"01:05:19", -- [4]
			"Felguard Punisher", -- [5]
		}, -- [1]
		{
			119068.526, -- [1]
			119080.538, -- [2]
			"01:05:23", -- [3]
			"01:05:35", -- [4]
			"Slipperyass-Mal'Ganis", -- [5]
		}, -- [2]
		{
			119198.533, -- [1]
			119208.531, -- [2]
			"01:07:34", -- [3]
			"01:07:43", -- [4]
			"Slipperyass-Mal'Ganis", -- [5]
		}, -- [3]
		{
			119345.526, -- [1]
			119352.526, -- [2]
			"01:10:00", -- [3]
			"01:10:07", -- [4]
			"Fel Screecher", -- [5]
		}, -- [4]
		{
			119886.535, -- [1]
			119907.545, -- [2]
			"01:19:01", -- [3]
			"01:19:22", -- [4]
			"Imagination-Auchindoun", -- [5]
		}, -- [5]
	},
	["FoughtWho"] = {
		"Imagination-Auchindoun 01:19:01-01:19:22", -- [1]
		"Fel Screecher 01:10:00-01:10:07", -- [2]
		"Slipperyass-Mal'Ganis 01:07:34-01:07:43", -- [3]
		"Slipperyass-Mal'Ganis 01:05:23-01:05:35", -- [4]
		"Felguard Punisher 01:05:01-01:05:19", -- [5]
	},
}
