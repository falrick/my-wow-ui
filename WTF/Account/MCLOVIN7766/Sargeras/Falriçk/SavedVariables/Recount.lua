
RecountPerCharDB = {
	["version"] = "1.3",
	["combatants"] = {
		["Magmatron"] = {
			["GUID"] = "Vehicle-0-3783-669-31546-42178-0000315283",
			["type"] = "Trivial",
			["FightsSaved"] = 5,
			["Owner"] = false,
			["enClass"] = "MOB",
			["Name"] = "Magmatron",
			["Fights"] = {
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
			},
			["level"] = -1,
			["UnitLockout"] = 398676.361,
			["LastFightIn"] = 4,
		},
		["Falriçk"] = {
			["DeathLogs"] = {
				{
					["MessageIncoming"] = {
						false, -- [1]
						false, -- [2]
						false, -- [3]
						false, -- [4]
						false, -- [5]
						false, -- [6]
						false, -- [7]
						false, -- [8]
						false, -- [9]
						true, -- [10]
						false, -- [11]
						false, -- [12]
						false, -- [13]
						false, -- [14]
						false, -- [15]
						false, -- [16]
						false, -- [17]
						false, -- [18]
						false, -- [19]
						false, -- [20]
						false, -- [21]
						false, -- [22]
						true, -- [23]
						true, -- [24]
						false, -- [25]
						false, -- [26]
						false, -- [27]
						false, -- [28]
						false, -- [29]
						false, -- [30]
						false, -- [31]
						false, -- [32]
						false, -- [33]
						false, -- [34]
						true, -- [35]
						false, -- [36]
						false, -- [37]
						false, -- [38]
						false, -- [39]
						false, -- [40]
						false, -- [41]
						false, -- [42]
						true, -- [43]
						true, -- [44]
					},
					["Messages"] = {
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39609 (Fire)", -- [1]
						"Falriçk Dragonsfire Grenade (DoT) Chimaeron Tick -91960 (Fire)", -- [2]
						"Falriçk Melee Chimaeron Hit -63620 (Physical)", -- [3]
						"Falriçk Lacerate (DoT) Chimaeron Tick -51749 (Physical)", -- [4]
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39610 (Fire)", -- [5]
						"Falriçk Dragonsfire Grenade (DoT) Chimaeron Tick -91960 (Fire)", -- [6]
						"Falriçk Lacerate (DoT) Chimaeron Tick -51748 (Physical)", -- [7]
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39610 (Fire)", -- [8]
						"Falriçk Hatchet Toss Chimaeron Hit -5768 (Physical)", -- [9]
						"Chimaeron Melee Falriçk Hit -6294 (Physical)", -- [10]
						"Falriçk Melee Chimaeron Hit -63585 (Physical)", -- [11]
						"Falriçk Lacerate (DoT) Chimaeron Tick -51749 (Physical)", -- [12]
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39610 (Fire)", -- [13]
						"Falriçk Lacerate (DoT) Chimaeron Crit -103498 (Physical)", -- [14]
						"Falriçk Carve Chimaeron Hit -75509 (Physical)", -- [15]
						"Falriçk Explosive Trap (DoT) Chimaeron Crit -79220 (Fire)", -- [16]
						"Falriçk Lacerate (DoT) Chimaeron Tick -51749 (Physical)", -- [17]
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39610 (Fire)", -- [18]
						"Falriçk Melee Chimaeron Hit -65424 (Physical)", -- [19]
						"Falriçk Carve Chimaeron Crit -141537 (Physical)", -- [20]
						"Falriçk Lacerate (DoT) Chimaeron Tick -51749 (Physical)", -- [21]
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39610 (Fire)", -- [22]
						"Chimaeron Melee Falriçk Hit -7775 (Physical)", -- [23]
						"Chimaeron Melee (Double Attack) Falriçk Hit -8030 (Physical)", -- [24]
						"Falriçk Lacerate (DoT) Chimaeron Tick -51749 (Physical)", -- [25]
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39610 (Fire)", -- [26]
						"Falriçk Mongoose Bite Chimaeron Crit -346972 (Physical)", -- [27]
						"Falriçk Melee Chimaeron Crit -128849 (Physical)", -- [28]
						"Falriçk Lacerate (DoT) Chimaeron Crit -103498 (Physical)", -- [29]
						"Falriçk Explosive Trap (DoT) Chimaeron Tick -39610 (Fire)", -- [30]
						"Falriçk Lacerate (DoT) Chimaeron Crit -103498 (Physical)", -- [31]
						"Falriçk Carve Chimaeron Hit -72277 (Physical)", -- [32]
						"Falriçk Lacerate (DoT) Chimaeron Crit -103498 (Physical)", -- [33]
						"Falriçk Melee Chimaeron Hit -65296 (Physical)", -- [34]
						"Chimaeron Melee Falriçk Hit -9423 (Physical)", -- [35]
						"Falriçk Lacerate (DoT) Chimaeron Crit -103498 (Physical)", -- [36]
						"Falriçk Lacerate (DoT) Chimaeron Tick -51749 (Physical)", -- [37]
						"Falriçk Carve Chimaeron Hit -76142 (Physical)", -- [38]
						"Falriçk Melee Chimaeron Hit -65056 (Physical)", -- [39]
						"Falriçk Lacerate (DoT) Chimaeron Crit -103498 (Physical)", -- [40]
						"Falriçk Muzzle Chimaeron Immune (Physical)", -- [41]
						"Falriçk Hatchet Toss Chimaeron Hit -5769 (Physical)", -- [42]
						"Chimaeron Massacre Falriçk Hit -999999 (Physical)", -- [43]
						"Falriçk dies.", -- [44]
					},
					["DeathAt"] = 399456.364,
					["HealthNum"] = {
						92.0502860584161, -- [1]
						92.0502860584161, -- [2]
						92.0502860584161, -- [3]
						92.0502860584161, -- [4]
						92.0502860584161, -- [5]
						92.0502860584161, -- [6]
						92.0502860584161, -- [7]
						92.0502860584161, -- [8]
						92.0502860584161, -- [9]
						92.0502860584161, -- [10]
						90.15507377296, -- [11]
						90.15507377296, -- [12]
						90.15507377296, -- [13]
						90.15507377296, -- [14]
						90.15507377296, -- [15]
						90.15507377296, -- [16]
						90.15507377296, -- [17]
						90.15507377296, -- [18]
						90.15507377296, -- [19]
						90.15507377296, -- [20]
						90.15507377296, -- [21]
						90.15507377296, -- [22]
						90.15507377296, -- [23]
						90.15507377296, -- [24]
						85.3959650707618, -- [25]
						85.3959650707618, -- [26]
						85.3959650707618, -- [27]
						85.3959650707618, -- [28]
						85.3959650707618, -- [29]
						85.3959650707618, -- [30]
						85.3959650707618, -- [31]
						85.3959650707618, -- [32]
						85.3959650707618, -- [33]
						85.3959650707618, -- [34]
						85.3959650707618, -- [35]
						82.5585666967781, -- [36]
						82.5585666967781, -- [37]
						82.5585666967781, -- [38]
						82.5585666967781, -- [39]
						82.5585666967781, -- [40]
						82.5585666967781, -- [41]
						82.5585666967781, -- [42]
						0.000301114122252334, -- [43]
						0, -- [44]
					},
					["MessageTimes"] = {
						-14.99099999998, -- [1]
						-14.8039999999455, -- [2]
						-14.7369999999646, -- [3]
						-14.3759999999893, -- [4]
						-13.9949999999953, -- [5]
						-13.7880000000005, -- [6]
						-13.0760000000009, -- [7]
						-12.9939999999478, -- [8]
						-12.5159999999451, -- [9]
						-12.4510000000009, -- [10]
						-12.2519999999786, -- [11]
						-12.0879999999888, -- [12]
						-11.9929999999586, -- [13]
						-11.0839999999735, -- [14]
						-11.0469999999623, -- [15]
						-10.9919999999693, -- [16]
						-10.0839999999735, -- [17]
						-9.99099999997998, -- [18]
						-9.75999999995111, -- [19]
						-9.43399999995017, -- [20]
						-9.08499999996275, -- [21]
						-8.99399999994785, -- [22]
						-8.44999999995343, -- [23]
						-8.44999999995343, -- [24]
						-8.08899999997811, -- [25]
						-7.99399999994785, -- [26]
						-7.52399999997579, -- [27]
						-7.27499999996508, -- [28]
						-7.08099999994738, -- [29]
						-6.9890000000014, -- [30]
						-6.07699999999022, -- [31]
						-5.82199999998557, -- [32]
						-5.07799999997951, -- [33]
						-4.77599999995437, -- [34]
						-4.45100000000093, -- [35]
						-4.08799999998882, -- [36]
						-3.08799999998882, -- [37]
						-2.94799999997485, -- [38]
						-2.28800000000047, -- [39]
						-2.07999999995809, -- [40]
						-0.8629999999539, -- [41]
						-0.118999999947846, -- [42]
						-0.018999999971129, -- [43]
						0, -- [44]
					},
					["KilledBy"] = "Chimaeron",
					["Health"] = {
						"305699 (92%)", -- [1]
						"305699 (92%)", -- [2]
						"305699 (92%)", -- [3]
						"305699 (92%)", -- [4]
						"305699 (92%)", -- [5]
						"305699 (92%)", -- [6]
						"305699 (92%)", -- [7]
						"305699 (92%)", -- [8]
						"305699 (92%)", -- [9]
						"305699 (92%)", -- [10]
						"299405 (90%)", -- [11]
						"299405 (90%)", -- [12]
						"299405 (90%)", -- [13]
						"299405 (90%)", -- [14]
						"299405 (90%)", -- [15]
						"299405 (90%)", -- [16]
						"299405 (90%)", -- [17]
						"299405 (90%)", -- [18]
						"299405 (90%)", -- [19]
						"299405 (90%)", -- [20]
						"299405 (90%)", -- [21]
						"299405 (90%)", -- [22]
						"299405 (90%)", -- [23]
						"299405 (90%)", -- [24]
						"283600 (85%)", -- [25]
						"283600 (85%)", -- [26]
						"283600 (85%)", -- [27]
						"283600 (85%)", -- [28]
						"283600 (85%)", -- [29]
						"283600 (85%)", -- [30]
						"283600 (85%)", -- [31]
						"283600 (85%)", -- [32]
						"283600 (85%)", -- [33]
						"283600 (85%)", -- [34]
						"283600 (85%)", -- [35]
						"274177 (82%)", -- [36]
						"274177 (82%)", -- [37]
						"274177 (82%)", -- [38]
						"274177 (82%)", -- [39]
						"274177 (82%)", -- [40]
						"274177 (82%)", -- [41]
						"274177 (82%)", -- [42]
						"1 (0%)", -- [43]
						"0 (0%)", -- [44]
					},
					["EventNum"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0, -- [4]
						0, -- [5]
						0, -- [6]
						0, -- [7]
						0, -- [8]
						0, -- [9]
						1.89521228545619, -- [10]
						0, -- [11]
						0, -- [12]
						0, -- [13]
						0, -- [14]
						0, -- [15]
						0, -- [16]
						0, -- [17]
						0, -- [18]
						0, -- [19]
						0, -- [20]
						0, -- [21]
						0, -- [22]
						2.34116230051189, -- [23]
						2.41794640168624, -- [24]
						0, -- [25]
						0, -- [26]
						0, -- [27]
						0, -- [28]
						0, -- [29]
						0, -- [30]
						0, -- [31]
						0, -- [32]
						0, -- [33]
						0, -- [34]
						2.83739837398374, -- [35]
						0, -- [36]
						0, -- [37]
						0, -- [38]
						0, -- [39]
						0, -- [40]
						0, -- [41]
						0, -- [42]
						301.113821138211, -- [43]
						0, -- [44]
					},
					["MessageType"] = {
						"DAMAGE", -- [1]
						"DAMAGE", -- [2]
						"DAMAGE", -- [3]
						"DAMAGE", -- [4]
						"DAMAGE", -- [5]
						"DAMAGE", -- [6]
						"DAMAGE", -- [7]
						"DAMAGE", -- [8]
						"DAMAGE", -- [9]
						"DAMAGE", -- [10]
						"DAMAGE", -- [11]
						"DAMAGE", -- [12]
						"DAMAGE", -- [13]
						"DAMAGE", -- [14]
						"DAMAGE", -- [15]
						"DAMAGE", -- [16]
						"DAMAGE", -- [17]
						"DAMAGE", -- [18]
						"DAMAGE", -- [19]
						"DAMAGE", -- [20]
						"DAMAGE", -- [21]
						"DAMAGE", -- [22]
						"DAMAGE", -- [23]
						"DAMAGE", -- [24]
						"DAMAGE", -- [25]
						"DAMAGE", -- [26]
						"DAMAGE", -- [27]
						"DAMAGE", -- [28]
						"DAMAGE", -- [29]
						"DAMAGE", -- [30]
						"DAMAGE", -- [31]
						"DAMAGE", -- [32]
						"DAMAGE", -- [33]
						"DAMAGE", -- [34]
						"DAMAGE", -- [35]
						"DAMAGE", -- [36]
						"DAMAGE", -- [37]
						"DAMAGE", -- [38]
						"DAMAGE", -- [39]
						"DAMAGE", -- [40]
						"DAMAGE", -- [41]
						"DAMAGE", -- [42]
						"DAMAGE", -- [43]
						"MISC", -- [44]
					},
				}, -- [1]
				{
					["MessageIncoming"] = {
						false, -- [1]
						false, -- [2]
						false, -- [3]
						true, -- [4]
						false, -- [5]
						false, -- [6]
						false, -- [7]
						false, -- [8]
						false, -- [9]
						false, -- [10]
						false, -- [11]
						false, -- [12]
						false, -- [13]
						false, -- [14]
						false, -- [15]
						false, -- [16]
						false, -- [17]
						false, -- [18]
						false, -- [19]
						false, -- [20]
						false, -- [21]
						true, -- [22]
						true, -- [23]
						false, -- [24]
						false, -- [25]
						true, -- [26]
						false, -- [27]
						false, -- [28]
						false, -- [29]
						false, -- [30]
						false, -- [31]
						true, -- [32]
						true, -- [33]
					},
					["Messages"] = {
						"Falriçk Cobra Shot Chimaeron Crit -486785 (Physical)", -- [1]
						"Falriçk Auto Shot Chimaeron Crit -150091 (Physical)", -- [2]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [3]
						"Chimaeron Melee Falriçk Hit -7136 (Physical)", -- [4]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [5]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [6]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [7]
						"Falriçk Barrage Chimaeron Crit -143148 (Physical)", -- [8]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [9]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [10]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [11]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [12]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [13]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [14]
						"Falriçk Barrage Chimaeron Crit -143148 (Physical)", -- [15]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [16]
						"Falriçk Barrage Chimaeron Crit -143148 (Physical)", -- [17]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [18]
						"Falriçk Barrage Chimaeron Hit -71574 (Physical)", -- [19]
						"Falriçk Auto Shot Chimaeron Hit -75045 (Physical)", -- [20]
						"Falriçk Cobra Shot Chimaeron Hit -243392 (Physical)", -- [21]
						"Chimaeron Melee Falriçk Hit -9363 (Physical)", -- [22]
						"Chimaeron Melee (Double Attack) Falriçk Hit -9100 (Physical)", -- [23]
						"Falriçk Auto Shot Chimaeron Hit -75045 (Physical)", -- [24]
						"Falriçk Auto Shot Chimaeron Hit -75045 (Physical)", -- [25]
						"Chimaeron Melee Falriçk Hit -8226 (Physical)", -- [26]
						"Falriçk Cobra Shot Chimaeron Hit -243393 (Physical)", -- [27]
						"Falriçk Cobra Shot Chimaeron Hit -243392 (Physical)", -- [28]
						"Falriçk Auto Shot Chimaeron Hit -75046 (Physical)", -- [29]
						"Falriçk Auto Shot Chimaeron Hit -75046 (Physical)", -- [30]
						"Falriçk Cobra Shot Chimaeron Crit -486785 (Physical)", -- [31]
						"Chimaeron Massacre Falriçk Hit -999999 (Physical)", -- [32]
						"Falriçk dies.", -- [33]
					},
					["DeathAt"] = 399295.362,
					["HealthNum"] = {
						90.2990137392395, -- [1]
						90.2990137392395, -- [2]
						90.2990137392395, -- [3]
						90.2990137392395, -- [4]
						90.2990137392395, -- [5]
						88.2649221823157, -- [6]
						88.2649221823157, -- [7]
						88.2649221823157, -- [8]
						88.2649221823157, -- [9]
						88.2649221823157, -- [10]
						88.2649221823157, -- [11]
						88.2649221823157, -- [12]
						88.2649221823157, -- [13]
						88.2649221823157, -- [14]
						88.2649221823157, -- [15]
						88.2649221823157, -- [16]
						88.2649221823157, -- [17]
						88.2649221823157, -- [18]
						88.2649221823157, -- [19]
						88.2649221823157, -- [20]
						88.2649221823157, -- [21]
						88.2649221823157, -- [22]
						88.2649221823157, -- [23]
						83.0021093438231, -- [24]
						83.0021093438231, -- [25]
						83.0021093438231, -- [26]
						80.6573171426943, -- [27]
						80.6573171426943, -- [28]
						80.6573171426943, -- [29]
						80.6573171426943, -- [30]
						80.6573171426943, -- [31]
						0.000285046462573399, -- [32]
						0, -- [33]
					},
					["MessageTimes"] = {
						-14.6429999999818, -- [1]
						-13.8730000000214, -- [2]
						-13.1840000000084, -- [3]
						-13.0079999999725, -- [4]
						-13.0079999999725, -- [5]
						-12.8400000000256, -- [6]
						-12.6809999999823, -- [7]
						-12.5219999999972, -- [8]
						-12.36599999998, -- [9]
						-12.2079999999842, -- [10]
						-12.0489999999991, -- [11]
						-11.8809999999939, -- [12]
						-11.7050000000163, -- [13]
						-11.5370000000112, -- [14]
						-11.3939999999711, -- [15]
						-11.2270000000135, -- [16]
						-11.0520000000251, -- [17]
						-10.9049999999697, -- [18]
						-10.7299999999814, -- [19]
						-10.5910000000149, -- [20]
						-10.164999999979, -- [21]
						-9.01799999998184, -- [22]
						-9.01799999998184, -- [23]
						-8.14399999997113, -- [24]
						-5.69500000000699, -- [25]
						-5, -- [26]
						-4.49400000000605, -- [27]
						-3.28299999999581, -- [28]
						-3.24300000001676, -- [29]
						-0.754000000015367, -- [30]
						-0.25, -- [31]
						-0.0310000000172295, -- [32]
						0, -- [33]
					},
					["KilledBy"] = "Chimaeron",
					["Health"] = {
						"316787 (90%)", -- [1]
						"316787 (90%)", -- [2]
						"316787 (90%)", -- [3]
						"316787 (90%)", -- [4]
						"316787 (90%)", -- [5]
						"309651 (88%)", -- [6]
						"309651 (88%)", -- [7]
						"309651 (88%)", -- [8]
						"309651 (88%)", -- [9]
						"309651 (88%)", -- [10]
						"309651 (88%)", -- [11]
						"309651 (88%)", -- [12]
						"309651 (88%)", -- [13]
						"309651 (88%)", -- [14]
						"309651 (88%)", -- [15]
						"309651 (88%)", -- [16]
						"309651 (88%)", -- [17]
						"309651 (88%)", -- [18]
						"309651 (88%)", -- [19]
						"309651 (88%)", -- [20]
						"309651 (88%)", -- [21]
						"309651 (88%)", -- [22]
						"309651 (88%)", -- [23]
						"291188 (83%)", -- [24]
						"291188 (83%)", -- [25]
						"291188 (83%)", -- [26]
						"282962 (80%)", -- [27]
						"282962 (80%)", -- [28]
						"282962 (80%)", -- [29]
						"282962 (80%)", -- [30]
						"282962 (80%)", -- [31]
						"1 (0%)", -- [32]
						"0 (0%)", -- [33]
					},
					["EventNum"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						2.03409155692378, -- [4]
						0, -- [5]
						0, -- [6]
						0, -- [7]
						0, -- [8]
						0, -- [9]
						0, -- [10]
						0, -- [11]
						0, -- [12]
						0, -- [13]
						0, -- [14]
						0, -- [15]
						0, -- [16]
						0, -- [17]
						0, -- [18]
						0, -- [19]
						0, -- [20]
						0, -- [21]
						2.66889002907474, -- [22]
						2.59392280941794, -- [23]
						0, -- [24]
						0, -- [25]
						2.34479220112878, -- [26]
						0, -- [27]
						0, -- [28]
						0, -- [29]
						0, -- [30]
						0, -- [31]
						285.046177526937, -- [32]
						0, -- [33]
					},
					["MessageType"] = {
						"DAMAGE", -- [1]
						"DAMAGE", -- [2]
						"DAMAGE", -- [3]
						"DAMAGE", -- [4]
						"DAMAGE", -- [5]
						"DAMAGE", -- [6]
						"DAMAGE", -- [7]
						"DAMAGE", -- [8]
						"DAMAGE", -- [9]
						"DAMAGE", -- [10]
						"DAMAGE", -- [11]
						"DAMAGE", -- [12]
						"DAMAGE", -- [13]
						"DAMAGE", -- [14]
						"DAMAGE", -- [15]
						"DAMAGE", -- [16]
						"DAMAGE", -- [17]
						"DAMAGE", -- [18]
						"DAMAGE", -- [19]
						"DAMAGE", -- [20]
						"DAMAGE", -- [21]
						"DAMAGE", -- [22]
						"DAMAGE", -- [23]
						"DAMAGE", -- [24]
						"DAMAGE", -- [25]
						"DAMAGE", -- [26]
						"DAMAGE", -- [27]
						"DAMAGE", -- [28]
						"DAMAGE", -- [29]
						"DAMAGE", -- [30]
						"DAMAGE", -- [31]
						"DAMAGE", -- [32]
						"MISC", -- [33]
					},
				}, -- [2]
			},
			["TimeLast"] = {
				["TimeHeal"] = 399645.362,
				["OVERALL"] = 399667.36,
				["DamageTaken"] = 399666.357,
				["EnergyGain"] = 399208.358,
				["DeathCount"] = 399454.362,
				["HealingTaken"] = 399645.362,
				["TimeDamage"] = 399661.362,
				["Healing"] = 399667.36,
				["ActiveTime"] = 399661.362,
				["DOT_Time"] = 399628.363,
				["Damage"] = 399667.36,
			},
			["LastAttackedBy"] = "Chimaeron",
			["LastEventType"] = {
				"HEAL", -- [1]
				"DAMAGE", -- [2]
				"DAMAGE", -- [3]
				"DAMAGE", -- [4]
				"DAMAGE", -- [5]
				"DAMAGE", -- [6]
				"DAMAGE", -- [7]
				"DAMAGE", -- [8]
				"DAMAGE", -- [9]
				"DAMAGE", -- [10]
				"DAMAGE", -- [11]
				"DAMAGE", -- [12]
				"DAMAGE", -- [13]
				"DAMAGE", -- [14]
				"DAMAGE", -- [15]
				"DAMAGE", -- [16]
				"DAMAGE", -- [17]
				"DAMAGE", -- [18]
				"DAMAGE", -- [19]
				"DAMAGE", -- [20]
				"HEAL", -- [21]
				"DAMAGE", -- [22]
				"DAMAGE", -- [23]
				"DAMAGE", -- [24]
				"DAMAGE", -- [25]
				"DAMAGE", -- [26]
				"DAMAGE", -- [27]
				"DAMAGE", -- [28]
				"DAMAGE", -- [29]
				"DAMAGE", -- [30]
				"DAMAGE", -- [31]
				"DAMAGE", -- [32]
				"DAMAGE", -- [33]
				"DAMAGE", -- [34]
				"DAMAGE", -- [35]
				"DAMAGE", -- [36]
				"DAMAGE", -- [37]
				"DAMAGE", -- [38]
				"DAMAGE", -- [39]
				"DAMAGE", -- [40]
				"DAMAGE", -- [41]
				"DAMAGE", -- [42]
				"DAMAGE", -- [43]
				"DAMAGE", -- [44]
				"DAMAGE", -- [45]
				"DAMAGE", -- [46]
				"DAMAGE", -- [47]
				"DAMAGE", -- [48]
				"DAMAGE", -- [49]
				"DAMAGE", -- [50]
			},
			["TimeWindows"] = {
				["TimeHeal"] = {
					3.5, -- [1]
				},
				["Healing"] = {
					99630, -- [1]
				},
				["DamageTaken"] = {
					3447618, -- [1]
				},
				["EnergyGain"] = {
					1350, -- [1]
				},
				["DeathCount"] = {
					2, -- [1]
				},
				["TimeDamage"] = {
					325.52, -- [1]
				},
				["HealingTaken"] = {
					99630, -- [1]
				},
				["ActiveTime"] = {
					329.02, -- [1]
				},
				["DOT_Time"] = {
					192, -- [1]
				},
				["Damage"] = {
					104805840, -- [1]
				},
			},
			["enClass"] = "HUNTER",
			["LastDamageTaken"] = 9501,
			["LastAbility"] = 399661.452,
			["GUID"] = "Player-76-08C3D1D4",
			["level"] = 100,
			["LastDamageAbility"] = "Melee (Double Attack)",
			["LastFightIn"] = 12,
			["LastEventNum"] = {
				30, -- [1]
				[50] = 87.4441433303222,
				[14] = 2.55404998494429,
				[21] = 50,
				[35] = 1.97560975609756,
				[11] = 3.48358928033725,
				[18] = 3.09153869316471,
				[49] = 213.669677807889,
				[19] = 4.01505570611262,
				[20] = 2.86088527551942,
				[3] = 3.15778380006022,
				[6] = 2.52845528455285,
				[12] = 6.66214995483288,
				[42] = 2.29659741041855,
			},
			["type"] = "Self",
			["FightsSaved"] = 5,
			["LastEventHealth"] = {
				"99631 (30%)", -- [1]
				"99631 (30%)", -- [2]
				"99631 (30%)", -- [3]
				"89144 (26%)", -- [4]
				"89144 (26%)", -- [5]
				"89144 (26%)", -- [6]
				"80747 (24%)", -- [7]
				"80747 (24%)", -- [8]
				"80747 (24%)", -- [9]
				"80747 (24%)", -- [10]
				"80747 (24%)", -- [11]
				"80747 (24%)", -- [12]
				"47053 (14%)", -- [13]
				"47053 (14%)", -- [14]
				"38571 (11%)", -- [15]
				"38571 (11%)", -- [16]
				"38571 (11%)", -- [17]
				"38571 (11%)", -- [18]
				"28304 (8%)", -- [19]
				"28304 (8%)", -- [20]
				"240744 (69%)", -- [21]
				"304591 (91%)", -- [22]
				"304591 (91%)", -- [23]
				"304591 (91%)", -- [24]
				"304591 (91%)", -- [25]
				"304591 (91%)", -- [26]
				"304591 (91%)", -- [27]
				"304591 (91%)", -- [28]
				"304591 (91%)", -- [29]
				"304591 (91%)", -- [30]
				"304591 (91%)", -- [31]
				"304591 (91%)", -- [32]
				"304591 (91%)", -- [33]
				"304591 (91%)", -- [34]
				"304591 (91%)", -- [35]
				"298030 (89%)", -- [36]
				"298030 (89%)", -- [37]
				"298030 (89%)", -- [38]
				"298030 (89%)", -- [39]
				"298030 (89%)", -- [40]
				"298030 (89%)", -- [41]
				"298030 (89%)", -- [42]
				"290403 (87%)", -- [43]
				"290403 (87%)", -- [44]
				"290403 (87%)", -- [45]
				"290403 (87%)", -- [46]
				"290403 (87%)", -- [47]
				"290403 (87%)", -- [48]
				"1 (0%)", -- [49]
				"1 (0%)", -- [50]
			},
			["unit"] = "Falriçk",
			["Owner"] = false,
			["Pet"] = {
				"Hydra <Falriçk>", -- [1]
			},
			["NextEventNum"] = 22,
			["LastEventHealthNum"] = {
				30.0003011141223, -- [1]
				30.0003011141223, -- [2]
				30.0003011141223, -- [3]
				26.842517314062, -- [4]
				26.842517314062, -- [5]
				26.842517314062, -- [6]
				24.3140620295092, -- [7]
				24.3140620295092, -- [8]
				24.3140620295092, -- [9]
				24.3140620295092, -- [10]
				24.3140620295092, -- [11]
				24.3140620295092, -- [12]
				14.1683227943391, -- [13]
				14.1683227943391, -- [14]
				11.6142728093948, -- [15]
				11.6142728093948, -- [16]
				11.6142728093948, -- [17]
				11.6142728093948, -- [18]
				8.52273411623005, -- [19]
				8.52273411623005, -- [20]
				69.8172959805116, -- [21]
				91.7166516109606, -- [22]
				91.7166516109606, -- [23]
				91.7166516109606, -- [24]
				91.7166516109606, -- [25]
				91.7166516109606, -- [26]
				91.7166516109606, -- [27]
				91.7166516109606, -- [28]
				91.7166516109606, -- [29]
				91.7166516109606, -- [30]
				91.7166516109606, -- [31]
				91.7166516109606, -- [32]
				91.7166516109606, -- [33]
				91.7166516109606, -- [34]
				91.7166516109606, -- [35]
				89.741041854863, -- [36]
				89.741041854863, -- [37]
				89.741041854863, -- [38]
				89.741041854863, -- [39]
				89.741041854863, -- [40]
				89.741041854863, -- [41]
				89.741041854863, -- [42]
				87.4444444444445, -- [43]
				87.4444444444445, -- [44]
				87.4444444444445, -- [45]
				87.4444444444445, -- [46]
				87.4444444444445, -- [47]
				87.4444444444445, -- [48]
				0.000301114122252334, -- [49]
				0.000301114122252334, -- [50]
			},
			["LastEvents"] = {
				"Falriçk Exhilaration Falriçk Hit +99630", -- [1]
				"Falriçk Melee Chimaeron Hit -62762 (Physical)", -- [2]
				"Chimaeron Melee Falriçk Hit -10487 (Physical)", -- [3]
				"Falriçk Mongoose Bite Chimaeron Hit -172131 (Physical)", -- [4]
				"Falriçk Melee Chimaeron Hit -61804 (Physical)", -- [5]
				"Chimaeron Melee Falriçk Hit -8397 (Physical)", -- [6]
				"Falriçk Mongoose Bite Chimaeron Crit -513463 (Physical)", -- [7]
				"Falriçk Melee Chimaeron Parry (1)", -- [8]
				"Falriçk Melee Chimaeron Hit -62430 (Physical)", -- [9]
				"Falriçk Raptor Strike Chimaeron Crit -199842 (Physical)", -- [10]
				"Chimaeron Melee Falriçk Hit -11569 (Physical)", -- [11]
				"Chimaeron Melee (Double Attack) Falriçk Crit -22125 (Physical)", -- [12]
				"Falriçk Melee Chimaeron Hit -62900 (Physical)", -- [13]
				"Chimaeron Melee Falriçk Hit -8482 (Physical)", -- [14]
				"Falriçk Melee Chimaeron Hit -62149 (Physical)", -- [15]
				"Falriçk Mongoose Bite Chimaeron Hit -360494 (Physical)", -- [16]
				"Falriçk Melee Chimaeron Crit -123978 (Physical)", -- [17]
				"Chimaeron Melee Falriçk Hit -10267 (Physical)", -- [18]
				"Chimaeron Melee Falriçk Hit -13334 (Physical)", -- [19]
				"Chimaeron Melee (Double Attack) Falriçk Hit -9501 (Physical)", -- [20]
				"Falriçk Exhilaration Falriçk Hit +172410", -- [21]
				"Falriçk Melee Chimaeron Hit -66577 (Physical)", -- [22]
				"Falriçk Lacerate (DoT) Chimaeron Tick -53303 (Physical)", -- [23]
				"Falriçk Raptor Strike Chimaeron Hit -102963 (Physical)", -- [24]
				"Falriçk Lacerate (DoT) Chimaeron Tick -54856 (Physical)", -- [25]
				"Falriçk Melee Chimaeron Crit -136171 (Physical)", -- [26]
				"Chimaeron Melee Falriçk Dodge (1)", -- [27]
				"Falriçk Lacerate (DoT) Chimaeron Crit -109713 (Physical)", -- [28]
				"Falriçk Raptor Strike Chimaeron Crit -221794 (Physical)", -- [29]
				"Falriçk Lacerate (DoT) Chimaeron Tick -56400 (Physical)", -- [30]
				"Falriçk Lacerate (DoT) Chimaeron Tick -56400 (Physical)", -- [31]
				"Falriçk Melee Chimaeron Parry (1)", -- [32]
				"Falriçk Carve Chimaeron Hit -80953 (Physical)", -- [33]
				"Falriçk Lacerate (DoT) Chimaeron Tick -56400 (Physical)", -- [34]
				"Chimaeron Melee Falriçk Hit -6561 (Physical)", -- [35]
				"Falriçk Lacerate (DoT) Chimaeron Crit -112799 (Physical)", -- [36]
				"Falriçk Mongoose Bite Chimaeron Crit -896461 (Physical)", -- [37]
				"Falriçk Melee Chimaeron Crit -134569 (Physical)", -- [38]
				"Falriçk Raptor Strike Chimaeron Hit -111841 (Physical)", -- [39]
				"Falriçk Melee Chimaeron Hit -67269 (Physical)", -- [40]
				"Chimaeron Melee Falriçk Dodge (1)", -- [41]
				"Chimaeron Melee (Double Attack) Falriçk Hit -7627 (Physical)", -- [42]
				"Falriçk Raptor Strike Chimaeron Hit -114349 (Physical)", -- [43]
				"Falriçk Melee Chimaeron Hit -66675 (Physical)", -- [44]
				"Falriçk Raptor Strike Chimaeron Parry (Physical)", -- [45]
				"Falriçk Raptor Strike Chimaeron Hit -80548 (Physical)", -- [46]
				"Chimaeron Melee Falriçk Dodge (1)", -- [47]
				"Falriçk Melee Chimaeron Hit -70421 (Physical)", -- [48]
				"Chimaeron Massacre Falriçk Absorb -709597 (Physical)", -- [49]
				"Chimaeron Massacre Falriçk Hit -290402 (709597 Absorbed) (Physical)", -- [50]
			},
			["LastEventIncoming"] = {
				true, -- [1]
				false, -- [2]
				true, -- [3]
				false, -- [4]
				false, -- [5]
				true, -- [6]
				false, -- [7]
				false, -- [8]
				false, -- [9]
				false, -- [10]
				true, -- [11]
				true, -- [12]
				false, -- [13]
				true, -- [14]
				false, -- [15]
				false, -- [16]
				false, -- [17]
				true, -- [18]
				true, -- [19]
				true, -- [20]
				true, -- [21]
				false, -- [22]
				false, -- [23]
				false, -- [24]
				false, -- [25]
				false, -- [26]
				true, -- [27]
				false, -- [28]
				false, -- [29]
				false, -- [30]
				false, -- [31]
				false, -- [32]
				false, -- [33]
				false, -- [34]
				true, -- [35]
				false, -- [36]
				false, -- [37]
				false, -- [38]
				false, -- [39]
				false, -- [40]
				true, -- [41]
				true, -- [42]
				false, -- [43]
				false, -- [44]
				false, -- [45]
				false, -- [46]
				true, -- [47]
				false, -- [48]
				true, -- [49]
				true, -- [50]
			},
			["Name"] = "Falriçk",
			["LastEventTimes"] = {
				399645.372, -- [1]
				399646.509, -- [2]
				399646.604, -- [3]
				399648.086, -- [4]
				399649.007, -- [5]
				399650.601, -- [6]
				399650.788, -- [7]
				399651.488, -- [8]
				399653.978, -- [9]
				399654.069, -- [10]
				399654.621, -- [11]
				399654.621, -- [12]
				399656.47, -- [13]
				399658.619, -- [14]
				399658.961, -- [15]
				399659.63, -- [16]
				399661.452, -- [17]
				399662.628, -- [18]
				399666.636, -- [19]
				399666.636, -- [20]
				399703.707, -- [21]
				399622.511, -- [22]
				399623.345, -- [23]
				399623.582, -- [24]
				399624.346, -- [25]
				399624.998, -- [26]
				399625.159, -- [27]
				399625.341, -- [28]
				399625.508, -- [29]
				399626.342, -- [30]
				399627.349, -- [31]
				399627.481, -- [32]
				399628.066, -- [33]
				399628.345, -- [34]
				399629.159, -- [35]
				399629.349, -- [36]
				399629.781, -- [37]
				399629.995, -- [38]
				399631.048, -- [39]
				399632.464, -- [40]
				399633.183, -- [41]
				399633.183, -- [42]
				399633.821, -- [43]
				399634.949, -- [44]
				399635.211, -- [45]
				399636.99, -- [46]
				399637.176, -- [47]
				399637.455, -- [48]
				399642.09, -- [49]
				399642.09, -- [50]
			},
			["Fights"] = {
				["Fight5"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 3,
								},
							},
							["amount"] = 3,
						},
						["Nature"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 2,
								},
							},
							["amount"] = 2,
						},
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
						["Nature"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 3,
								},
								["Hit"] = {
									["count"] = 7,
								},
							},
							["amount"] = 10,
						},
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Fire"] = 0,
					},
					["DOT_Time"] = 0,
					["Damage"] = 3961574,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
						["Nature"] = 241094,
						["Physical"] = 3720480,
					},
					["PartialAbsorb"] = {
						["Magma Spit"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Chain Lightning"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
						["Stormbolt"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Spirit of Shadowforge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 241094,
								},
								["Marked Shot"] = {
									["count"] = 855631,
								},
							},
							["amount"] = 1096725,
						},
						["Spirit of Moltenfist"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 613088,
								},
								["Auto Shot"] = {
									["count"] = 177678,
								},
							},
							["amount"] = 790766,
						},
						["Spirit of Anvilrage"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 177678,
								},
								["Aimed Shot"] = {
									["count"] = 863097,
								},
							},
							["amount"] = 1040775,
						},
						["Spirit of Corehammer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 177678,
								},
								["Marked Shot"] = {
									["count"] = 855630,
								},
							},
							["amount"] = 1033308,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Magmaw"] = {
							["Details"] = {
								["Magma Spit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["PartialResist"] = {
						["Magma Spit"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Chain Lightning"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
						["Stormbolt"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 50,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
						["Spirit of Shadowforge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.59,
								},
								["Marked Shot"] = {
									["count"] = 0.2,
								},
							},
							["amount"] = 0.79,
						},
						["Spirit of Moltenfist"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 2.63,
								},
								["Auto Shot"] = {
									["count"] = 2.5,
								},
							},
							["amount"] = 5.13,
						},
						["Spirit of Anvilrage"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.42,
								},
								["Aimed Shot"] = {
									["count"] = 0.88,
								},
							},
							["amount"] = 4.3,
						},
						["Spirit of Corehammer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.05,
								},
								["Marked Shot"] = {
									["count"] = 1.85,
								},
							},
							["amount"] = 4.9,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["ActiveTime"] = 15.12,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 241094,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 241094,
								},
							},
							["count"] = 1,
							["amount"] = 241094,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 613088,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 1183007,
								},
								["Hit"] = {
									["max"] = 293178,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 293178,
								},
							},
							["count"] = 3,
							["amount"] = 1476185,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 855631,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 1711261,
								},
							},
							["count"] = 2,
							["amount"] = 1711261,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 177678,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 177678,
								},
								["Hit"] = {
									["max"] = 88839,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 355356,
								},
							},
							["count"] = 5,
							["amount"] = 533034,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 15.12,
					["TimeDamaging"] = {
						["Spirit of Shadowforge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.59,
								},
								["Marked Shot"] = {
									["count"] = 0.2,
								},
							},
							["amount"] = 0.79,
						},
						["Spirit of Moltenfist"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 2.63,
								},
								["Auto Shot"] = {
									["count"] = 2.5,
								},
							},
							["amount"] = 5.13,
						},
						["Spirit of Anvilrage"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.42,
								},
								["Aimed Shot"] = {
									["count"] = 0.88,
								},
							},
							["amount"] = 4.3,
						},
						["Spirit of Corehammer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.05,
								},
								["Marked Shot"] = {
									["count"] = 1.85,
								},
							},
							["amount"] = 4.9,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["Fight2"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 1675,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Physical"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
						["Fire"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
						["Nature"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 2,
								},
								["Hit"] = {
									["count"] = 3,
								},
							},
							["amount"] = 5,
						},
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Melee"] = 1675,
					},
					["DOT_Time"] = 0,
					["Damage"] = 1773874,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
						["Nature"] = 241094,
						["Physical"] = 1532780,
					},
					["PartialAbsorb"] = {
						["Charge"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Flame Buffet"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Pyrecraw"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 266517,
								},
								["Barrage"] = {
									["count"] = 410632,
								},
								["Sidewinders"] = {
									["count"] = 241094,
								},
								["Marked Shot"] = {
									["count"] = 855631,
								},
							},
							["amount"] = 1773874,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Maloriak"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Maloriak"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Pyrecraw"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 1675,
								},
							},
							["amount"] = 1675,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["PartialResist"] = {
						["Charge"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Flame Buffet"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 50,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
						["Pyrecraw"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 5.56,
								},
								["Barrage"] = {
									["count"] = 1.08,
								},
								["Sidewinders"] = {
									["count"] = 0.48,
								},
								["Marked Shot"] = {
									["count"] = 0.09,
								},
							},
							["amount"] = 7.21,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Maloriak"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["ActiveTime"] = 7.21,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 241094,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 241094,
								},
							},
							["count"] = 1,
							["amount"] = 241094,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 273755,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 273755,
								},
								["Hit"] = {
									["max"] = 136877,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 136877,
								},
							},
							["count"] = 2,
							["amount"] = 410632,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 177678,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 177678,
								},
								["Hit"] = {
									["max"] = 88839,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 88839,
								},
							},
							["count"] = 2,
							["amount"] = 266517,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 855631,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 855631,
								},
							},
							["count"] = 1,
							["amount"] = 855631,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 7.21,
					["TimeDamaging"] = {
						["Pyrecraw"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 5.56,
								},
								["Barrage"] = {
									["count"] = 1.08,
								},
								["Sidewinders"] = {
									["count"] = 0.48,
								},
								["Marked Shot"] = {
									["count"] = 0.09,
								},
							},
							["amount"] = 7.21,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Maloriak"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
						["Nature"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 0,
								},
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 0,
								},
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Melee"] = 0,
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
						["Nature"] = 0,
						["Physical"] = 0,
					},
					["PartialAbsorb"] = {
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Angerforge"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Ironstar"] = {
							["Details"] = {
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Thaurissan"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["PartialResist"] = {
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Angerforge"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Ironstar"] = {
							["Details"] = {
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Thaurissan"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Angerforge"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Ironstar"] = {
							["Details"] = {
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Spirit of Thaurissan"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["Fight3"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["Dodge"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 1,
						},
						["Physical"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 2,
								},
							},
							["amount"] = 2,
						},
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
						["Nature"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 2,
								},
							},
							["amount"] = 3,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 0,
								},
								["Hit"] = {
									["count"] = 4,
								},
							},
							["amount"] = 4,
						},
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Melee"] = 0,
						["Fire"] = 0,
					},
					["DOT_Time"] = 0,
					["Damage"] = 3620109,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
						["Nature"] = 964378,
						["Physical"] = 2655731,
					},
					["PartialAbsorb"] = {
						["Laser Strike"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Charge"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Incineration Security Measure"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Maimgor"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 482189,
								},
								["Marked Shot"] = {
									["count"] = 1711261,
								},
							},
							["amount"] = 2193450,
						},
						["Magmatron"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 88839,
								},
								["Sidewinders"] = {
									["count"] = 482189,
								},
								["Marked Shot"] = {
									["count"] = 855631,
								},
							},
							["amount"] = 1426659,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Laser Strike"] = {
							["Details"] = {
								["Laser Strike"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmatron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
								["Incineration Security Measure"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["PartialResist"] = {
						["Laser Strike"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Charge"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Incineration Security Measure"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 50,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
						["Maimgor"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.41,
								},
								["Marked Shot"] = {
									["count"] = 1.47,
								},
							},
							["amount"] = 1.88,
						},
						["Magmatron"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.5,
								},
								["Sidewinders"] = {
									["count"] = 0.09,
								},
								["Marked Shot"] = {
									["count"] = 0.19,
								},
							},
							["amount"] = 3.78,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["ActiveTime"] = 5.66,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 482189,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 482189,
								},
								["Hit"] = {
									["max"] = 241095,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 482189,
								},
							},
							["count"] = 3,
							["amount"] = 964378,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 88839,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 88839,
								},
							},
							["count"] = 1,
							["amount"] = 88839,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 855631,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 2566892,
								},
							},
							["count"] = 3,
							["amount"] = 2566892,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 5.66,
					["TimeDamaging"] = {
						["Maimgor"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.41,
								},
								["Marked Shot"] = {
									["count"] = 1.47,
								},
							},
							["amount"] = 1.88,
						},
						["Magmatron"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.5,
								},
								["Sidewinders"] = {
									["count"] = 0.09,
								},
								["Marked Shot"] = {
									["count"] = 0.19,
								},
							},
							["amount"] = 3.78,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["Fight4"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 5584,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
						["Physical"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 3,
								},
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 3,
						},
						["Nature"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
						["Nature"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 0,
								},
								["Hit"] = {
									["count"] = 3,
								},
							},
							["amount"] = 3,
						},
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 6,
								},
								["Hit"] = {
									["count"] = 24,
								},
							},
							["amount"] = 30,
						},
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
						["Fire"] = 0,
						["Melee"] = 5584,
						["Physical"] = 0,
					},
					["DOT_Time"] = 0,
					["Damage"] = 9703525,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
						["Nature"] = 723283,
						["Physical"] = 8980242,
					},
					["PartialAbsorb"] = {
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
						["Laser Strike"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Electrical Discharge"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Flash Bomb"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["DamagedWho"] = {
						["Golem Sentry"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Atramedes"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 723283,
								},
								["Auto Shot"] = {
									["count"] = 885535,
								},
								["Barrage"] = {
									["count"] = 2592324,
								},
								["Aimed Shot"] = {
									["count"] = 2943736,
								},
								["Marked Shot"] = {
									["count"] = 2558647,
								},
							},
							["amount"] = 9703525,
						},
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
						["Golem Sentry"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Atramedes"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 5584,
								},
							},
							["amount"] = 5584,
						},
						["Laser Strike"] = {
							["Details"] = {
								["Laser Strike"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Flash Bomb"] = {
							["Details"] = {
								["Flash Bomb"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 150,
								},
							},
							["amount"] = 150,
						},
					},
					["PartialResist"] = {
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
						["Laser Strike"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Electrical Discharge"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Flash Bomb"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 150,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
						["Golem Sentry"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Atramedes"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.49,
								},
								["Auto Shot"] = {
									["count"] = 8.2,
								},
								["Barrage"] = {
									["count"] = 4.88,
								},
								["Aimed Shot"] = {
									["count"] = 4.54,
								},
								["Marked Shot"] = {
									["count"] = 1.3,
								},
							},
							["amount"] = 20.41,
						},
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 150,
								},
							},
							["amount"] = 150,
						},
					},
					["ActiveTime"] = 20.41,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 241095,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 723283,
								},
							},
							["count"] = 3,
							["amount"] = 723283,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 177107,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 531321,
								},
								["Hit"] = {
									["max"] = 88554,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 354214,
								},
							},
							["count"] = 7,
							["amount"] = 885535,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 272876,
									["min"] = 272876,
									["count"] = 3,
									["amount"] = 818628,
								},
								["Hit"] = {
									["max"] = 136439,
									["min"] = 136438,
									["count"] = 13,
									["amount"] = 1773696,
								},
							},
							["count"] = 16,
							["amount"] = 2592324,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 762946,
									["min"] = 718001,
									["count"] = 4,
									["amount"] = 2943736,
								},
							},
							["count"] = 4,
							["amount"] = 2943736,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 852883,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 2558647,
								},
							},
							["count"] = 3,
							["amount"] = 2558647,
						},
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 20.41,
					["TimeDamaging"] = {
						["Golem Sentry"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Atramedes"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.49,
								},
								["Auto Shot"] = {
									["count"] = 8.2,
								},
								["Barrage"] = {
									["count"] = 4.88,
								},
								["Aimed Shot"] = {
									["count"] = 4.54,
								},
								["Marked Shot"] = {
									["count"] = 1.3,
								},
							},
							["amount"] = 20.41,
						},
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["LastFightData"] = {
					["ElementDoneBlock"] = {
						["Melee"] = 37986,
						["Physical"] = 87339,
					},
					["TimeHealing"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["DOTs"] = {
						["Dragonsfire Grenade (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 54,
								},
							},
							["amount"] = 54,
						},
						["Lacerate (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 108,
								},
							},
							["amount"] = 108,
						},
						["Explosive Trap (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 30,
								},
							},
							["amount"] = 30,
						},
					},
					["TimeSpent"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Muzzle"] = {
									["count"] = 1.22,
								},
								["Harpoon"] = {
									["count"] = 0.37,
								},
								["Melee"] = {
									["count"] = 20.88,
								},
								["Raptor Strike"] = {
									["count"] = 6.02,
								},
								["Auto Shot"] = {
									["count"] = 30.86,
								},
								["Explosive Trap"] = {
									["count"] = 0.22,
								},
								["Carve"] = {
									["count"] = 1.86,
								},
								["Marked Shot"] = {
									["count"] = 1.84,
								},
								["Sidewinders"] = {
									["count"] = 0.19,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 1.71,
								},
								["Lacerate (DoT)"] = {
									["count"] = 21.34,
								},
								["Cobra Shot"] = {
									["count"] = 5.01,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 7.99,
								},
								["Mongoose Bite"] = {
									["count"] = 8.47,
								},
								["Hatchet Toss"] = {
									["count"] = 8.22,
								},
								["Barrage"] = {
									["count"] = 5.92,
								},
							},
							["amount"] = 122.12,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["DamageTaken"] = 3302006,
					["ElementHitsTaken"] = {
						["Shadow"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 26,
								},
								["Dodge"] = {
									["count"] = 4,
								},
								["Crit"] = {
									["count"] = 1,
								},
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 31,
						},
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Physical"] = {
							["Details"] = {
								["Absorb"] = {
									["count"] = 1,
								},
								["Crit"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 11,
								},
							},
							["amount"] = 13,
						},
						["Nature"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGain"] = 50,
					["DeathCount"] = 2,
					["PartialAbsorb"] = {
						["Magma Spit"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Thunderclap"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 31,
									["amount"] = 0,
								},
							},
							["count"] = 31,
							["amount"] = 0,
						},
						["Massacre"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
								["Absorbed"] = {
									["max"] = 709597,
									["min"] = 709597,
									["count"] = 1,
									["amount"] = 709597,
								},
							},
							["count"] = 4,
							["amount"] = 709597,
						},
						["Shadow Breath"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee (Double Attack)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 9,
									["amount"] = 0,
								},
							},
							["count"] = 9,
							["amount"] = 0,
						},
						["Lava Spew"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["ActiveTime"] = 125.62,
					["WhoHealed"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 99630,
								},
							},
							["amount"] = 99630,
						},
					},
					["ElementTaken"] = {
						["Physical"] = 3085802,
						["Fire"] = 0,
						["Melee"] = 216204,
						["Shadow"] = 0,
					},
					["DOT_Time"] = 192,
					["Damage"] = 23887873,
					["HealedWho"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 99630,
								},
							},
							["amount"] = 99630,
						},
					},
					["TimeHeal"] = 3.5,
					["Heals"] = {
						["Exhilaration"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 99630,
									["min"] = 99630,
									["count"] = 1,
									["amount"] = 99630,
								},
							},
							["count"] = 1,
							["amount"] = 99630,
						},
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["ElementTakenAbsorb"] = {
						["Physical"] = 709597,
					},
					["Healing"] = 99630,
					["WhoDamaged"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Shadow Breath"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Melee (Double Attack)"] = {
									["count"] = 85805,
								},
								["Melee"] = {
									["count"] = 216204,
								},
								["Massacre"] = {
									["count"] = 2999997,
								},
							},
							["amount"] = 3302006,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Magma Spit"] = {
									["count"] = 0,
								},
								["Lava Spew"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["Attacks"] = {
						["Harpoon"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Cobra Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 486785,
									["min"] = 486785,
									["count"] = 2,
									["amount"] = 973570,
								},
								["Hit"] = {
									["max"] = 243393,
									["min"] = 243392,
									["count"] = 4,
									["amount"] = 973569,
								},
							},
							["count"] = 6,
							["amount"] = 1947139,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 150091,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 150091,
								},
								["Hit"] = {
									["max"] = 88554,
									["min"] = 0,
									["count"] = 15,
									["amount"] = 1193223,
								},
							},
							["count"] = 16,
							["amount"] = 1343314,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 1705764,
									["min"] = 1705764,
									["count"] = 1,
									["amount"] = 1705764,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 1705764,
						},
						["Muzzle"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Explosive Trap (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 79220,
									["min"] = 79220,
									["count"] = 1,
									["amount"] = 79220,
								},
								["Tick"] = {
									["max"] = 39610,
									["min"] = 39609,
									["count"] = 9,
									["amount"] = 356489,
								},
							},
							["count"] = 10,
							["amount"] = 435709,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Lacerate (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 112799,
									["min"] = 103498,
									["count"] = 12,
									["amount"] = 1263708,
								},
								["Tick"] = {
									["max"] = 56400,
									["min"] = 51748,
									["count"] = 24,
									["amount"] = 1271466,
								},
							},
							["count"] = 36,
							["amount"] = 2535174,
						},
						["Raptor Strike"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 80548,
									["min"] = 71818,
									["count"] = 2,
									["amount"] = 152366,
								},
								["Parry"] = {
									["count"] = 1,
									["amount"] = 0,
								},
								["Crit"] = {
									["max"] = 221794,
									["min"] = 199842,
									["count"] = 3,
									["amount"] = 622660,
								},
								["Hit"] = {
									["max"] = 114349,
									["min"] = 102963,
									["count"] = 3,
									["amount"] = 329153,
								},
							},
							["count"] = 9,
							["amount"] = 1104179,
						},
						["Melee"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 44811,
									["min"] = 43822,
									["count"] = 2,
									["amount"] = 88633,
								},
								["Hit"] = {
									["max"] = 70421,
									["min"] = 60955,
									["count"] = 19,
									["amount"] = 1222505,
								},
								["Crit"] = {
									["max"] = 136171,
									["min"] = 123978,
									["count"] = 6,
									["amount"] = 786638,
								},
								["Parry"] = {
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 30,
							["amount"] = 2097776,
						},
						["Sidewinders"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 482189,
									["min"] = 482189,
									["count"] = 1,
									["amount"] = 482189,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 482189,
						},
						["Explosive Trap"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 396099,
									["min"] = 396099,
									["count"] = 1,
									["amount"] = 396099,
								},
							},
							["count"] = 1,
							["amount"] = 396099,
						},
						["Carve"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 51424,
									["min"] = 51424,
									["count"] = 1,
									["amount"] = 51424,
								},
								["Crit"] = {
									["max"] = 146513,
									["min"] = 141537,
									["count"] = 2,
									["amount"] = 288050,
								},
								["Hit"] = {
									["max"] = 80953,
									["min"] = 72277,
									["count"] = 6,
									["amount"] = 457754,
								},
							},
							["count"] = 9,
							["amount"] = 797228,
						},
						["Dragonsfire Grenade (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 183919,
									["min"] = 183919,
									["count"] = 3,
									["amount"] = 551757,
								},
								["Tick"] = {
									["max"] = 96557,
									["min"] = 91959,
									["count"] = 15,
									["amount"] = 1402383,
								},
							},
							["count"] = 18,
							["amount"] = 1954140,
						},
						["Mongoose Bite"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 896461,
									["min"] = 346972,
									["count"] = 3,
									["amount"] = 1756896,
								},
								["Hit"] = {
									["max"] = 408902,
									["min"] = 172131,
									["count"] = 9,
									["amount"] = 2532567,
								},
							},
							["count"] = 12,
							["amount"] = 4289463,
						},
						["Hatchet Toss"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 11537,
									["min"] = 11537,
									["count"] = 1,
									["amount"] = 11537,
								},
								["Hit"] = {
									["max"] = 5769,
									["min"] = 5768,
									["count"] = 3,
									["amount"] = 17306,
								},
							},
							["count"] = 4,
							["amount"] = 28843,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 272876,
									["min"] = 0,
									["count"] = 12,
									["amount"] = 2885328,
								},
								["Hit"] = {
									["max"] = 136438,
									["min"] = 0,
									["count"] = 20,
									["amount"] = 1885528,
								},
							},
							["count"] = 32,
							["amount"] = 4770856,
						},
					},
					["ElementDone"] = {
						["Fire"] = 2785948,
						["Physical"] = 18521960,
						["Melee"] = 2097776,
						["Nature"] = 482189,
					},
					["HealingTaken"] = 99630,
					["DamagedWho"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Raptor Strike"] = {
									["count"] = 1104179,
								},
								["Cobra Shot"] = {
									["count"] = 1947139,
								},
								["Auto Shot"] = {
									["count"] = 1343314,
								},
								["Carve"] = {
									["count"] = 797228,
								},
								["Explosive Trap"] = {
									["count"] = 396099,
								},
								["Marked Shot"] = {
									["count"] = 1705764,
								},
								["Sidewinders"] = {
									["count"] = 482189,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 435709,
								},
								["Melee"] = {
									["count"] = 2097776,
								},
								["Lacerate (DoT)"] = {
									["count"] = 2535174,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 1954140,
								},
								["Mongoose Bite"] = {
									["count"] = 4289463,
								},
								["Hatchet Toss"] = {
									["count"] = 28843,
								},
								["Barrage"] = {
									["count"] = 4770856,
								},
							},
							["amount"] = 23887873,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["TimeDamage"] = 122.12,
					["TimeDamaging"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Muzzle"] = {
									["count"] = 1.22,
								},
								["Harpoon"] = {
									["count"] = 0.37,
								},
								["Melee"] = {
									["count"] = 20.88,
								},
								["Raptor Strike"] = {
									["count"] = 6.02,
								},
								["Auto Shot"] = {
									["count"] = 30.86,
								},
								["Explosive Trap"] = {
									["count"] = 0.22,
								},
								["Carve"] = {
									["count"] = 1.86,
								},
								["Marked Shot"] = {
									["count"] = 1.84,
								},
								["Sidewinders"] = {
									["count"] = 0.19,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 1.71,
								},
								["Lacerate (DoT)"] = {
									["count"] = 21.34,
								},
								["Cobra Shot"] = {
									["count"] = 5.01,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 7.99,
								},
								["Mongoose Bite"] = {
									["count"] = 8.47,
								},
								["Hatchet Toss"] = {
									["count"] = 8.22,
								},
								["Barrage"] = {
									["count"] = 5.92,
								},
							},
							["amount"] = 122.12,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["PartialResist"] = {
						["Magma Spit"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Thunderclap"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 31,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 31,
						},
						["Massacre"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 4,
						},
						["Shadow Breath"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee (Double Attack)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 9,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 9,
						},
						["Lava Spew"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["ElementHitsDone"] = {
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
								["Crit"] = {
									["count"] = 4,
								},
								["Tick"] = {
									["count"] = 24,
								},
							},
							["amount"] = 29,
						},
						["Physical"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 3,
								},
								["Hit"] = {
									["count"] = 63,
								},
								["Tick"] = {
									["count"] = 24,
								},
								["Crit"] = {
									["count"] = 37,
								},
								["Parry"] = {
									["count"] = 1,
								},
							},
							["amount"] = 128,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 21,
								},
								["Crit"] = {
									["count"] = 6,
								},
								["Parry"] = {
									["count"] = 3,
								},
							},
							["amount"] = 30,
						},
						["Nature"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 1,
						},
					},
				},
				["Fight1"] = {
					["ElementDoneBlock"] = {
						["Melee"] = 37986,
						["Physical"] = 87339,
					},
					["TimeHealing"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["DOTs"] = {
						["Dragonsfire Grenade (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 54,
								},
							},
							["amount"] = 54,
						},
						["Lacerate (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 108,
								},
							},
							["amount"] = 108,
						},
						["Explosive Trap (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 30,
								},
							},
							["amount"] = 30,
						},
					},
					["TimeSpent"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Muzzle"] = {
									["count"] = 1.22,
								},
								["Harpoon"] = {
									["count"] = 0.37,
								},
								["Melee"] = {
									["count"] = 20.88,
								},
								["Raptor Strike"] = {
									["count"] = 6.02,
								},
								["Auto Shot"] = {
									["count"] = 30.86,
								},
								["Explosive Trap"] = {
									["count"] = 0.22,
								},
								["Carve"] = {
									["count"] = 1.86,
								},
								["Marked Shot"] = {
									["count"] = 1.84,
								},
								["Sidewinders"] = {
									["count"] = 0.19,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 1.71,
								},
								["Lacerate (DoT)"] = {
									["count"] = 21.34,
								},
								["Cobra Shot"] = {
									["count"] = 5.01,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 7.99,
								},
								["Mongoose Bite"] = {
									["count"] = 8.47,
								},
								["Hatchet Toss"] = {
									["count"] = 8.22,
								},
								["Barrage"] = {
									["count"] = 5.92,
								},
							},
							["amount"] = 122.12,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["DamageTaken"] = 3302006,
					["ElementHitsTaken"] = {
						["Shadow"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 26,
								},
								["Dodge"] = {
									["count"] = 4,
								},
								["Crit"] = {
									["count"] = 1,
								},
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 31,
						},
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Physical"] = {
							["Details"] = {
								["Absorb"] = {
									["count"] = 1,
								},
								["Crit"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 11,
								},
							},
							["amount"] = 13,
						},
						["Nature"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGain"] = 50,
					["DeathCount"] = 2,
					["PartialAbsorb"] = {
						["Magma Spit"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Thunderclap"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 31,
									["amount"] = 0,
								},
							},
							["count"] = 31,
							["amount"] = 0,
						},
						["Massacre"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
								["Absorbed"] = {
									["max"] = 709597,
									["min"] = 709597,
									["count"] = 1,
									["amount"] = 709597,
								},
							},
							["count"] = 4,
							["amount"] = 709597,
						},
						["Shadow Breath"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee (Double Attack)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 9,
									["amount"] = 0,
								},
							},
							["count"] = 9,
							["amount"] = 0,
						},
						["Lava Spew"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["ActiveTime"] = 125.62,
					["WhoHealed"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 99630,
								},
							},
							["amount"] = 99630,
						},
					},
					["ElementTaken"] = {
						["Physical"] = 3085802,
						["Fire"] = 0,
						["Melee"] = 216204,
						["Shadow"] = 0,
					},
					["DOT_Time"] = 192,
					["Damage"] = 23887873,
					["HealedWho"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 99630,
								},
							},
							["amount"] = 99630,
						},
					},
					["TimeHeal"] = 3.5,
					["Heals"] = {
						["Exhilaration"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 99630,
									["min"] = 99630,
									["count"] = 1,
									["amount"] = 99630,
								},
							},
							["count"] = 1,
							["amount"] = 99630,
						},
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["ElementTakenAbsorb"] = {
						["Physical"] = 709597,
					},
					["Healing"] = 99630,
					["WhoDamaged"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Shadow Breath"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Melee (Double Attack)"] = {
									["count"] = 85805,
								},
								["Melee"] = {
									["count"] = 216204,
								},
								["Massacre"] = {
									["count"] = 2999997,
								},
							},
							["amount"] = 3302006,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Magma Spit"] = {
									["count"] = 0,
								},
								["Lava Spew"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["Attacks"] = {
						["Harpoon"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Cobra Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 486785,
									["min"] = 486785,
									["count"] = 2,
									["amount"] = 973570,
								},
								["Hit"] = {
									["max"] = 243393,
									["min"] = 243392,
									["count"] = 4,
									["amount"] = 973569,
								},
							},
							["count"] = 6,
							["amount"] = 1947139,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 150091,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 150091,
								},
								["Hit"] = {
									["max"] = 88554,
									["min"] = 0,
									["count"] = 15,
									["amount"] = 1193223,
								},
							},
							["count"] = 16,
							["amount"] = 1343314,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 1705764,
									["min"] = 1705764,
									["count"] = 1,
									["amount"] = 1705764,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 1705764,
						},
						["Muzzle"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Explosive Trap (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 79220,
									["min"] = 79220,
									["count"] = 1,
									["amount"] = 79220,
								},
								["Tick"] = {
									["max"] = 39610,
									["min"] = 39609,
									["count"] = 9,
									["amount"] = 356489,
								},
							},
							["count"] = 10,
							["amount"] = 435709,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Lacerate (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 112799,
									["min"] = 103498,
									["count"] = 12,
									["amount"] = 1263708,
								},
								["Tick"] = {
									["max"] = 56400,
									["min"] = 51748,
									["count"] = 24,
									["amount"] = 1271466,
								},
							},
							["count"] = 36,
							["amount"] = 2535174,
						},
						["Raptor Strike"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 80548,
									["min"] = 71818,
									["count"] = 2,
									["amount"] = 152366,
								},
								["Parry"] = {
									["count"] = 1,
									["amount"] = 0,
								},
								["Crit"] = {
									["max"] = 221794,
									["min"] = 199842,
									["count"] = 3,
									["amount"] = 622660,
								},
								["Hit"] = {
									["max"] = 114349,
									["min"] = 102963,
									["count"] = 3,
									["amount"] = 329153,
								},
							},
							["count"] = 9,
							["amount"] = 1104179,
						},
						["Melee"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 44811,
									["min"] = 43822,
									["count"] = 2,
									["amount"] = 88633,
								},
								["Hit"] = {
									["max"] = 70421,
									["min"] = 60955,
									["count"] = 19,
									["amount"] = 1222505,
								},
								["Crit"] = {
									["max"] = 136171,
									["min"] = 123978,
									["count"] = 6,
									["amount"] = 786638,
								},
								["Parry"] = {
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 30,
							["amount"] = 2097776,
						},
						["Sidewinders"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 482189,
									["min"] = 482189,
									["count"] = 1,
									["amount"] = 482189,
								},
								["Hit"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 482189,
						},
						["Explosive Trap"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 396099,
									["min"] = 396099,
									["count"] = 1,
									["amount"] = 396099,
								},
							},
							["count"] = 1,
							["amount"] = 396099,
						},
						["Carve"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 51424,
									["min"] = 51424,
									["count"] = 1,
									["amount"] = 51424,
								},
								["Crit"] = {
									["max"] = 146513,
									["min"] = 141537,
									["count"] = 2,
									["amount"] = 288050,
								},
								["Hit"] = {
									["max"] = 80953,
									["min"] = 72277,
									["count"] = 6,
									["amount"] = 457754,
								},
							},
							["count"] = 9,
							["amount"] = 797228,
						},
						["Dragonsfire Grenade (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 183919,
									["min"] = 183919,
									["count"] = 3,
									["amount"] = 551757,
								},
								["Tick"] = {
									["max"] = 96557,
									["min"] = 91959,
									["count"] = 15,
									["amount"] = 1402383,
								},
							},
							["count"] = 18,
							["amount"] = 1954140,
						},
						["Mongoose Bite"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 896461,
									["min"] = 346972,
									["count"] = 3,
									["amount"] = 1756896,
								},
								["Hit"] = {
									["max"] = 408902,
									["min"] = 172131,
									["count"] = 9,
									["amount"] = 2532567,
								},
							},
							["count"] = 12,
							["amount"] = 4289463,
						},
						["Hatchet Toss"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 11537,
									["min"] = 11537,
									["count"] = 1,
									["amount"] = 11537,
								},
								["Hit"] = {
									["max"] = 5769,
									["min"] = 5768,
									["count"] = 3,
									["amount"] = 17306,
								},
							},
							["count"] = 4,
							["amount"] = 28843,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 272876,
									["min"] = 0,
									["count"] = 12,
									["amount"] = 2885328,
								},
								["Hit"] = {
									["max"] = 136438,
									["min"] = 0,
									["count"] = 20,
									["amount"] = 1885528,
								},
							},
							["count"] = 32,
							["amount"] = 4770856,
						},
					},
					["ElementDone"] = {
						["Fire"] = 2785948,
						["Physical"] = 18521960,
						["Melee"] = 2097776,
						["Nature"] = 482189,
					},
					["HealingTaken"] = 99630,
					["DamagedWho"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Raptor Strike"] = {
									["count"] = 1104179,
								},
								["Cobra Shot"] = {
									["count"] = 1947139,
								},
								["Auto Shot"] = {
									["count"] = 1343314,
								},
								["Carve"] = {
									["count"] = 797228,
								},
								["Explosive Trap"] = {
									["count"] = 396099,
								},
								["Marked Shot"] = {
									["count"] = 1705764,
								},
								["Sidewinders"] = {
									["count"] = 482189,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 435709,
								},
								["Melee"] = {
									["count"] = 2097776,
								},
								["Lacerate (DoT)"] = {
									["count"] = 2535174,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 1954140,
								},
								["Mongoose Bite"] = {
									["count"] = 4289463,
								},
								["Hatchet Toss"] = {
									["count"] = 28843,
								},
								["Barrage"] = {
									["count"] = 4770856,
								},
							},
							["amount"] = 23887873,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["TimeDamage"] = 122.12,
					["TimeDamaging"] = {
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Muzzle"] = {
									["count"] = 1.22,
								},
								["Harpoon"] = {
									["count"] = 0.37,
								},
								["Melee"] = {
									["count"] = 20.88,
								},
								["Raptor Strike"] = {
									["count"] = 6.02,
								},
								["Auto Shot"] = {
									["count"] = 30.86,
								},
								["Explosive Trap"] = {
									["count"] = 0.22,
								},
								["Carve"] = {
									["count"] = 1.86,
								},
								["Marked Shot"] = {
									["count"] = 1.84,
								},
								["Sidewinders"] = {
									["count"] = 0.19,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 1.71,
								},
								["Lacerate (DoT)"] = {
									["count"] = 21.34,
								},
								["Cobra Shot"] = {
									["count"] = 5.01,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 7.99,
								},
								["Mongoose Bite"] = {
									["count"] = 8.47,
								},
								["Hatchet Toss"] = {
									["count"] = 8.22,
								},
								["Barrage"] = {
									["count"] = 5.92,
								},
							},
							["amount"] = 122.12,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0,
								},
								["Auto Shot"] = {
									["count"] = 0,
								},
								["Aimed Shot"] = {
									["count"] = 0,
								},
								["Marked Shot"] = {
									["count"] = 0,
								},
								["Barrage"] = {
									["count"] = 0,
								},
							},
							["amount"] = 0,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 50,
								},
							},
							["amount"] = 50,
						},
					},
					["PartialResist"] = {
						["Magma Spit"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Thunderclap"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 31,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 31,
						},
						["Massacre"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 4,
						},
						["Shadow Breath"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
						["Melee (Double Attack)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 9,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 9,
						},
						["Lava Spew"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 0,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 0,
						},
					},
					["ElementHitsDone"] = {
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
								["Crit"] = {
									["count"] = 4,
								},
								["Tick"] = {
									["count"] = 24,
								},
							},
							["amount"] = 29,
						},
						["Physical"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 3,
								},
								["Hit"] = {
									["count"] = 63,
								},
								["Tick"] = {
									["count"] = 24,
								},
								["Crit"] = {
									["count"] = 37,
								},
								["Parry"] = {
									["count"] = 1,
								},
							},
							["amount"] = 128,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 21,
								},
								["Crit"] = {
									["count"] = 6,
								},
								["Parry"] = {
									["count"] = 3,
								},
							},
							["amount"] = 30,
						},
						["Nature"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 0,
								},
							},
							["amount"] = 1,
						},
					},
				},
				["OverallData"] = {
					["ElementDoneBlock"] = {
						["Melee"] = 37986,
						["Physical"] = 87339,
					},
					["TimeHealing"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["DOTs"] = {
						["Dragonsfire Grenade (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 54,
								},
							},
							["amount"] = 54,
						},
						["Lacerate (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 108,
								},
							},
							["amount"] = 108,
						},
						["Explosive Trap (DoT)"] = {
							["Details"] = {
								["Chimaeron"] = {
									["count"] = 30,
								},
							},
							["amount"] = 30,
						},
					},
					["TimeSpent"] = {
						["Magmatron"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.93,
								},
								["Auto Shot"] = {
									["count"] = 9.73,
								},
								["Barrage"] = {
									["count"] = 3.7,
								},
								["Aimed Shot"] = {
									["count"] = 0.56,
								},
								["Marked Shot"] = {
									["count"] = 1.29,
								},
							},
							["amount"] = 17.21,
						},
						["Spirit of Moltenfist"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 2.63,
								},
								["Auto Shot"] = {
									["count"] = 2.5,
								},
							},
							["amount"] = 5.13,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.51,
								},
								["Barrage"] = {
									["count"] = 2.42,
								},
								["Auto Shot"] = {
									["count"] = 7,
								},
								["Marked Shot"] = {
									["count"] = 2.75,
								},
							},
							["amount"] = 13.68,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.41,
								},
								["Marked Shot"] = {
									["count"] = 2.84,
								},
							},
							["amount"] = 3.25,
						},
						["Spirit of Thaurissan"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.35,
								},
								["Barrage"] = {
									["count"] = 0.24,
								},
							},
							["amount"] = 0.59,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Muzzle"] = {
									["count"] = 1.22,
								},
								["Harpoon"] = {
									["count"] = 0.37,
								},
								["Melee"] = {
									["count"] = 20.88,
								},
								["Raptor Strike"] = {
									["count"] = 6.02,
								},
								["Auto Shot"] = {
									["count"] = 30.86,
								},
								["Explosive Trap"] = {
									["count"] = 0.22,
								},
								["Carve"] = {
									["count"] = 1.86,
								},
								["Marked Shot"] = {
									["count"] = 1.84,
								},
								["Sidewinders"] = {
									["count"] = 0.19,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 1.71,
								},
								["Lacerate (DoT)"] = {
									["count"] = 21.34,
								},
								["Cobra Shot"] = {
									["count"] = 5.01,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 7.99,
								},
								["Mongoose Bite"] = {
									["count"] = 8.47,
								},
								["Hatchet Toss"] = {
									["count"] = 8.22,
								},
								["Barrage"] = {
									["count"] = 5.92,
								},
							},
							["amount"] = 122.12,
						},
						["Spirit of Anvilrage"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.42,
								},
								["Aimed Shot"] = {
									["count"] = 0.88,
								},
							},
							["amount"] = 4.3,
						},
						["Spirit of Corehammer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.05,
								},
								["Marked Shot"] = {
									["count"] = 1.85,
								},
							},
							["amount"] = 4.9,
						},
						["Maloriak"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.39,
								},
								["Aimed Shot"] = {
									["count"] = 5.75,
								},
								["Barrage"] = {
									["count"] = 2.66,
								},
								["Auto Shot"] = {
									["count"] = 4.19,
								},
								["Marked Shot"] = {
									["count"] = 1.45,
								},
							},
							["amount"] = 14.44,
						},
						["Spirit of Shadowforge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.59,
								},
								["Marked Shot"] = {
									["count"] = 0.2,
								},
							},
							["amount"] = 0.79,
						},
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.08,
								},
								["Auto Shot"] = {
									["count"] = 6.83,
								},
								["Marked Shot"] = {
									["count"] = 0.38,
								},
								["Aimed Shot"] = {
									["count"] = 2.62,
								},
								["Barrage"] = {
									["count"] = 1.2,
								},
							},
							["amount"] = 12.11,
						},
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.49,
								},
								["Marked Shot"] = {
									["count"] = 0.02,
								},
								["Auto Shot"] = {
									["count"] = 3.57,
								},
								["Barrage"] = {
									["count"] = 1.32,
								},
							},
							["amount"] = 6.4,
						},
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 4.27,
								},
								["Auto Shot"] = {
									["count"] = 22.62,
								},
								["Aimed Shot"] = {
									["count"] = 11.11,
								},
								["Marked Shot"] = {
									["count"] = 0.77,
								},
								["Barrage"] = {
									["count"] = 5.17,
								},
							},
							["amount"] = 43.94,
						},
						["Atramedes"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.49,
								},
								["Auto Shot"] = {
									["count"] = 8.2,
								},
								["Barrage"] = {
									["count"] = 4.88,
								},
								["Aimed Shot"] = {
									["count"] = 4.54,
								},
								["Marked Shot"] = {
									["count"] = 1.3,
								},
							},
							["amount"] = 20.41,
						},
						["Pyrecraw"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 5.56,
								},
								["Barrage"] = {
									["count"] = 1.08,
								},
								["Sidewinders"] = {
									["count"] = 0.48,
								},
								["Marked Shot"] = {
									["count"] = 0.09,
								},
							},
							["amount"] = 7.21,
						},
						["Maimgor"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.37,
								},
								["Aimed Shot"] = {
									["count"] = 2.53,
								},
								["Auto Shot"] = {
									["count"] = 6.22,
								},
							},
							["amount"] = 9.12,
						},
						["Golem Sentry"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.57,
								},
								["Aimed Shot"] = {
									["count"] = 3.46,
								},
								["Auto Shot"] = {
									["count"] = 11.24,
								},
								["Marked Shot"] = {
									["count"] = 0.42,
								},
							},
							["amount"] = 15.69,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 9.35,
								},
								["Aimed Shot"] = {
									["count"] = 3.16,
								},
							},
							["amount"] = 12.51,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 7,
								},
								["Sidewinders"] = {
									["count"] = 0.23,
								},
								["Marked Shot"] = {
									["count"] = 0.37,
								},
							},
							["amount"] = 7.6,
						},
						["Spirit of Ironstar"] = {
							["Details"] = {
								["Barrage"] = {
									["count"] = 0.88,
								},
							},
							["amount"] = 0.88,
						},
						["Spirit of Angerforge"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 2.17,
								},
								["Barrage"] = {
									["count"] = 1.07,
								},
							},
							["amount"] = 3.24,
						},
					},
					["DamageTaken"] = 3447618,
					["ElementHitsTaken"] = {
						["Physical"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 4,
								},
								["Absorb"] = {
									["count"] = 1,
								},
								["Crit"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 12,
								},
							},
							["amount"] = 18,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 44,
								},
								["Dodge"] = {
									["count"] = 6,
								},
								["Crit"] = {
									["count"] = 1,
								},
								["Miss"] = {
									["count"] = 15,
								},
							},
							["amount"] = 66,
						},
						["Fire"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 1,
								},
								["Hit"] = {
									["count"] = 27,
								},
							},
							["amount"] = 28,
						},
						["Shadow"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
						["Nature"] = {
							["Details"] = {
								["Miss"] = {
									["count"] = 5,
								},
							},
							["amount"] = 5,
						},
					},
					["EnergyGain"] = 1350,
					["DeathCount"] = 2,
					["PartialAbsorb"] = {
						["Charge"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 4,
							["amount"] = 0,
						},
						["Laser Strike"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Incineration Security Measure"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Massacre"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
								["Absorbed"] = {
									["max"] = 709597,
									["min"] = 709597,
									["count"] = 1,
									["amount"] = 709597,
								},
							},
							["count"] = 4,
							["amount"] = 709597,
						},
						["Melee (Double Attack)"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 9,
									["amount"] = 0,
								},
							},
							["count"] = 9,
							["amount"] = 0,
						},
						["Electrical Discharge"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Flame Buffet"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Magma Spit"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 18,
									["amount"] = 0,
								},
							},
							["count"] = 18,
							["amount"] = 0,
						},
						["Thunderclap"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Stormbolt"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Flash Bomb"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Melee"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 66,
									["amount"] = 0,
								},
							},
							["count"] = 66,
							["amount"] = 0,
						},
						["Shadow Breath"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Chain Lightning"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Lava Spew"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
					},
					["ActiveTime"] = 329.02,
					["WhoHealed"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 99630,
								},
							},
							["amount"] = 99630,
						},
					},
					["ElementTaken"] = {
						["Shadow"] = 1178,
						["Fire"] = 115889,
						["Melee"] = 244471,
						["Physical"] = 3086080,
					},
					["DOT_Time"] = 192,
					["Damage"] = 104805840,
					["HealedWho"] = {
						["Falriçk"] = {
							["Details"] = {
								["Exhilaration"] = {
									["count"] = 99630,
								},
							},
							["amount"] = 99630,
						},
					},
					["TimeHeal"] = 3.5,
					["Heals"] = {
						["Exhilaration"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 99630,
									["min"] = 99630,
									["count"] = 1,
									["amount"] = 99630,
								},
							},
							["count"] = 1,
							["amount"] = 99630,
						},
					},
					["EnergyGained"] = {
						["Sidewinders"] = {
							["Details"] = {
								["Falriçk"] = {
									["count"] = 1350,
								},
							},
							["amount"] = 1350,
						},
					},
					["ElementTakenAbsorb"] = {
						["Physical"] = 709597,
					},
					["Healing"] = 99630,
					["WhoDamaged"] = {
						["Magmatron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 7965,
								},
								["Incineration Security Measure"] = {
									["count"] = 624,
								},
							},
							["amount"] = 8589,
						},
						["Drakonid Drudge"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 1915,
								},
							},
							["amount"] = 1915,
						},
						["Laser Strike"] = {
							["Details"] = {
								["Laser Strike"] = {
									["count"] = 911,
								},
							},
							["amount"] = 911,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 1556,
								},
							},
							["amount"] = 1556,
						},
						["Magmaw"] = {
							["Details"] = {
								["Magma Spit"] = {
									["count"] = 98285,
								},
								["Lava Spew"] = {
									["count"] = 16069,
								},
							},
							["amount"] = 114354,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Melee (Double Attack)"] = {
									["count"] = 85805,
								},
								["Melee"] = {
									["count"] = 216204,
								},
								["Massacre"] = {
									["count"] = 2999997,
								},
							},
							["amount"] = 3302006,
						},
						["Pyrecraw"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 1675,
								},
							},
							["amount"] = 1675,
						},
						["Atramedes"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 5584,
								},
							},
							["amount"] = 5584,
						},
						["Flash Bomb"] = {
							["Details"] = {
								["Flash Bomb"] = {
									["count"] = 278,
								},
							},
							["amount"] = 278,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Shadow Breath"] = {
									["count"] = 1178,
								},
							},
							["amount"] = 1178,
						},
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 2986,
								},
							},
							["amount"] = 2986,
						},
						["Maloriak"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 4534,
								},
							},
							["amount"] = 4534,
						},
						["Golem Sentry"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 2052,
								},
							},
							["amount"] = 2052,
						},
					},
					["Attacks"] = {
						["Harpoon"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 2,
							["amount"] = 0,
						},
						["Cobra Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 486785,
									["min"] = 486785,
									["count"] = 2,
									["amount"] = 973570,
								},
								["Hit"] = {
									["max"] = 243393,
									["min"] = 243392,
									["count"] = 4,
									["amount"] = 973569,
								},
							},
							["count"] = 6,
							["amount"] = 1947139,
						},
						["Auto Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 177679,
									["min"] = 150091,
									["count"] = 20,
									["amount"] = 3520266,
								},
								["Hit"] = {
									["max"] = 88840,
									["min"] = 75045,
									["count"] = 65,
									["amount"] = 5627757,
								},
							},
							["count"] = 85,
							["amount"] = 9148023,
						},
						["Marked Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 1711262,
									["min"] = 1705764,
									["count"] = 6,
									["amount"] = 10251078,
								},
								["Hit"] = {
									["max"] = 855631,
									["min"] = 852882,
									["count"] = 19,
									["amount"] = 16234996,
								},
							},
							["count"] = 25,
							["amount"] = 26486074,
						},
						["Muzzle"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 1,
							["amount"] = 0,
						},
						["Explosive Trap (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 79220,
									["min"] = 79220,
									["count"] = 1,
									["amount"] = 79220,
								},
								["Tick"] = {
									["max"] = 39610,
									["min"] = 39609,
									["count"] = 9,
									["amount"] = 356489,
								},
							},
							["count"] = 10,
							["amount"] = 435709,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 1458076,
									["min"] = 561798,
									["count"] = 12,
									["amount"] = 9634829,
								},
								["Hit"] = {
									["max"] = 766466,
									["min"] = 282969,
									["count"] = 22,
									["amount"] = 12578158,
								},
							},
							["count"] = 34,
							["amount"] = 22212987,
						},
						["Lacerate (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 112799,
									["min"] = 103498,
									["count"] = 12,
									["amount"] = 1263708,
								},
								["Tick"] = {
									["max"] = 56400,
									["min"] = 51748,
									["count"] = 24,
									["amount"] = 1271466,
								},
							},
							["count"] = 36,
							["amount"] = 2535174,
						},
						["Raptor Strike"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 80548,
									["min"] = 71818,
									["count"] = 2,
									["amount"] = 152366,
								},
								["Parry"] = {
									["count"] = 1,
									["amount"] = 0,
								},
								["Crit"] = {
									["max"] = 221794,
									["min"] = 199842,
									["count"] = 3,
									["amount"] = 622660,
								},
								["Hit"] = {
									["max"] = 114349,
									["min"] = 102963,
									["count"] = 3,
									["amount"] = 329153,
								},
							},
							["count"] = 9,
							["amount"] = 1104179,
						},
						["Melee"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 44811,
									["min"] = 43822,
									["count"] = 2,
									["amount"] = 88633,
								},
								["Hit"] = {
									["max"] = 70421,
									["min"] = 60955,
									["count"] = 19,
									["amount"] = 1222505,
								},
								["Crit"] = {
									["max"] = 136171,
									["min"] = 123978,
									["count"] = 6,
									["amount"] = 786638,
								},
								["Parry"] = {
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 30,
							["amount"] = 2097776,
						},
						["Sidewinders"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 482189,
									["min"] = 482188,
									["count"] = 6,
									["amount"] = 2893130,
								},
								["Hit"] = {
									["max"] = 241095,
									["min"] = 241094,
									["count"] = 23,
									["amount"] = 5545171,
								},
							},
							["count"] = 29,
							["amount"] = 8438301,
						},
						["Explosive Trap"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 396099,
									["min"] = 396099,
									["count"] = 1,
									["amount"] = 396099,
								},
							},
							["count"] = 1,
							["amount"] = 396099,
						},
						["Carve"] = {
							["Details"] = {
								["Hit (Blocked)"] = {
									["max"] = 51424,
									["min"] = 51424,
									["count"] = 1,
									["amount"] = 51424,
								},
								["Crit"] = {
									["max"] = 146513,
									["min"] = 141537,
									["count"] = 2,
									["amount"] = 288050,
								},
								["Hit"] = {
									["max"] = 80953,
									["min"] = 72277,
									["count"] = 6,
									["amount"] = 457754,
								},
							},
							["count"] = 9,
							["amount"] = 797228,
						},
						["Dragonsfire Grenade (DoT)"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 183919,
									["min"] = 183919,
									["count"] = 3,
									["amount"] = 551757,
								},
								["Tick"] = {
									["max"] = 96557,
									["min"] = 91959,
									["count"] = 15,
									["amount"] = 1402383,
								},
							},
							["count"] = 18,
							["amount"] = 1954140,
						},
						["Mongoose Bite"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 896461,
									["min"] = 346972,
									["count"] = 3,
									["amount"] = 1756896,
								},
								["Hit"] = {
									["max"] = 408902,
									["min"] = 172131,
									["count"] = 9,
									["amount"] = 2532567,
								},
							},
							["count"] = 12,
							["amount"] = 4289463,
						},
						["Hatchet Toss"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 11537,
									["min"] = 11537,
									["count"] = 1,
									["amount"] = 11537,
								},
								["Hit"] = {
									["max"] = 5769,
									["min"] = 5768,
									["count"] = 3,
									["amount"] = 17306,
								},
							},
							["count"] = 4,
							["amount"] = 28843,
						},
						["Barrage"] = {
							["Details"] = {
								["Crit"] = {
									["max"] = 273756,
									["min"] = 143148,
									["count"] = 39,
									["amount"] = 10259139,
								},
								["Hit"] = {
									["max"] = 136878,
									["min"] = 71574,
									["count"] = 99,
									["amount"] = 12675566,
								},
							},
							["count"] = 138,
							["amount"] = 22934705,
						},
					},
					["ElementDone"] = {
						["Fire"] = 2785948,
						["Physical"] = 91483815,
						["Melee"] = 2097776,
						["Nature"] = 8438301,
					},
					["HealingTaken"] = 99630,
					["DamagedWho"] = {
						["Magmatron"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 723284,
								},
								["Auto Shot"] = {
									["count"] = 708430,
								},
								["Barrage"] = {
									["count"] = 2728760,
								},
								["Aimed Shot"] = {
									["count"] = 2052171,
								},
								["Marked Shot"] = {
									["count"] = 3411530,
								},
							},
							["amount"] = 9624175,
						},
						["Spirit of Moltenfist"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 613088,
								},
								["Auto Shot"] = {
									["count"] = 177678,
								},
							},
							["amount"] = 790766,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 241094,
								},
								["Barrage"] = {
									["count"] = 273756,
								},
								["Auto Shot"] = {
									["count"] = 177679,
								},
								["Marked Shot"] = {
									["count"] = 2566893,
								},
							},
							["amount"] = 3259422,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 482189,
								},
								["Marked Shot"] = {
									["count"] = 4278152,
								},
							},
							["amount"] = 4760341,
						},
						["Spirit of Thaurissan"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 482188,
								},
								["Barrage"] = {
									["count"] = 273755,
								},
							},
							["amount"] = 755943,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Raptor Strike"] = {
									["count"] = 1104179,
								},
								["Cobra Shot"] = {
									["count"] = 1947139,
								},
								["Auto Shot"] = {
									["count"] = 1343314,
								},
								["Carve"] = {
									["count"] = 797228,
								},
								["Explosive Trap"] = {
									["count"] = 396099,
								},
								["Marked Shot"] = {
									["count"] = 1705764,
								},
								["Sidewinders"] = {
									["count"] = 482189,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 435709,
								},
								["Melee"] = {
									["count"] = 2097776,
								},
								["Lacerate (DoT)"] = {
									["count"] = 2535174,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 1954140,
								},
								["Mongoose Bite"] = {
									["count"] = 4289463,
								},
								["Hatchet Toss"] = {
									["count"] = 28843,
								},
								["Barrage"] = {
									["count"] = 4770856,
								},
							},
							["amount"] = 23887873,
						},
						["Spirit of Anvilrage"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 177678,
								},
								["Aimed Shot"] = {
									["count"] = 863097,
								},
							},
							["amount"] = 1040775,
						},
						["Spirit of Corehammer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 177678,
								},
								["Marked Shot"] = {
									["count"] = 855630,
								},
							},
							["amount"] = 1033308,
						},
						["Maloriak"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 482189,
								},
								["Aimed Shot"] = {
									["count"] = 2869329,
								},
								["Barrage"] = {
									["count"] = 2728763,
								},
								["Auto Shot"] = {
									["count"] = 442769,
								},
								["Marked Shot"] = {
									["count"] = 1705764,
								},
							},
							["amount"] = 8228814,
						},
						["Spirit of Shadowforge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 241094,
								},
								["Marked Shot"] = {
									["count"] = 855631,
								},
							},
							["amount"] = 1096725,
						},
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 964377,
								},
								["Auto Shot"] = {
									["count"] = 533035,
								},
								["Marked Shot"] = {
									["count"] = 855631,
								},
								["Aimed Shot"] = {
									["count"] = 899704,
								},
								["Barrage"] = {
									["count"] = 1368777,
								},
							},
							["amount"] = 4621524,
						},
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 241095,
								},
								["Marked Shot"] = {
									["count"] = 1711262,
								},
								["Auto Shot"] = {
									["count"] = 177678,
								},
								["Barrage"] = {
									["count"] = 821267,
								},
							},
							["amount"] = 2951302,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1205471,
								},
								["Auto Shot"] = {
									["count"] = 2036734,
								},
								["Aimed Shot"] = {
									["count"] = 7298467,
								},
								["Marked Shot"] = {
									["count"] = 2558646,
								},
								["Barrage"] = {
									["count"] = 4638894,
								},
							},
							["amount"] = 17738212,
						},
						["Atramedes"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 723283,
								},
								["Auto Shot"] = {
									["count"] = 885535,
								},
								["Barrage"] = {
									["count"] = 2592324,
								},
								["Aimed Shot"] = {
									["count"] = 2943736,
								},
								["Marked Shot"] = {
									["count"] = 2558647,
								},
							},
							["amount"] = 9703525,
						},
						["Pyrecraw"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 266517,
								},
								["Barrage"] = {
									["count"] = 410632,
								},
								["Sidewinders"] = {
									["count"] = 241094,
								},
								["Marked Shot"] = {
									["count"] = 855631,
								},
							},
							["amount"] = 1773874,
						},
						["Maimgor"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 482188,
								},
								["Aimed Shot"] = {
									["count"] = 1486088,
								},
								["Auto Shot"] = {
									["count"] = 533034,
								},
							},
							["amount"] = 2501310,
						},
						["Golem Sentry"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 723283,
								},
								["Aimed Shot"] = {
									["count"] = 1741383,
								},
								["Auto Shot"] = {
									["count"] = 621873,
								},
								["Marked Shot"] = {
									["count"] = 855631,
								},
							},
							["amount"] = 3942170,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 621874,
								},
								["Aimed Shot"] = {
									["count"] = 1445924,
								},
							},
							["amount"] = 2067798,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 177678,
								},
								["Sidewinders"] = {
									["count"] = 723283,
								},
								["Marked Shot"] = {
									["count"] = 1711262,
								},
							},
							["amount"] = 2612223,
						},
						["Spirit of Ironstar"] = {
							["Details"] = {
								["Barrage"] = {
									["count"] = 821267,
								},
							},
							["amount"] = 821267,
						},
						["Spirit of Angerforge"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 88839,
								},
								["Barrage"] = {
									["count"] = 1505654,
								},
							},
							["amount"] = 1594493,
						},
					},
					["TimeDamage"] = 325.52,
					["TimeDamaging"] = {
						["Magmatron"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.93,
								},
								["Auto Shot"] = {
									["count"] = 9.73,
								},
								["Barrage"] = {
									["count"] = 3.7,
								},
								["Aimed Shot"] = {
									["count"] = 0.56,
								},
								["Marked Shot"] = {
									["count"] = 1.29,
								},
							},
							["amount"] = 17.21,
						},
						["Spirit of Moltenfist"] = {
							["Details"] = {
								["Aimed Shot"] = {
									["count"] = 2.63,
								},
								["Auto Shot"] = {
									["count"] = 2.5,
								},
							},
							["amount"] = 5.13,
						},
						["Drakonid Chainwielder"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.51,
								},
								["Barrage"] = {
									["count"] = 2.42,
								},
								["Auto Shot"] = {
									["count"] = 7,
								},
								["Marked Shot"] = {
									["count"] = 2.75,
								},
							},
							["amount"] = 13.68,
						},
						["Drakeadon Mongrel"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.41,
								},
								["Marked Shot"] = {
									["count"] = 2.84,
								},
							},
							["amount"] = 3.25,
						},
						["Spirit of Thaurissan"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.35,
								},
								["Barrage"] = {
									["count"] = 0.24,
								},
							},
							["amount"] = 0.59,
						},
						["Chimaeron"] = {
							["Details"] = {
								["Muzzle"] = {
									["count"] = 1.22,
								},
								["Harpoon"] = {
									["count"] = 0.37,
								},
								["Melee"] = {
									["count"] = 20.88,
								},
								["Raptor Strike"] = {
									["count"] = 6.02,
								},
								["Auto Shot"] = {
									["count"] = 30.86,
								},
								["Explosive Trap"] = {
									["count"] = 0.22,
								},
								["Carve"] = {
									["count"] = 1.86,
								},
								["Marked Shot"] = {
									["count"] = 1.84,
								},
								["Sidewinders"] = {
									["count"] = 0.19,
								},
								["Explosive Trap (DoT)"] = {
									["count"] = 1.71,
								},
								["Lacerate (DoT)"] = {
									["count"] = 21.34,
								},
								["Cobra Shot"] = {
									["count"] = 5.01,
								},
								["Dragonsfire Grenade (DoT)"] = {
									["count"] = 7.99,
								},
								["Mongoose Bite"] = {
									["count"] = 8.47,
								},
								["Hatchet Toss"] = {
									["count"] = 8.22,
								},
								["Barrage"] = {
									["count"] = 5.92,
								},
							},
							["amount"] = 122.12,
						},
						["Spirit of Anvilrage"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.42,
								},
								["Aimed Shot"] = {
									["count"] = 0.88,
								},
							},
							["amount"] = 4.3,
						},
						["Spirit of Corehammer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 3.05,
								},
								["Marked Shot"] = {
									["count"] = 1.85,
								},
							},
							["amount"] = 4.9,
						},
						["Maloriak"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.39,
								},
								["Aimed Shot"] = {
									["count"] = 5.75,
								},
								["Barrage"] = {
									["count"] = 2.66,
								},
								["Auto Shot"] = {
									["count"] = 4.19,
								},
								["Marked Shot"] = {
									["count"] = 1.45,
								},
							},
							["amount"] = 14.44,
						},
						["Spirit of Shadowforge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.59,
								},
								["Marked Shot"] = {
									["count"] = 0.2,
								},
							},
							["amount"] = 0.79,
						},
						["Drakonid Drudge"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.08,
								},
								["Auto Shot"] = {
									["count"] = 6.83,
								},
								["Marked Shot"] = {
									["count"] = 0.38,
								},
								["Aimed Shot"] = {
									["count"] = 2.62,
								},
								["Barrage"] = {
									["count"] = 1.2,
								},
							},
							["amount"] = 12.11,
						},
						["Spirit of Burningeye"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.49,
								},
								["Marked Shot"] = {
									["count"] = 0.02,
								},
								["Auto Shot"] = {
									["count"] = 3.57,
								},
								["Barrage"] = {
									["count"] = 1.32,
								},
							},
							["amount"] = 6.4,
						},
						["Magmaw"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 4.27,
								},
								["Auto Shot"] = {
									["count"] = 22.62,
								},
								["Aimed Shot"] = {
									["count"] = 11.11,
								},
								["Marked Shot"] = {
									["count"] = 0.77,
								},
								["Barrage"] = {
									["count"] = 5.17,
								},
							},
							["amount"] = 43.94,
						},
						["Atramedes"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1.49,
								},
								["Auto Shot"] = {
									["count"] = 8.2,
								},
								["Barrage"] = {
									["count"] = 4.88,
								},
								["Aimed Shot"] = {
									["count"] = 4.54,
								},
								["Marked Shot"] = {
									["count"] = 1.3,
								},
							},
							["amount"] = 20.41,
						},
						["Pyrecraw"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 5.56,
								},
								["Barrage"] = {
									["count"] = 1.08,
								},
								["Sidewinders"] = {
									["count"] = 0.48,
								},
								["Marked Shot"] = {
									["count"] = 0.09,
								},
							},
							["amount"] = 7.21,
						},
						["Maimgor"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.37,
								},
								["Aimed Shot"] = {
									["count"] = 2.53,
								},
								["Auto Shot"] = {
									["count"] = 6.22,
								},
							},
							["amount"] = 9.12,
						},
						["Golem Sentry"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 0.57,
								},
								["Aimed Shot"] = {
									["count"] = 3.46,
								},
								["Auto Shot"] = {
									["count"] = 11.24,
								},
								["Marked Shot"] = {
									["count"] = 0.42,
								},
							},
							["amount"] = 15.69,
						},
						["Ivoroc"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 9.35,
								},
								["Aimed Shot"] = {
									["count"] = 3.16,
								},
							},
							["amount"] = 12.51,
						},
						["Drakonid Slayer"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 7,
								},
								["Sidewinders"] = {
									["count"] = 0.23,
								},
								["Marked Shot"] = {
									["count"] = 0.37,
								},
							},
							["amount"] = 7.6,
						},
						["Spirit of Ironstar"] = {
							["Details"] = {
								["Barrage"] = {
									["count"] = 0.88,
								},
							},
							["amount"] = 0.88,
						},
						["Spirit of Angerforge"] = {
							["Details"] = {
								["Auto Shot"] = {
									["count"] = 2.17,
								},
								["Barrage"] = {
									["count"] = 1.07,
								},
							},
							["amount"] = 3.24,
						},
					},
					["EnergyGainedFrom"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1350,
								},
							},
							["amount"] = 1350,
						},
					},
					["PartialResist"] = {
						["Charge"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 4,
						},
						["Laser Strike"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Incineration Security Measure"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Massacre"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 4,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 4,
						},
						["Melee (Double Attack)"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 9,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 9,
						},
						["Electrical Discharge"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 2,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 2,
						},
						["Flame Buffet"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Magma Spit"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 18,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 18,
						},
						["Thunderclap"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Stormbolt"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Flash Bomb"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Melee"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 66,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 66,
						},
						["Shadow Breath"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Chain Lightning"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 1,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 1,
						},
						["Lava Spew"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
					},
					["ElementHitsDone"] = {
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
								["Crit"] = {
									["count"] = 4,
								},
								["Tick"] = {
									["count"] = 24,
								},
							},
							["amount"] = 29,
						},
						["Physical"] = {
							["Details"] = {
								["Immune"] = {
									["count"] = 3,
								},
								["Hit"] = {
									["count"] = 233,
								},
								["Tick"] = {
									["count"] = 24,
								},
								["Crit"] = {
									["count"] = 100,
								},
								["Parry"] = {
									["count"] = 1,
								},
							},
							["amount"] = 361,
						},
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 21,
								},
								["Crit"] = {
									["count"] = 6,
								},
								["Parry"] = {
									["count"] = 3,
								},
							},
							["amount"] = 30,
						},
						["Nature"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 6,
								},
								["Hit"] = {
									["count"] = 23,
								},
							},
							["amount"] = 29,
						},
					},
				},
			},
			["UnitLockout"] = 398473.238,
			["LastActive"] = 399703.359,
		},
		["Atramedes"] = {
			["GUID"] = "Vehicle-0-3783-669-31546-41442-00003154BC",
			["type"] = "Trivial",
			["FightsSaved"] = 4,
			["Owner"] = false,
			["enClass"] = "MOB",
			["Name"] = "Atramedes",
			["Fights"] = {
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
			},
			["level"] = -1,
			["UnitLockout"] = 399124.358,
			["LastFightIn"] = 9,
		},
		["Chimaeron"] = {
			["GUID"] = "Creature-0-3783-669-31546-43296-0000315283",
			["type"] = "Trivial",
			["FightsSaved"] = 1,
			["Owner"] = false,
			["enClass"] = "MOB",
			["Name"] = "Chimaeron",
			["Fights"] = {
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
			},
			["level"] = -1,
			["UnitLockout"] = 399209.357,
			["LastFightIn"] = 12,
		},
		["Toxitron"] = {
			["GUID"] = "Vehicle-0-3783-669-31546-42180-0000315283",
			["LastEventHealth"] = {
				"???", -- [1]
			},
			["LastEventType"] = {
				"MISC", -- [1]
			},
			["TimeWindows"] = {
				["DeathCount"] = {
					1, -- [1]
				},
			},
			["enClass"] = "MOB",
			["level"] = -1,
			["LastFightIn"] = 4,
			["type"] = "Boss",
			["FightsSaved"] = 5,
			["DeathLogs"] = {
				{
					["MessageTimes"] = {
						0, -- [1]
					},
					["MessageIncoming"] = {
						true, -- [1]
					},
					["Messages"] = {
						"Toxitron dies.", -- [1]
					},
					["EventNum"] = {
						0, -- [1]
					},
					["DeathAt"] = 398692.356,
					["HealthNum"] = {
						0, -- [1]
					},
					["Health"] = {
						"???", -- [1]
					},
					["MessageType"] = {
						"MISC", -- [1]
					},
				}, -- [1]
			},
			["Owner"] = false,
			["NextEventNum"] = 2,
			["LastEventHealthNum"] = {
				0, -- [1]
			},
			["LastEvents"] = {
				"Toxitron dies.", -- [1]
			},
			["Name"] = "Toxitron",
			["TimeLast"] = {
				["OVERALL"] = 398690.368,
				["DeathCount"] = 398690.368,
			},
			["LastEventIncoming"] = {
				true, -- [1]
			},
			["LastEventTimes"] = {
				398690.384, -- [1]
			},
			["UnitLockout"] = 398690.368,
			["Fights"] = {
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["OverallData"] = {
					["DeathCount"] = 1,
				},
			},
		},
		["Maloriak"] = {
			["GUID"] = "Creature-0-3783-669-31546-41378-0000315283",
			["type"] = "Trivial",
			["FightsSaved"] = 5,
			["Owner"] = false,
			["enClass"] = "MOB",
			["Name"] = "Maloriak",
			["Fights"] = {
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
			},
			["level"] = -1,
			["UnitLockout"] = 398852.356,
			["LastFightIn"] = 5,
		},
		["Magmaw"] = {
			["GUID"] = "Vehicle-0-3138-669-87-41570-000031522B",
			["LastEventHealth"] = {
				"7733120 (80%)", -- [1]
				"7596682 (79%)", -- [2]
				"7460244 (77%)", -- [3]
				"7371690 (76%)", -- [4]
				"7098814 (73%)", -- [5]
				"6825938 (71%)", -- [6]
				"6689500 (69%)", -- [7]
				"6553062 (68%)", -- [8]
				"6553062 (68%)", -- [9]
				"5848300 (60%)", -- [10]
				"5759746 (59%)", -- [11]
				"5476777 (57%)", -- [12]
				"5476777 (57%)", -- [13]
				"5179645 (53%)", -- [14]
				"5002538 (52%)", -- [15]
				"5002538 (52%)", -- [16]
				"4913985 (51%)", -- [17]
				"4610679 (48%)", -- [18]
				"4610679 (48%)", -- [19]
				"4522125 (47%)", -- [20]
				"3669243 (38%)", -- [21]
				"3428149 (35%)", -- [22]
				"3428149 (35%)", -- [23]
				"3339596 (34%)", -- [24]
				"2637168 (27%)", -- [25]
				"1179092 (12%)", -- [26]
				"1090539 (11%)", -- [27]
				"1090539 (11%)", -- [28]
				"913431 (9%)", -- [29]
				"672337 (7%)", -- [30]
				"1 (0%)", -- [31]
				"1 (0%)", -- [32]
				"1 (0%)", -- [33]
				"1 (0%)", -- [34]
				"0 (0%)", -- [35]
				"2486986 (25%)", -- [36]
				"2486986 (25%)", -- [37]
				"2486986 (25%)", -- [38]
				"???", -- [39]
				"???", -- [40]
				"9338595 (97%)", -- [41]
				"9202157 (95%)", -- [42]
				"8961062 (93%)", -- [43]
				"8824624 (91%)", -- [44]
				"8688186 (90%)", -- [45]
				"8551748 (89%)", -- [46]
				"8415310 (87%)", -- [47]
				"8142434 (84%)", -- [48]
				"8005996 (83%)", -- [49]
				"7869558 (81%)", -- [50]
			},
			["LastEventType"] = {
				"DAMAGE", -- [1]
				"DAMAGE", -- [2]
				"DAMAGE", -- [3]
				"DAMAGE", -- [4]
				"DAMAGE", -- [5]
				"DAMAGE", -- [6]
				"DAMAGE", -- [7]
				"DAMAGE", -- [8]
				"DAMAGE", -- [9]
				"DAMAGE", -- [10]
				"DAMAGE", -- [11]
				"DAMAGE", -- [12]
				"DAMAGE", -- [13]
				"DAMAGE", -- [14]
				"DAMAGE", -- [15]
				"DAMAGE", -- [16]
				"DAMAGE", -- [17]
				"DAMAGE", -- [18]
				"DAMAGE", -- [19]
				"DAMAGE", -- [20]
				"DAMAGE", -- [21]
				"DAMAGE", -- [22]
				"DAMAGE", -- [23]
				"DAMAGE", -- [24]
				"DAMAGE", -- [25]
				"DAMAGE", -- [26]
				"DAMAGE", -- [27]
				"DAMAGE", -- [28]
				"DAMAGE", -- [29]
				"DAMAGE", -- [30]
				"DAMAGE", -- [31]
				"DAMAGE", -- [32]
				"DAMAGE", -- [33]
				"DAMAGE", -- [34]
				"MISC", -- [35]
				"DAMAGE", -- [36]
				"DAMAGE", -- [37]
				"DAMAGE", -- [38]
				"DAMAGE", -- [39]
				"DAMAGE", -- [40]
				"DAMAGE", -- [41]
				"DAMAGE", -- [42]
				"DAMAGE", -- [43]
				"DAMAGE", -- [44]
				"DAMAGE", -- [45]
				"DAMAGE", -- [46]
				"DAMAGE", -- [47]
				"DAMAGE", -- [48]
				"DAMAGE", -- [49]
				"DAMAGE", -- [50]
			},
			["TimeWindows"] = {
				["DeathCount"] = {
					1, -- [1]
				},
				["ActiveTime"] = {
					53.47, -- [1]
				},
				["TimeDamage"] = {
					53.47, -- [1]
				},
				["DamageTaken"] = {
					17738212, -- [1]
				},
				["Damage"] = {
					114354, -- [1]
				},
			},
			["enClass"] = "MOB",
			["LastDamageTaken"] = 852882,
			["DeathLogs"] = {
				{
					["MessageIncoming"] = {
						true, -- [1]
						true, -- [2]
						false, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						true, -- [7]
						true, -- [8]
						false, -- [9]
						true, -- [10]
						true, -- [11]
						true, -- [12]
						false, -- [13]
						true, -- [14]
						true, -- [15]
						true, -- [16]
						true, -- [17]
						false, -- [18]
						true, -- [19]
						true, -- [20]
						true, -- [21]
						false, -- [22]
						true, -- [23]
						true, -- [24]
						true, -- [25]
					},
					["Messages"] = {
						"Falriçk Auto Shot Magmaw Hit -88554 (Physical)", -- [1]
						"Falriçk Aimed Shot Magmaw Hit -282969 (Physical)", -- [2]
						"Magmaw Magma Spit Falriçk Hit -2595 (Fire)", -- [3]
						"Falriçk Aimed Shot Magmaw Hit -297132 (Physical)", -- [4]
						"Falriçk Auto Shot Magmaw Crit -177107 (Physical)", -- [5]
						"Magmaw Magma Spit Falriçk Hit -6105 (Fire)", -- [6]
						"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [7]
						"Falriçk Aimed Shot Magmaw Hit -303306 (Physical)", -- [8]
						"Magmaw Magma Spit Falriçk Hit -4590 (Fire)", -- [9]
						"Falriçk Auto Shot Magmaw Hit -88554 (Physical)", -- [10]
						"Falriçk Marked Shot Magmaw Hit -852882 (Physical)", -- [11]
						"Falriçk Sidewinders Magmaw Hit -241094 (Nature)", -- [12]
						"Magmaw Magma Spit Falriçk Hit -5480 (Fire)", -- [13]
						"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [14]
						"Falriçk Aimed Shot Magmaw Hit -702428 (Physical)", -- [15]
						"Falriçk Aimed Shot Magmaw Crit -1458076 (Physical)", -- [16]
						"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [17]
						"Magmaw Magma Spit Falriçk Hit -6485 (Fire)", -- [18]
						"Falriçk Auto Shot Magmaw Crit -177108 (Physical)", -- [19]
						"Falriçk Sidewinders Magmaw Hit -241094 (Nature)", -- [20]
						"Falriçk Aimed Shot Magmaw Hit -747588 (Physical)", -- [21]
						"Magmaw Magma Spit Falriçk Hit -9732 (Fire)", -- [22]
						"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [23]
						"Falriçk Marked Shot Magmaw Hit -852882 (Physical)", -- [24]
						"Magmaw dies.", -- [25]
					},
					["DeathAt"] = 398600.356,
					["HealthNum"] = {
						59.9707671265739, -- [1]
						57.0244795640599, -- [2]
						57.0244795640599, -- [3]
						53.9307261280832, -- [4]
						52.086679072278, -- [5]
						52.086679072278, -- [6]
						51.1646607504007, -- [7]
						48.0066233136643, -- [8]
						48.0066233136643, -- [9]
						47.0845945797363, -- [10]
						38.2043439908307, -- [11]
						35.6940610496013, -- [12]
						35.6940610496013, -- [13]
						34.772042727724, -- [14]
						27.4583268084483, -- [15]
						12.2767656339023, -- [16]
						11.354747312025, -- [17]
						11.354747312025, -- [18]
						9.51068984416909, -- [19]
						7.0004069029397, -- [20]
						1.041205065754180e-005, -- [21]
						1.041205065754180e-005, -- [22]
						1.041205065754180e-005, -- [23]
						1.041205065754180e-005, -- [24]
						0, -- [25]
					},
					["MessageTimes"] = {
						-14.195000000007, -- [1]
						-13.63400000002, -- [2]
						-13.2220000000088, -- [3]
						-12.0030000000261, -- [4]
						-11.7129999999888, -- [5]
						-10.5360000000219, -- [6]
						-9.27399999997579, -- [7]
						-8.42200000002049, -- [8]
						-8.375, -- [9]
						-7.52199999999721, -- [10]
						-6.9089999999851, -- [11]
						-6.59899999998743, -- [12]
						-5.95699999999488, -- [13]
						-5.76299999997718, -- [14]
						-5.45199999999022, -- [15]
						-4.29100000002654, -- [16]
						-4.01900000002934, -- [17]
						-3.52700000000186, -- [18]
						-2.27000000001863, -- [19]
						-1.29800000000978, -- [20]
						-1.07000000000698, -- [21]
						-0.831999999994878, -- [22]
						-0.519000000029337, -- [23]
						-0.470000000030268, -- [24]
						0, -- [25]
					},
					["KilledBy"] = "Falriçk",
					["Health"] = {
						"5759746 (59%)", -- [1]
						"5476777 (57%)", -- [2]
						"5476777 (57%)", -- [3]
						"5179645 (53%)", -- [4]
						"5002538 (52%)", -- [5]
						"5002538 (52%)", -- [6]
						"4913985 (51%)", -- [7]
						"4610679 (48%)", -- [8]
						"4610679 (48%)", -- [9]
						"4522125 (47%)", -- [10]
						"3669243 (38%)", -- [11]
						"3428149 (35%)", -- [12]
						"3428149 (35%)", -- [13]
						"3339596 (34%)", -- [14]
						"2637168 (27%)", -- [15]
						"1179092 (12%)", -- [16]
						"1090539 (11%)", -- [17]
						"1090539 (11%)", -- [18]
						"913431 (9%)", -- [19]
						"672337 (7%)", -- [20]
						"1 (0%)", -- [21]
						"1 (0%)", -- [22]
						"1 (0%)", -- [23]
						"1 (0%)", -- [24]
						"0 (0%)", -- [25]
					},
					["EventNum"] = {
						0.922028733927959, -- [1]
						2.94628756251395, -- [2]
						0, -- [3]
						3.09375343597672, -- [4]
						1.84404705580526, -- [5]
						0, -- [6]
						0.922018321877301, -- [7]
						3.15803743673638, -- [8]
						0, -- [9]
						0.922028733927959, -- [10]
						8.88025058890559, -- [11]
						2.51028294122939, -- [12]
						0, -- [13]
						0.922018321877301, -- [14]
						7.31371591927579, -- [15]
						15.181561174546, -- [16]
						0.922018321877301, -- [17]
						0, -- [18]
						1.84405746785592, -- [19]
						2.51028294122939, -- [20]
						7.78392412697038, -- [21]
						0, -- [22]
						0.922018321877301, -- [23]
						8.88025058890559, -- [24]
						0, -- [25]
					},
					["MessageType"] = {
						"DAMAGE", -- [1]
						"DAMAGE", -- [2]
						"DAMAGE", -- [3]
						"DAMAGE", -- [4]
						"DAMAGE", -- [5]
						"DAMAGE", -- [6]
						"DAMAGE", -- [7]
						"DAMAGE", -- [8]
						"DAMAGE", -- [9]
						"DAMAGE", -- [10]
						"DAMAGE", -- [11]
						"DAMAGE", -- [12]
						"DAMAGE", -- [13]
						"DAMAGE", -- [14]
						"DAMAGE", -- [15]
						"DAMAGE", -- [16]
						"DAMAGE", -- [17]
						"DAMAGE", -- [18]
						"DAMAGE", -- [19]
						"DAMAGE", -- [20]
						"DAMAGE", -- [21]
						"DAMAGE", -- [22]
						"DAMAGE", -- [23]
						"DAMAGE", -- [24]
						"MISC", -- [25]
					},
				}, -- [1]
			},
			["level"] = -1,
			["LastDamageAbility"] = "Marked Shot",
			["LastFightIn"] = 2,
			["LastEventNum"] = {
				1.42059936761369, -- [1]
				1.42059936761369, -- [2]
				1.42059936761369, -- [3]
				0.922028733927959, -- [4]
				2.84119873522738, -- [5]
				2.84119873522738, -- [6]
				1.42059936761369, -- [7]
				1.42059936761369, -- [8]
				nil, -- [9]
				7.33801764551049, -- [10]
				0.922028733927959, -- [11]
				2.94628756251395, -- [12]
				nil, -- [13]
				3.09375343597672, -- [14]
				1.84404705580526, -- [15]
				[17] = 0.922018321877301,
				[18] = 3.15803743673638,
				[20] = 0.922028733927959,
				[21] = 8.88025058890559,
				[22] = 2.51028294122939,
				[24] = 0.922018321877301,
				[25] = 7.31371591927579,
				[26] = 15.181561174546,
				[27] = 0.922018321877301,
				[29] = 1.84405746785592,
				[30] = 2.51028294122939,
				[31] = 7.78392412697038,
				[46] = 1.42059936761369,
				[48] = 2.84119873522738,
				[50] = 1.42059936761369,
				[41] = 1.84404705580526,
				[43] = 2.51029335328005,
				[45] = 1.42059936761369,
				[47] = 1.42059936761369,
				[49] = 1.42059936761369,
				[34] = 8.88025058890559,
				[42] = 1.42059936761369,
				[44] = 1.42059936761369,
				[33] = 0.922018321877301,
			},
			["type"] = "Boss",
			["FightsSaved"] = 5,
			["LastAbility"] = 398598.455,
			["unit"] = "playertarget",
			["Owner"] = false,
			["TimeLast"] = {
				["DeathCount"] = 398598.356,
				["ActiveTime"] = 398598.356,
				["TimeDamage"] = 398598.356,
				["OVERALL"] = 398598.356,
				["DamageTaken"] = 398598.356,
				["Damage"] = 398598.356,
			},
			["NextEventNum"] = 36,
			["LastEventHealthNum"] = {
				80.5176371808498, -- [1]
				79.0970378132361, -- [2]
				77.6764384456224, -- [3]
				76.7544097116945, -- [4]
				73.9132109764671, -- [5]
				71.0720122412397, -- [6]
				69.651412873626, -- [7]
				68.2308135060124, -- [8]
				68.2308135060124, -- [9]
				60.8927958605019, -- [10]
				59.9707671265739, -- [11]
				57.0244795640599, -- [12]
				57.0244795640599, -- [13]
				53.9307261280832, -- [14]
				52.086679072278, -- [15]
				52.086679072278, -- [16]
				51.1646607504007, -- [17]
				48.0066233136643, -- [18]
				48.0066233136643, -- [19]
				47.0845945797363, -- [20]
				38.2043439908307, -- [21]
				35.6940610496013, -- [22]
				35.6940610496013, -- [23]
				34.772042727724, -- [24]
				27.4583268084483, -- [25]
				12.2767656339023, -- [26]
				11.354747312025, -- [27]
				11.354747312025, -- [28]
				9.51068984416909, -- [29]
				7.0004069029397, -- [30]
				1.041205065754180e-005, -- [31]
				1.041205065754180e-005, -- [32]
				1.041205065754180e-005, -- [33]
				1.041205065754180e-005, -- [34]
				0, -- [35]
				25.8946242165973, -- [36]
				25.8946242165973, -- [37]
				25.8946242165973, -- [38]
				0, -- [39]
				0, -- [40]
				97.2339242102668, -- [41]
				95.8133248426531, -- [42]
				93.303031489373, -- [43]
				91.8824321217594, -- [44]
				90.4618327541457, -- [45]
				89.041233386532, -- [46]
				87.6206340189183, -- [47]
				84.7794352836909, -- [48]
				83.3588359160772, -- [49]
				81.9382365484635, -- [50]
			},
			["LastEvents"] = {
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [1]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [2]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [3]
				"Falriçk Auto Shot Magmaw Hit -88554 (Physical)", -- [4]
				"Falriçk Barrage Magmaw Crit -272876 (Physical)", -- [5]
				"Falriçk Barrage Magmaw Crit -272876 (Physical)", -- [6]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [7]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [8]
				"Magmaw Magma Spit Falriçk Hit -1609 (Fire)", -- [9]
				"Falriçk Aimed Shot Magmaw Hit -704762 (Physical)", -- [10]
				"Falriçk Auto Shot Magmaw Hit -88554 (Physical)", -- [11]
				"Falriçk Aimed Shot Magmaw Hit -282969 (Physical)", -- [12]
				"Magmaw Magma Spit Falriçk Hit -2595 (Fire)", -- [13]
				"Falriçk Aimed Shot Magmaw Hit -297132 (Physical)", -- [14]
				"Falriçk Auto Shot Magmaw Crit -177107 (Physical)", -- [15]
				"Magmaw Magma Spit Falriçk Hit -6105 (Fire)", -- [16]
				"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [17]
				"Falriçk Aimed Shot Magmaw Hit -303306 (Physical)", -- [18]
				"Magmaw Magma Spit Falriçk Hit -4590 (Fire)", -- [19]
				"Falriçk Auto Shot Magmaw Hit -88554 (Physical)", -- [20]
				"Falriçk Marked Shot Magmaw Hit -852882 (Physical)", -- [21]
				"Falriçk Sidewinders Magmaw Hit -241094 (Nature)", -- [22]
				"Magmaw Magma Spit Falriçk Hit -5480 (Fire)", -- [23]
				"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [24]
				"Falriçk Aimed Shot Magmaw Hit -702428 (Physical)", -- [25]
				"Falriçk Aimed Shot Magmaw Crit -1458076 (Physical)", -- [26]
				"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [27]
				"Magmaw Magma Spit Falriçk Hit -6485 (Fire)", -- [28]
				"Falriçk Auto Shot Magmaw Crit -177108 (Physical)", -- [29]
				"Falriçk Sidewinders Magmaw Hit -241094 (Nature)", -- [30]
				"Falriçk Aimed Shot Magmaw Hit -747588 (Physical)", -- [31]
				"Magmaw Magma Spit Falriçk Hit -9732 (Fire)", -- [32]
				"Falriçk Auto Shot Magmaw Hit -88553 (Physical)", -- [33]
				"Falriçk Marked Shot Magmaw Hit -852882 (Physical)", -- [34]
				"Magmaw dies.", -- [35]
				"Magmaw Magma Spit Falriçk Hit -9408 (Fire)", -- [36]
				"Magmaw Magma Spit Falriçk Hit -11673 (Fire)", -- [37]
				"Magmaw Magma Spit Falriçk Hit -1430 (Fire)", -- [38]
				"Magmaw Magma Spit Falriçk Hit -2637 (Fire)", -- [39]
				"Falriçk Auto Shot Magmaw Hit -88554 (Physical)", -- [40]
				"Falriçk Auto Shot Magmaw Crit -177107 (Physical)", -- [41]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [42]
				"Falriçk Sidewinders Magmaw Hit -241095 (Nature)", -- [43]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [44]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [45]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [46]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [47]
				"Falriçk Barrage Magmaw Crit -272876 (Physical)", -- [48]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [49]
				"Falriçk Barrage Magmaw Hit -136438 (Physical)", -- [50]
			},
			["Name"] = "Magmaw",
			["LastEventIncoming"] = {
				true, -- [1]
				true, -- [2]
				true, -- [3]
				true, -- [4]
				true, -- [5]
				true, -- [6]
				true, -- [7]
				true, -- [8]
				false, -- [9]
				true, -- [10]
				true, -- [11]
				true, -- [12]
				false, -- [13]
				true, -- [14]
				true, -- [15]
				false, -- [16]
				true, -- [17]
				true, -- [18]
				false, -- [19]
				true, -- [20]
				true, -- [21]
				true, -- [22]
				false, -- [23]
				true, -- [24]
				true, -- [25]
				true, -- [26]
				true, -- [27]
				false, -- [28]
				true, -- [29]
				true, -- [30]
				true, -- [31]
				false, -- [32]
				true, -- [33]
				true, -- [34]
				true, -- [35]
				false, -- [36]
				false, -- [37]
				false, -- [38]
				false, -- [39]
				true, -- [40]
				true, -- [41]
				true, -- [42]
				true, -- [43]
				true, -- [44]
				true, -- [45]
				true, -- [46]
				true, -- [47]
				true, -- [48]
				true, -- [49]
				true, -- [50]
			},
			["LastEventTimes"] = {
				398582.171, -- [1]
				398582.333, -- [2]
				398582.512, -- [3]
				398582.656, -- [4]
				398582.665, -- [5]
				398582.838, -- [6]
				398583.006, -- [7]
				398583.166, -- [8]
				398583.626, -- [9]
				398584.009, -- [10]
				398585.092, -- [11]
				398585.653, -- [12]
				398586.065, -- [13]
				398587.284, -- [14]
				398587.574, -- [15]
				398588.751, -- [16]
				398590.013, -- [17]
				398590.865, -- [18]
				398590.912, -- [19]
				398591.765, -- [20]
				398592.378, -- [21]
				398592.688, -- [22]
				398593.33, -- [23]
				398593.524, -- [24]
				398593.835, -- [25]
				398594.996, -- [26]
				398595.268, -- [27]
				398595.76, -- [28]
				398597.017, -- [29]
				398597.989, -- [30]
				398598.217, -- [31]
				398598.455, -- [32]
				398598.768, -- [33]
				398598.817, -- [34]
				398599.287, -- [35]
				398508.879, -- [36]
				398511.844, -- [37]
				398525.297, -- [38]
				398530.874, -- [39]
				398577.616, -- [40]
				398579.981, -- [41]
				398580.866, -- [42]
				398580.954, -- [43]
				398581.04, -- [44]
				398581.206, -- [45]
				398581.361, -- [46]
				398581.524, -- [47]
				398581.697, -- [48]
				398581.86, -- [49]
				398582.016, -- [50]
			},
			["Fights"] = {
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["OverallData"] = {
					["TimeDamaging"] = {
						["Falriçk"] = {
							["Details"] = {
								["Magma Spit"] = {
									["count"] = 47.46,
								},
								["Lava Spew"] = {
									["count"] = 6.01,
								},
							},
							["amount"] = 53.47,
						},
					},
					["ElementHitsDone"] = {
						["Fire"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 21,
								},
							},
							["amount"] = 21,
						},
					},
					["DamagedWho"] = {
						["Falriçk"] = {
							["Details"] = {
								["Magma Spit"] = {
									["count"] = 98285,
								},
								["Lava Spew"] = {
									["count"] = 16069,
								},
							},
							["amount"] = 114354,
						},
					},
					["ElementDone"] = {
						["Fire"] = 114354,
					},
					["Attacks"] = {
						["Magma Spit"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 11673,
									["min"] = 1430,
									["count"] = 18,
									["amount"] = 98285,
								},
							},
							["count"] = 18,
							["amount"] = 98285,
						},
						["Lava Spew"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 5790,
									["min"] = 5135,
									["count"] = 3,
									["amount"] = 16069,
								},
							},
							["count"] = 3,
							["amount"] = 16069,
						},
					},
					["ElementHitsTaken"] = {
						["Physical"] = {
							["Details"] = {
								["Crit"] = {
									["count"] = 15,
								},
								["Hit"] = {
									["count"] = 43,
								},
							},
							["amount"] = 58,
						},
						["Nature"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 5,
								},
							},
							["amount"] = 5,
						},
					},
					["DamageTaken"] = 17738212,
					["TimeSpent"] = {
						["Falriçk"] = {
							["Details"] = {
								["Magma Spit"] = {
									["count"] = 47.46,
								},
								["Lava Spew"] = {
									["count"] = 6.01,
								},
							},
							["amount"] = 53.47,
						},
					},
					["PartialResist"] = {
						["Sidewinders"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 5,
						},
						["Auto Shot"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 19,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 19,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 11,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 11,
						},
						["Marked Shot"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 3,
						},
						["Barrage"] = {
							["Details"] = {
								["No Resist"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 25,
									["amount"] = 0,
								},
							},
							["count"] = 0,
							["amount"] = 25,
						},
					},
					["DeathCount"] = 1,
					["PartialAbsorb"] = {
						["Sidewinders"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 5,
									["amount"] = 0,
								},
							},
							["count"] = 5,
							["amount"] = 0,
						},
						["Auto Shot"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 19,
									["amount"] = 0,
								},
							},
							["count"] = 19,
							["amount"] = 0,
						},
						["Aimed Shot"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 11,
									["amount"] = 0,
								},
							},
							["count"] = 11,
							["amount"] = 0,
						},
						["Marked Shot"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 3,
									["amount"] = 0,
								},
							},
							["count"] = 3,
							["amount"] = 0,
						},
						["Barrage"] = {
							["Details"] = {
								["No Absorb"] = {
									["max"] = 0,
									["min"] = 0,
									["count"] = 25,
									["amount"] = 0,
								},
							},
							["count"] = 25,
							["amount"] = 0,
						},
					},
					["ActiveTime"] = 53.47,
					["WhoDamaged"] = {
						["Falriçk"] = {
							["Details"] = {
								["Sidewinders"] = {
									["count"] = 1205471,
								},
								["Auto Shot"] = {
									["count"] = 2036734,
								},
								["Aimed Shot"] = {
									["count"] = 7298467,
								},
								["Marked Shot"] = {
									["count"] = 2558646,
								},
								["Barrage"] = {
									["count"] = 4638894,
								},
							},
							["amount"] = 17738212,
						},
					},
					["ElementTaken"] = {
						["Physical"] = 16532741,
						["Nature"] = 1205471,
					},
					["TimeDamage"] = 53.47,
					["Damage"] = 114354,
				},
			},
			["UnitLockout"] = 398579.363,
			["LastActive"] = 398598.356,
		},
		["Arcanotron"] = {
			["GUID"] = "Vehicle-0-3783-669-31546-42166-0000315283",
			["LastEventHealth"] = {
				"???", -- [1]
			},
			["LastEventType"] = {
				"MISC", -- [1]
			},
			["TimeWindows"] = {
				["DeathCount"] = {
					1, -- [1]
				},
			},
			["enClass"] = "MOB",
			["level"] = -1,
			["LastFightIn"] = 4,
			["type"] = "Boss",
			["FightsSaved"] = 5,
			["DeathLogs"] = {
				{
					["MessageTimes"] = {
						0, -- [1]
					},
					["MessageIncoming"] = {
						true, -- [1]
					},
					["Messages"] = {
						"Arcanotron dies.", -- [1]
					},
					["EventNum"] = {
						0, -- [1]
					},
					["DeathAt"] = 398692.356,
					["HealthNum"] = {
						0, -- [1]
					},
					["Health"] = {
						"???", -- [1]
					},
					["MessageType"] = {
						"MISC", -- [1]
					},
				}, -- [1]
			},
			["Owner"] = false,
			["NextEventNum"] = 2,
			["LastEventHealthNum"] = {
				0, -- [1]
			},
			["LastEvents"] = {
				"Arcanotron dies.", -- [1]
			},
			["Name"] = "Arcanotron",
			["TimeLast"] = {
				["OVERALL"] = 398690.368,
				["DeathCount"] = 398690.368,
			},
			["LastEventIncoming"] = {
				true, -- [1]
			},
			["LastEventTimes"] = {
				398690.384, -- [1]
			},
			["UnitLockout"] = 398690.368,
			["Fights"] = {
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["OverallData"] = {
					["DeathCount"] = 1,
				},
			},
		},
		["Hydra <Falriçk>"] = {
			["GUID"] = "Pet-0-3783-669-31546-43296-0102809CF0",
			["LastEventHealth"] = {
				"232470 (100%)", -- [1]
			},
			["LastEventType"] = {
				"DAMAGE", -- [1]
			},
			["TimeWindows"] = {
				["Damage"] = {
					39561, -- [1]
				},
				["TimeDamage"] = {
					3.5, -- [1]
				},
				["ActiveTime"] = {
					3.5, -- [1]
				},
			},
			["enClass"] = "PET",
			["unit"] = "Hydra",
			["level"] = 1,
			["LastFightIn"] = 12,
			["type"] = "Pet",
			["FightsSaved"] = 1,
			["TimeLast"] = {
				["Damage"] = 399667.36,
				["OVERALL"] = 399667.36,
				["TimeDamage"] = 399667.36,
				["ActiveTime"] = 399667.36,
			},
			["LastAbility"] = 399667.634,
			["NextEventNum"] = 2,
			["LastEventHealthNum"] = {
				100, -- [1]
			},
			["LastEvents"] = {
				"Hydra <Falriçk> Melee Chimaeron Hit -39561 (Physical)", -- [1]
			},
			["Name"] = "Hydra",
			["LastEventIncoming"] = {
				false, -- [1]
			},
			["LastEventTimes"] = {
				399667.634, -- [1]
			},
			["Fights"] = {
				["Fight1"] = {
					["TimeSpent"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["ElementDone"] = {
						["Melee"] = 39561,
					},
					["ElementHitsDone"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
					},
					["DamagedWho"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 39561,
								},
							},
							["amount"] = 39561,
						},
					},
					["TimeDamage"] = 3.5,
					["TimeDamaging"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["Attacks"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 39561,
									["min"] = 39561,
									["count"] = 1,
									["amount"] = 39561,
								},
							},
							["count"] = 1,
							["amount"] = 39561,
						},
					},
					["ActiveTime"] = 3.5,
					["Damage"] = 39561,
				},
				["LastFightData"] = {
					["TimeSpent"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["ElementDone"] = {
						["Melee"] = 39561,
					},
					["ElementHitsDone"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
					},
					["DamagedWho"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 39561,
								},
							},
							["amount"] = 39561,
						},
					},
					["TimeDamage"] = 3.5,
					["TimeDamaging"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["Attacks"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 39561,
									["min"] = 39561,
									["count"] = 1,
									["amount"] = 39561,
								},
							},
							["count"] = 1,
							["amount"] = 39561,
						},
					},
					["ActiveTime"] = 3.5,
					["Damage"] = 39561,
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["OverallData"] = {
					["TimeSpent"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["ElementDone"] = {
						["Melee"] = 39561,
					},
					["ElementHitsDone"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["count"] = 1,
								},
							},
							["amount"] = 1,
						},
					},
					["DamagedWho"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 39561,
								},
							},
							["amount"] = 39561,
						},
					},
					["TimeDamage"] = 3.5,
					["TimeDamaging"] = {
						["Chimaeron"] = {
							["Details"] = {
								["Melee"] = {
									["count"] = 3.5,
								},
							},
							["amount"] = 3.5,
						},
					},
					["Attacks"] = {
						["Melee"] = {
							["Details"] = {
								["Hit"] = {
									["max"] = 39561,
									["min"] = 39561,
									["count"] = 1,
									["amount"] = 39561,
								},
							},
							["count"] = 1,
							["amount"] = 39561,
						},
					},
					["ActiveTime"] = 3.5,
					["Damage"] = 39561,
				},
			},
			["UnitLockout"] = 399667.36,
			["LastActive"] = 399667.36,
		},
		["Electron"] = {
			["GUID"] = "Vehicle-0-3783-669-31546-42179-0000315283",
			["LastEventHealth"] = {
				"???", -- [1]
			},
			["LastEventType"] = {
				"MISC", -- [1]
			},
			["TimeWindows"] = {
				["DeathCount"] = {
					1, -- [1]
				},
			},
			["enClass"] = "MOB",
			["level"] = -1,
			["LastFightIn"] = 4,
			["type"] = "Boss",
			["FightsSaved"] = 5,
			["DeathLogs"] = {
				{
					["MessageTimes"] = {
						0, -- [1]
					},
					["MessageIncoming"] = {
						true, -- [1]
					},
					["Messages"] = {
						"Electron dies.", -- [1]
					},
					["EventNum"] = {
						0, -- [1]
					},
					["DeathAt"] = 398692.356,
					["HealthNum"] = {
						0, -- [1]
					},
					["Health"] = {
						"???", -- [1]
					},
					["MessageType"] = {
						"MISC", -- [1]
					},
				}, -- [1]
			},
			["Owner"] = false,
			["NextEventNum"] = 2,
			["LastEventHealthNum"] = {
				0, -- [1]
			},
			["LastEvents"] = {
				"Electron dies.", -- [1]
			},
			["Name"] = "Electron",
			["TimeLast"] = {
				["OVERALL"] = 398690.368,
				["DeathCount"] = 398690.368,
			},
			["LastEventIncoming"] = {
				true, -- [1]
			},
			["LastEventTimes"] = {
				398690.384, -- [1]
			},
			["UnitLockout"] = 398690.368,
			["Fights"] = {
				["LastFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["CurrentFightData"] = {
					["DOTs"] = {
					},
					["ElementDoneResist"] = {
					},
					["Ressed"] = 0,
					["DamageTaken"] = 0,
					["RageGainedFrom"] = {
					},
					["ElementHitsTaken"] = {
					},
					["DeathCount"] = 0,
					["HOT_Time"] = 0,
					["ElementHitsDone"] = {
					},
					["ElementTakenAbsorb"] = {
					},
					["ElementTaken"] = {
					},
					["DOT_Time"] = 0,
					["Damage"] = 0,
					["ElementTakenBlock"] = {
					},
					["TimeHeal"] = 0,
					["RessedWho"] = {
					},
					["Dispels"] = 0,
					["ElementTakenResist"] = {
					},
					["ElementDoneAbsorb"] = {
					},
					["FAttacks"] = {
					},
					["RunicPowerGainedFrom"] = {
					},
					["ElementDone"] = {
					},
					["PartialAbsorb"] = {
					},
					["DamagedWho"] = {
					},
					["PartialBlock"] = {
					},
					["WhoDamaged"] = {
					},
					["EnergyGainedFrom"] = {
					},
					["PartialResist"] = {
					},
					["CCBroken"] = {
					},
					["ElementDoneBlock"] = {
					},
					["TimeHealing"] = {
					},
					["OverHeals"] = {
					},
					["ManaGainedFrom"] = {
					},
					["RunicPowerGained"] = {
					},
					["CCBreak"] = 0,
					["RageGained"] = {
					},
					["HealedWho"] = {
					},
					["EnergyGain"] = 0,
					["ManaGained"] = {
					},
					["FDamage"] = 0,
					["Interrupts"] = 0,
					["Overhealing"] = 0,
					["TimeSpent"] = {
					},
					["WhoDispelled"] = {
					},
					["InterruptData"] = {
					},
					["RunicPowerGain"] = 0,
					["Heals"] = {
					},
					["WhoHealed"] = {
					},
					["EnergyGained"] = {
					},
					["ActiveTime"] = 0,
					["Healing"] = 0,
					["FDamagedWho"] = {
					},
					["Dispelled"] = 0,
					["Attacks"] = {
					},
					["HealingTaken"] = 0,
					["RageGain"] = 0,
					["TimeDamage"] = 0,
					["TimeDamaging"] = {
					},
					["ManaGain"] = 0,
					["HOTs"] = {
					},
					["DispelledWho"] = {
					},
				},
				["OverallData"] = {
					["DeathCount"] = 1,
				},
			},
		},
	},
	["FightNum"] = 13,
	["CombatTimes"] = {
		{
			398473.238, -- [1]
			398532.967, -- [2]
			"00:25:20", -- [3]
			"00:26:18", -- [4]
			"Magmaw", -- [5]
		}, -- [1]
		{
			398569.364, -- [1]
			398576.363, -- [2]
			"00:26:55", -- [3]
			"00:27:02", -- [4]
			"Drakonid Drudge", -- [5]
		}, -- [2]
		{
			398577.36, -- [1]
			398601.364, -- [2]
			"00:27:03", -- [3]
			"00:27:27", -- [4]
			"Magmaw", -- [5]
		}, -- [3]
		{
			398650.36, -- [1]
			398664.357, -- [2]
			"00:28:16", -- [3]
			"00:28:30", -- [4]
			"Golem Sentry", -- [5]
		}, -- [4]
		{
			398826.357, -- [1]
			398833.357, -- [2]
			"00:31:13", -- [3]
			"00:31:19", -- [4]
			"Maimgor", -- [5]
		}, -- [5]
		{
			398901.36, -- [1]
			398905.357, -- [2]
			"00:32:28", -- [3]
			"00:32:31", -- [4]
			"Drakonid Slayer", -- [5]
		}, -- [6]
		{
			398909.36, -- [1]
			398919.361, -- [2]
			"00:32:35", -- [3]
			"00:32:45", -- [4]
			"Ivoroc", -- [5]
		}, -- [7]
		{
			398955.358, -- [1]
			398964.358, -- [2]
			"00:33:21", -- [3]
			"00:33:30", -- [4]
			"Spirit of Burningeye", -- [5]
		}, -- [8]
		{
			398965.364, -- [1]
			398979.358, -- [2]
			"00:33:32", -- [3]
			"00:33:45", -- [4]
			"Spirit of Corehammer", -- [5]
		}, -- [9]
		{
			399124.358, -- [1]
			399143.363, -- [2]
			"00:36:11", -- [3]
			"00:36:29", -- [4]
			"Atramedes", -- [5]
		}, -- [10]
		{
			399173.36, -- [1]
			399177.361, -- [2]
			"00:37:00", -- [3]
			"00:37:03", -- [4]
			"Drakonid Slayer", -- [5]
		}, -- [11]
		{
			399186.362, -- [1]
			399192.356, -- [2]
			"00:37:13", -- [3]
			"00:37:18", -- [4]
			"Pyrecraw", -- [5]
		}, -- [12]
		{
			399611.356, -- [1]
			399668.361, -- [2]
			"00:44:18", -- [3]
			"00:45:14", -- [4]
			"Chimaeron", -- [5]
		}, -- [13]
	},
	["FoughtWho"] = {
		"Chimaeron 00:44:18-00:45:14", -- [1]
		"Pyrecraw 00:37:13-00:37:18", -- [2]
		"Drakonid Slayer 00:37:00-00:37:03", -- [3]
		"Atramedes 00:36:11-00:36:29", -- [4]
		"Spirit of Corehammer 00:33:32-00:33:45", -- [5]
	},
}
