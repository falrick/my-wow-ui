
DataStore_SpellsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471225165,
				["Spells"] = {
					["Protection"] = {
						8175360, -- [1]
						52232960, -- [2]
						54692864, -- [3]
						6802688, -- [4]
						127488, -- [5]
						164352, -- [6]
						48840704, -- [7]
						5056000, -- [8]
						218368, -- [9]
						9061120, -- [10]
						15903744, -- [11]
						5189376, -- [12]
						162048, -- [13]
						47127552, -- [14]
						24635136, -- [15]
						1875968, -- [16]
						13721600, -- [17]
						21771008, -- [18]
						8249088, -- [19]
						261680, -- [20]
						267316, -- [21]
						1776696, -- [22]
						8153665, -- [23]
						8162376, -- [24]
						19627854, -- [25]
						22184787, -- [26]
					},
					["Holy"] = {
						54286336, -- [1]
						8146176, -- [2]
						8151552, -- [3]
						13712128, -- [4]
						267264, -- [5]
						261632, -- [6]
						1776640, -- [7]
						1276672, -- [8]
						6802688, -- [9]
						9061120, -- [10]
						127488, -- [11]
						164352, -- [12]
						48840704, -- [13]
						5056000, -- [14]
						218368, -- [15]
						15903744, -- [16]
						21075456, -- [17]
						5241088, -- [18]
						5189376, -- [19]
						162048, -- [20]
						21816832, -- [21]
						47103488, -- [22]
						1875968, -- [23]
						8249088, -- [24]
						13715456, -- [25]
						47103232, -- [26]
						58129408, -- [27]
					},
					["Retribution"] = {
						8162304, -- [1]
						47251200, -- [2]
						267264, -- [3]
						261632, -- [4]
						54692864, -- [5]
						9061120, -- [6]
						164352, -- [7]
						48840704, -- [8]
						13666560, -- [9]
						5056000, -- [10]
						218368, -- [11]
						46903808, -- [12]
						15903744, -- [13]
						5189376, -- [14]
						162048, -- [15]
						24635136, -- [16]
						1875968, -- [17]
						47273472, -- [18]
						21825536, -- [19]
						8249088, -- [20]
						19628032, -- [21]
						46959360, -- [22]
						13696768, -- [23]
					},
					["General"] = {
						1690368, -- [1]
						15296512, -- [2]
						21493248, -- [3]
						32112384, -- [4]
						8547328, -- [5]
						19525376, -- [6]
						5273344, -- [7]
						21491456, -- [8]
						21489664, -- [9]
						8548096, -- [10]
						20412928, -- [11]
						20130048, -- [12]
						5273088, -- [13]
						21491200, -- [14]
						19531264, -- [15]
						8727100, -- [16]
						8727100, -- [17]
						23108412, -- [18]
						13874500, -- [19]
						8727366, -- [20]
						23107920, -- [21]
						29673813, -- [22]
					},
				},
				["SpellTabs"] = {
					"General", -- [1]
					"Protection", -- [2]
					"Holy", -- [3]
					"Retribution", -- [4]
				},
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
				["Spells"] = {
					["Havoc"] = {
						48255744, -- [1]
						50838784, -- [2]
						45838592, -- [3]
						41675264, -- [4]
						47040512, -- [5]
						50359808, -- [6]
						41534208, -- [7]
						50691328, -- [8]
						49938432, -- [9]
						33624832, -- [10]
						55764992, -- [11]
						49005312, -- [12]
						48256256, -- [13]
						47391488, -- [14]
						50891008, -- [15]
						50190080, -- [16]
						47401984, -- [17]
						45808640, -- [18]
					},
					["General"] = {
						1690368, -- [1]
						21493248, -- [2]
						32112384, -- [3]
						15099904, -- [4]
						51912192, -- [5]
						8727296, -- [6]
						13874432, -- [7]
						23108352, -- [8]
						21491456, -- [9]
						21489664, -- [10]
						19520512, -- [11]
						20130048, -- [12]
						5269248, -- [13]
						5268992, -- [14]
						21491200, -- [15]
						39615488, -- [16]
						51912448, -- [17]
						5269760, -- [18]
						23107920, -- [19]
						29673813, -- [20]
					},
					["Vengeance"] = {
						47040512, -- [1]
						52152320, -- [2]
						55873536, -- [3]
						52229376, -- [4]
						33624832, -- [5]
						45757440, -- [6]
						55764992, -- [7]
						48412160, -- [8]
						48083712, -- [9]
						52168192, -- [10]
						52376576, -- [11]
						53167104, -- [12]
						51747072, -- [13]
						58490112, -- [14]
						48256256, -- [15]
						52264192, -- [16]
						47422720, -- [17]
						52099328, -- [18]
						50190080, -- [19]
						52159232, -- [20]
						52289024, -- [21]
					},
				},
				["SpellTabs"] = {
					"General", -- [1]
					"Havoc", -- [2]
					"Vengeance", -- [3]
				},
			},
			["Default.Sargeras.Falriçk"] = {
				["lastUpdate"] = 1471240396,
				["Spells"] = {
					["Marksmanship"] = {
						4975104, -- [1]
						47681792, -- [2]
						47683840, -- [3]
						30812160, -- [4]
						47715072, -- [5]
						226048, -- [6]
						21309952, -- [7]
						21310208, -- [8]
						21310464, -- [9]
						21310720, -- [10]
						44975616, -- [11]
						1309696, -- [12]
						37724672, -- [13]
						199936, -- [14]
						1586432, -- [15]
						49738496, -- [16]
						1378304, -- [17]
						395008, -- [18]
						47590656, -- [19]
						8826112, -- [20]
						676608, -- [21]
						374272, -- [22]
						676096, -- [23]
						1789696, -- [24]
						251392, -- [25]
						387840, -- [26]
						47451648, -- [27]
						49542656, -- [28]
						8988160, -- [29]
						47612672, -- [30]
						54440448, -- [31]
						49527808, -- [32]
					},
					["Beast Mastery"] = {
						47681792, -- [1]
						47683840, -- [2]
						49543680, -- [3]
						5010944, -- [4]
						226048, -- [5]
						21309952, -- [6]
						21310208, -- [7]
						21310464, -- [8]
						21310720, -- [9]
						49524480, -- [10]
						1309696, -- [11]
						37724672, -- [12]
						30893824, -- [13]
						199936, -- [14]
						1586432, -- [15]
						27981824, -- [16]
						1378304, -- [17]
						395008, -- [18]
						8710656, -- [19]
						8826112, -- [20]
						676608, -- [21]
						374272, -- [22]
						676096, -- [23]
						1789696, -- [24]
						251392, -- [25]
						387840, -- [26]
						29680384, -- [27]
						19624192, -- [28]
						47561984, -- [29]
					},
					["General"] = {
						1690368, -- [1]
						19200, -- [2]
						21493248, -- [3]
						32112384, -- [4]
						15099904, -- [5]
						19520000, -- [6]
						13874432, -- [7]
						23108352, -- [8]
						21491456, -- [9]
						21489664, -- [10]
						19520512, -- [11]
						23107840, -- [12]
						20130048, -- [13]
						5269248, -- [14]
						5268992, -- [15]
						21491200, -- [16]
						39615488, -- [17]
						19519744, -- [18]
						29673728, -- [19]
						5269760, -- [20]
					},
					["Survival"] = {
						47681792, -- [1]
						47689984, -- [2]
						47683840, -- [3]
						226048, -- [4]
						21309952, -- [5]
						21310208, -- [6]
						21310464, -- [7]
						21310720, -- [8]
						48053248, -- [9]
						1586432, -- [10]
						27981824, -- [11]
						1378304, -- [12]
						51916800, -- [13]
						395008, -- [14]
						48876800, -- [15]
						49475840, -- [16]
						47578880, -- [17]
						48877568, -- [18]
						48052992, -- [19]
						374272, -- [20]
						676096, -- [21]
						1789696, -- [22]
						251392, -- [23]
						387840, -- [24]
						47685120, -- [25]
						50085120, -- [26]
						48981504, -- [27]
						42203136, -- [28]
					},
				},
				["SpellTabs"] = {
					"General", -- [1]
					"Marksmanship", -- [2]
					"Beast Mastery", -- [3]
					"Survival", -- [4]
				},
			},
		},
	},
}
