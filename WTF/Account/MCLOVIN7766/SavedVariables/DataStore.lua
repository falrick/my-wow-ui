
DataStoreDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Guilds"] = {
			["Default.Sargeras.One Hundred Proof"] = {
				["faction"] = "Alliance",
			},
			["Default.Sargeras.TheLegion"] = {
				["faction"] = "Alliance",
			},
			["Default.Sargeras.Tainted Crusaders"] = {
				["faction"] = "Alliance",
			},
		},
		["Version"] = 1,
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["guildName"] = "Tainted Crusaders",
				["faction"] = "Alliance",
			},
			["Default.Sargeras.Falridan"] = {
				["guildName"] = "One Hundred Proof",
				["faction"] = "Alliance",
			},
			["Default.Sargeras.Falriçk"] = {
				["guildName"] = "One Hundred Proof",
				["faction"] = "Alliance",
			},
		},
		["ShortToLongRealmNames"] = {
			["Sargeras"] = "Sargeras",
		},
	},
}
