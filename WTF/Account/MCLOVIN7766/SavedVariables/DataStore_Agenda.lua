
DataStore_AgendaDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Options"] = {
			["WeeklyResetHour"] = 6,
			["WeeklyResetDay"] = 2,
			["NextWeeklyReset"] = "2016-08-16",
		},
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471225090,
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
			},
			["Default.Sargeras.Falriçk"] = {
				["DungeonIDs"] = {
					["Hellfire Citadel Heroic|309789365"] = "119204|1471240397|0|1",
					["Blackwing Descent 25 Player|222419877"] = "119204|1471240397|0|1",
					["Dragon Soul 25 Player (Heroic)|221846003"] = "119204|1471240397|0|1",
					["The Bastion of Twilight 25 Player (Heroic)|222253178"] = "119204|1471240397|0|1",
					["Icecrown Citadel 25 Player (Heroic)|222256186"] = "119204|1471240397|0|1",
					["Onyxia's Lair 25 Player|222417818"] = "119204|1471240397|0|1",
					["Ulduar 25 Player|222406116"] = "119204|1471240397|0|1",
					["Black Temple 25 Player|221854902"] = "119204|1471240397|0|1",
					["Firelands 25 Player (Heroic)|221763106"] = "119204|1471240397|0|1",
					["Trial of the Crusader 25 Player|222403717"] = "119204|1471240397|0|1",
					["Upper Blackrock Spire Heroic|3621202114"] = "32804|1471240397|0|0",
				},
				["lastUpdate"] = 1471240396,
				["LFGDungeons"] = {
					["846.Count"] = 3,
					["985.Count"] = 3,
					["985.Fel Lord Zakuun"] = true,
					["846.Flamebender Ka'graz"] = true,
					["846.Hans'gar & Franzok"] = true,
					["986.Archimonde"] = true,
					["846.Kromog, Legend of the Mountain"] = true,
					["986.Count"] = 1,
					["985.Xhul'horac"] = true,
					["985.Mannoroth"] = true,
				},
			},
		},
	},
}
