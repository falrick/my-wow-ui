
AUCTIONATOR_SAVEDVARS = {
	["_50000"] = 500,
	["_2000"] = 100,
	["_10000"] = 200,
	["_5000000"] = 10000,
	["_1000000"] = 2500,
	["_200000"] = 1000,
	["LOG_DE_DATA_X"] = true,
	["STARTING_DISCOUNT"] = 5,
	["_500"] = 5,
}
AUCTIONATOR_PRICING_HISTORY = {
}
AUCTIONATOR_SHOPPING_LISTS = {
	{
		["items"] = {
			"Bag", -- [1]
		},
		["isRecents"] = 1,
		["name"] = "Recent Searches",
	}, -- [1]
	{
		["items"] = {
			"Greater Cosmic Essence", -- [1]
			"Infinite Dust", -- [2]
			"Dream Shard", -- [3]
			"Abyss Crystal", -- [4]
		},
		["name"] = "Sample Shopping List #1",
		["isSorted"] = false,
	}, -- [2]
}
AUCTIONATOR_SHOPPING_LISTS_MIGRATED_V2 = true
AUCTIONATOR_PRICE_DATABASE = {
	["__dbversion"] = 4,
	["Sargeras_Alliance"] = {
		["Pattern: Runecloth Bag"] = {
			["mr"] = 8999998,
			["cc"] = 9,
			["id"] = "14468:0:0:0",
			["sc"] = 2,
			["H2099"] = 8999998,
		},
		["Netherweave Bag"] = {
			["mr"] = 299999,
			["cc"] = 1,
			["id"] = "21841:0:0:0",
			["sc"] = 0,
			["H2099"] = 299999,
		},
		["Red Woolen Bag"] = {
			["mr"] = 43080,
			["cc"] = 1,
			["id"] = "5763:0:0:0",
			["sc"] = 0,
			["H2099"] = 43080,
		},
		["Red Linen Bag"] = {
			["mr"] = 1429960,
			["cc"] = 1,
			["id"] = "5762:0:0:0",
			["sc"] = 0,
			["H2099"] = 1429960,
		},
		["Green Leather Bag"] = {
			["mr"] = 45676,
			["cc"] = 1,
			["id"] = "5573:0:0:0",
			["sc"] = 0,
			["H2099"] = 45676,
		},
		["Hyjal Expedition Bag"] = {
			["mr"] = 4987498,
			["cc"] = 1,
			["id"] = "54446:0:0:0",
			["sc"] = 2,
			["H2099"] = 4987498,
		},
		["Luxurious Silk Gem Bag"] = {
			["mr"] = 11990000,
			["cc"] = 1,
			["id"] = "70138:0:0:0",
			["sc"] = 5,
			["H2099"] = 11990000,
		},
		["Linen Bag"] = {
			["mr"] = 1000,
			["cc"] = 1,
			["id"] = "4238:0:0:0",
			["sc"] = 0,
			["H2099"] = 1000,
		},
		["Big Bag of Enchantment"] = {
			["mr"] = 5170723,
			["cc"] = 1,
			["id"] = "22249:0:0:0",
			["sc"] = 3,
			["H2099"] = 5170723,
		},
		["Pattern: Big Bag of Enchantment"] = {
			["mr"] = 80000,
			["cc"] = 9,
			["id"] = "22309:0:0:0",
			["sc"] = 2,
			["H2099"] = 80000,
		},
		["Pattern: Mooncloth Bag"] = {
			["mr"] = 247000,
			["cc"] = 9,
			["id"] = "14499:0:0:0",
			["sc"] = 2,
			["H2099"] = 247000,
		},
		["Embersilk Bag"] = {
			["mr"] = 2509994,
			["cc"] = 1,
			["id"] = "54443:0:0:0",
			["sc"] = 0,
			["H2099"] = 2509994,
		},
		["Pattern: Core Felcloth Bag"] = {
			["mr"] = 189500,
			["cc"] = 9,
			["id"] = "21371:0:0:0",
			["sc"] = 2,
			["H2099"] = 189500,
		},
		["Green Cabbage"] = {
			["mr"] = 85999,
			["cc"] = 7,
			["id"] = "74840:0:0:0",
			["sc"] = 8,
			["H2099"] = 85999,
		},
		["Mammoth Mining Bag"] = {
			["mr"] = 33039999,
			["cc"] = 1,
			["id"] = "38347:0:0:0",
			["sc"] = 6,
			["H2099"] = 33039999,
		},
		["Burnished Inscription Bag"] = {
			["mr"] = 990000,
			["cc"] = 1,
			["id"] = "116261:0:0:0",
			["sc"] = 8,
			["H2099"] = 990000,
		},
		["Red Leather Bag"] = {
			["mr"] = 117500,
			["cc"] = 1,
			["id"] = "2657:0:0:0",
			["sc"] = 0,
			["H2099"] = 117500,
		},
		["Red Mageweave Bag"] = {
			["mr"] = 80004,
			["cc"] = 1,
			["id"] = "10051:0:0:0",
			["sc"] = 0,
			["H2099"] = 80004,
		},
		["Glacial Bag"] = {
			["mr"] = 9070284,
			["cc"] = 1,
			["id"] = "41600:0:0:0",
			["sc"] = 0,
			["H2099"] = 9070284,
		},
		["Murloc Skin Bag"] = {
			["mr"] = 199999,
			["cc"] = 1,
			["id"] = "1470:0:0:0",
			["sc"] = 0,
			["H2099"] = 199999,
		},
		["Frostweave Bag"] = {
			["mr"] = 1299998,
			["cc"] = 1,
			["id"] = "41599:0:0:0",
			["sc"] = 0,
			["H2099"] = 1299998,
		},
		["Woolen Bag"] = {
			["mr"] = 27500,
			["cc"] = 1,
			["id"] = "4240:0:0:0",
			["sc"] = 0,
			["H2099"] = 27500,
		},
		["Core Felcloth Bag"] = {
			["mr"] = 8648960,
			["cc"] = 1,
			["id"] = "21342:0:0:0",
			["sc"] = 0,
			["H2099"] = 8648960,
		},
		["Burnished Mining Bag"] = {
			["mr"] = 9969996,
			["cc"] = 1,
			["id"] = "116260:0:0:0",
			["sc"] = 6,
			["H2099"] = 9969996,
		},
		["Pattern: Red Linen Bag"] = {
			["mr"] = 1997499,
			["cc"] = 9,
			["id"] = "5771:0:0:0",
			["sc"] = 2,
			["H2099"] = 1997499,
		},
		["Otherworldly Bag"] = {
			["mr"] = 3834997,
			["cc"] = 1,
			["id"] = "54445:0:0:0",
			["sc"] = 3,
			["H2099"] = 3834997,
		},
		["Pattern: Enchanted Runecloth Bag"] = {
			["mr"] = 6032283,
			["cc"] = 9,
			["id"] = "22308:0:0:0",
			["sc"] = 2,
			["H2099"] = 6032283,
		},
		["Enchanted Runecloth Bag"] = {
			["mr"] = 1251634,
			["cc"] = 1,
			["id"] = "22248:0:0:0",
			["sc"] = 3,
			["H2099"] = 1251634,
		},
		["Mageweave Bag"] = {
			["mr"] = 59999,
			["cc"] = 1,
			["id"] = "10050:0:0:0",
			["sc"] = 0,
			["H2099"] = 59999,
		},
		["Six Demon Bag"] = {
			["mr"] = 219000,
			["cc"] = 4,
			["id"] = "7734:0:0:0",
			["sc"] = 0,
			["H2099"] = 219000,
		},
		["Hexweave Bag"] = {
			["mr"] = 9970000,
			["cc"] = 1,
			["id"] = "114821:0:0:0",
			["sc"] = 0,
			["H2099"] = 9970000,
		},
		["Pattern: Imbued Netherweave Bag"] = {
			["mr"] = 1452500,
			["cc"] = 9,
			["id"] = "21893:0:0:0",
			["sc"] = 2,
			["H2099"] = 1452500,
		},
		["Pattern: Red Woolen Bag"] = {
			["mr"] = 148999,
			["cc"] = 9,
			["id"] = "5772:0:0:0",
			["sc"] = 2,
			["H2099"] = 148999,
		},
		["Runecloth Bag"] = {
			["mr"] = 80000,
			["cc"] = 1,
			["id"] = "14046:0:0:0",
			["sc"] = 0,
			["H2099"] = 80000,
		},
		["Bag of Jewels"] = {
			["mr"] = 3467829,
			["cc"] = 1,
			["id"] = "24270:0:0:0",
			["sc"] = 5,
			["H2099"] = 3467829,
		},
		["Blue Leather Bag"] = {
			["mr"] = 44785,
			["cc"] = 1,
			["id"] = "856:0:0:0",
			["sc"] = 0,
			["H2099"] = 44785,
		},
		["Imbued Netherweave Bag"] = {
			["mr"] = 1300000,
			["cc"] = 1,
			["id"] = "21843:0:0:0",
			["sc"] = 0,
			["H2099"] = 1300000,
		},
		["Illusionary Bag"] = {
			["mr"] = 9999986,
			["cc"] = 1,
			["id"] = "54444:0:0:0",
			["sc"] = 0,
			["H2099"] = 9999986,
		},
		["Pattern: Green Woolen Bag"] = {
			["mr"] = 21999,
			["cc"] = 9,
			["id"] = "4292:0:0:0",
			["sc"] = 2,
			["H2099"] = 21999,
		},
		["Burnished Leather Bag"] = {
			["mr"] = 4945000,
			["cc"] = 1,
			["id"] = "116259:0:0:0",
			["sc"] = 7,
			["H2099"] = 4945000,
		},
		["Pattern: Bottomless Bag"] = {
			["mr"] = 44180000,
			["cc"] = 9,
			["id"] = "14510:0:0:0",
			["sc"] = 2,
			["H2099"] = 44180000,
		},
		["Green Woolen Bag"] = {
			["mr"] = 134914,
			["cc"] = 1,
			["id"] = "4241:0:0:0",
			["sc"] = 0,
			["H2099"] = 134914,
		},
	},
}
AUCTIONATOR_LAST_SCAN_TIME = nil
AUCTIONATOR_TOONS = {
	["Varyen"] = {
		["firstSeen"] = 1470974660,
		["firstVersion"] = "4.0.9",
	},
	["Falriçk"] = {
		["firstSeen"] = 1470692236,
		["firstVersion"] = "4.0.9",
	},
	["Falridan"] = {
		["firstSeen"] = 1470800760,
		["guid"] = "Player-76-09050976",
		["firstVersion"] = "4.0.9",
	},
}
AUCTIONATOR_STACKING_PREFS = {
}
AUCTIONATOR_SCAN_MINLEVEL = 1
AUCTIONATOR_DB_MAXITEM_AGE = 180
AUCTIONATOR_DB_MAXHIST_AGE = -1
AUCTIONATOR_DB_MAXHIST_DAYS = 5
AUCTIONATOR_FS_CHUNK = nil
AUCTIONATOR_DE_DATA = nil
AUCTIONATOR_DE_DATA_BAK = nil
ITEM_ID_VERSION = "3.2.6"
AUCTIONATOR_SHOW_MAILBOX_TIPS = nil
