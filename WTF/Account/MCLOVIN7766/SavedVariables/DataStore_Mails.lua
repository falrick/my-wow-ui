
DataStore_MailsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471215431,
				["lastVisitDate"] = "2016/08/14 17:57",
			},
			["Default.Sargeras.Falriçk"] = {
				["lastUpdate"] = 1471215101,
				["Mails"] = {
					{
						["icon"] = 134327,
						["sender"] = "Archmage Khadgar",
						["subject"] = "Seaman",
						["returned"] = false,
						["money"] = 0,
						["text"] = "Greetings Commander!\r\n\r\nIt looks like you are going to have a bright future as naval strategist!\r\n\r\nI figured this trinket might be of use on your high seas adventures.  Use it anytime you want to quickly return to your shipyard.\r\n\r\n--Khadgar",
						["lastCheck"] = 1471215101,
						["daysLeft"] = 26.0727310180664,
					}, -- [1]
				},
				["lastVisitDate"] = "2016/08/14 17:51",
			},
		},
	},
}
