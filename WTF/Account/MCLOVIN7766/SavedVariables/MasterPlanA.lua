
MasterPlanAG = {
	["Sargeras"] = {
		["Varyen"] = {
			["faction"] = "Alliance",
			["class"] = "PALADIN",
		},
		["Falriçk"] = {
			["faction"] = "Alliance",
			["recruitTime"] = 1470841437,
			["summary"] = {
				["inProgress"] = {
					[147] = 1471241709,
					[223] = 1471240826,
					[534] = 1471245415,
					[224] = 1471240818,
					[157] = 1471239930,
					[150] = 1471241707,
					[188] = 1471239451,
					[151] = 1471239915,
					[628] = 1471252593,
					[664] = 1471252504,
					[594] = 1471252587,
					[368] = 1471274121,
				},
				["ti3"] = 128391,
				["tt3"] = 1470686498,
			},
			["curOil"] = 2655,
			["class"] = "HUNTER",
			["curRes"] = 5592,
			["lastCacheTime"] = 1471192850,
		},
		["Falridan"] = {
			["class"] = "DEMONHUNTER",
			["faction"] = "Alliance",
		},
	},
	["IgnoreRewards"] = {
	},
}
