
DataStore_StatsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471225204,
				["Stats"] = {
					["Ranged"] = "1-1|0|0|0|12.991341590881",
					["HealthMax"] = 7336,
					["Melee"] = "188-218|2.7050001621246|195|0|12.991341590881|3",
					["MaxPower"] = "0|1240",
					["Spell"] = "195|195|0|12.991341590881|28|198",
					["PVP"] = "2|0",
					["Base"] = "195|39|262|88|357",
					["Defense"] = "357|1|4.1903896331787|5.5997738838196|13|0",
				},
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
				["Stats"] = {
					["Ranged"] = "1-1|0|0|0|29.590908050537",
					["HealthMax"] = 303480,
					["Melee"] = "5072-5680|2.1070001125336|5875|0|29.590908050537|0",
					["MaxPower"] = "17|100",
					["Spell"] = "708|708|0|29.590908050537|2217|0",
					["PVP"] = "1|0",
					["Base"] = "1209|5875|5058|708|1126",
					["Defense"] = "1126|1|17.872690200806|3|0|0",
				},
			},
			["Default.Sargeras.Falriçk"] = {
				["lastUpdate"] = 1471240969,
				["Stats"] = {
					["Ranged"] = "7436-7437|2.444000005722|5014|0|27.527273178101",
					["HealthMax"] = 350820,
					["Melee"] = "5161-5162|2.444000005722|5014|0|27.527273178101|0",
					["MaxPower"] = "2|150",
					["Defense"] = "1496|1|14.81561088562|0|0|0",
					["PVP"] = "104|0",
					["Base"] = "889|5014|5847|851|1496",
					["Spell"] = "851|851|0|27.527273178101|2153|0",
				},
			},
		},
	},
}
