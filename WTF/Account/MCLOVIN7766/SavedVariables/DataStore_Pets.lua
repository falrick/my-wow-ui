
DataStore_PetsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Reference"] = {
			["Spells"] = {
				[176137] = 88222,
				[210699] = 106278,
				[139153] = 70154,
				[179835] = 90206,
				[62561] = 33226,
				[170292] = 85284,
				[188084] = 94867,
				[28487] = 16445,
				[177220] = 88814,
			},
		},
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471225090,
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
			},
			["Default.Sargeras.Falriçk"] = {
				["lastUpdate"] = 1471240396,
				["CRITTER"] = {
					139153, -- [1]
					176137, -- [2]
					210699, -- [3]
					188084, -- [4]
					177220, -- [5]
					179835, -- [6]
					170292, -- [7]
					62561, -- [8]
					28487, -- [9]
				},
			},
		},
	},
}
