
DataStore_TalentsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471225090,
				["Specializations"] = {
					[2] = 9,
				},
				["Class"] = "PALADIN",
			},
			["Default.Sargeras.Falriçk"] = {
				["lastUpdate"] = 1471240396,
				["Specializations"] = {
					7099, -- [1]
					7125, -- [2]
					10907, -- [3]
				},
				["Class"] = "HUNTER",
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
				["Specializations"] = {
					14, -- [1]
				},
				["Class"] = "DEMONHUNTER",
			},
		},
	},
}
DataStore_TalentsRefDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["HUNTER"] = {
			["Locale"] = "enUS",
			["Specializations"] = {
				{
					["id"] = 253,
					["talents"] = {
						22291, -- [1]
						22280, -- [2]
						22282, -- [3]
						21997, -- [4]
						22769, -- [5]
						22290, -- [6]
						19347, -- [7]
						19348, -- [8]
						22318, -- [9]
						22441, -- [10]
						22347, -- [11]
						22269, -- [12]
						22284, -- [13]
						22276, -- [14]
						22293, -- [15]
						19357, -- [16]
						22002, -- [17]
						22287, -- [18]
						22273, -- [19]
						21986, -- [20]
						22295, -- [21]
					},
					["icon"] = "INTERFACE\\ICONS\\ability_hunter_bestialdiscipline",
					["name"] = "Beast Mastery",
				}, -- [1]
				{
					["id"] = 254,
					["talents"] = {
						22279, -- [1]
						22501, -- [2]
						22289, -- [3]
						22495, -- [4]
						22497, -- [5]
						22498, -- [6]
						19347, -- [7]
						19348, -- [8]
						22318, -- [9]
						22267, -- [10]
						22286, -- [11]
						21998, -- [12]
						22284, -- [13]
						22276, -- [14]
						22499, -- [15]
						19357, -- [16]
						22002, -- [17]
						22287, -- [18]
						22274, -- [19]
						22308, -- [20]
						22288, -- [21]
					},
					["icon"] = "Interface\\Icons\\Ability_Hunter_FocusedAim",
					["name"] = "Marksmanship",
				}, -- [2]
				{
					["id"] = 255,
					["talents"] = {
						22275, -- [1]
						22283, -- [2]
						22296, -- [3]
						22500, -- [4]
						22266, -- [5]
						22297, -- [6]
						19347, -- [7]
						19348, -- [8]
						22318, -- [9]
						22277, -- [10]
						19361, -- [11]
						22299, -- [12]
						22268, -- [13]
						22496, -- [14]
						22298, -- [15]
						22300, -- [16]
						22278, -- [17]
						22271, -- [18]
						22272, -- [19]
						22301, -- [20]
						22295, -- [21]
					},
					["icon"] = "INTERFACE\\ICONS\\ability_hunter_camouflage",
					["name"] = "Survival",
				}, -- [3]
			},
			["Version"] = 22423,
		},
		["DEMONHUNTER"] = {
			["Locale"] = "enUS",
			["Specializations"] = {
				{
					["id"] = 577,
					["talents"] = {
						21854, -- [1]
						22493, -- [2]
						22416, -- [3]
						21857, -- [4]
						22765, -- [5]
						22799, -- [6]
					},
					["icon"] = "Interface\\Icons\\ability_demonhunter_specdps",
					["name"] = "Havoc",
				}, -- [1]
			},
			["Version"] = 22396,
		},
		["PALADIN"] = {
			["Locale"] = "enUS",
			["Specializations"] = {
				[2] = {
					["id"] = 66,
					["talents"] = {
						22428, -- [1]
						22558, -- [2]
						22430, -- [3]
						22431, -- [4]
						22604, -- [5]
						22594, -- [6]
					},
					["icon"] = "Interface\\Icons\\Ability_Paladin_ShieldoftheTemplar",
					["name"] = "Protection",
				},
			},
			["Version"] = 22423,
		},
	},
}
