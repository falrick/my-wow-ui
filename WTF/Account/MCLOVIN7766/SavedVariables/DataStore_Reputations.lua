
DataStore_ReputationsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471225090,
				["guildName"] = "Tainted Crusaders",
				["guildRep"] = 3025,
				["Factions"] = {
					4917, -- [1]
					4817, -- [2]
					4917, -- [3]
					4917, -- [4]
					13828, -- [5]
					[112] = 4917,
					[25] = 2557,
					[75] = 5117,
					[77] = 1817,
				},
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
				["guildName"] = "One Hundred Proof",
				["guildRep"] = 3375,
				["Factions"] = {
					4000, -- [1]
					3000, -- [2]
					3100, -- [3]
					3100, -- [4]
					3100, -- [5]
					[75] = 3300,
					[112] = 3100,
				},
			},
			["Default.Sargeras.Falriçk"] = {
				["lastUpdate"] = 1471240396,
				["guildName"] = "One Hundred Proof",
				["guildRep"] = 250,
				["Factions"] = {
					4000, -- [1]
					3000, -- [2]
					3100, -- [3]
					3100, -- [4]
					3100, -- [5]
					[87] = 8675,
					[62] = 12474,
					[132] = 9000,
					[37] = 6666,
					[38] = 3875,
					[39] = 2584,
					[133] = 11500,
					[123] = 17602,
					[125] = 312,
					[130] = 18700,
					[52] = 2519,
					[53] = 1734,
					[75] = 3300,
					[19] = 500,
					[24] = 2000,
					[112] = 3100,
					[122] = 111,
				},
			},
		},
	},
}
