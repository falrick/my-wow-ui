
DataStore_CraftsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["Professions"] = {
					["Cooking"] = {
						["FullLink"] = "|cffffd000|Htrade:Player-76-09077E34:2550:185|h[Cooking]|h|r",
						["MaxRank"] = 75,
						["Rank"] = 1,
					},
					["Blacksmithing"] = {
						["FullLink"] = "|cffffd000|Htrade:Player-76-09077E34:2018:164|h[Blacksmithing]|h|r",
						["Crafts"] = {
							"1|Materials", -- [1]
							13283, -- [2]
							"1|Weapon Mods", -- [3]
							10641, -- [4]
							12461, -- [5]
							"1|Chest", -- [6]
							49041, -- [7]
							"1|Gauntlets", -- [8]
							13295, -- [9]
							"1|Bracers", -- [10]
							10653, -- [11]
							"1|Belts", -- [12]
							10647, -- [13]
							"1|Legs", -- [14]
							10651, -- [15]
							"1|Boots", -- [16]
							13279, -- [17]
							"1|Weapons", -- [18]
							13175, -- [19]
							39935, -- [20]
							35523, -- [21]
							10959, -- [22]
							10955, -- [23]
							10951, -- [24]
						},
						["MaxRank"] = 75,
						["Rank"] = 43,
					},
					["Mining"] = {
						["Crafts"] = {
							"0|Smelting", -- [1]
							10629, -- [2]
						},
						["MaxRank"] = 75,
						["Rank"] = 47,
					},
					["First Aid"] = {
						["FullLink"] = "|cffffd000|Htrade:Player-76-09077E34:3273:129|h[First Aid]|h|r",
						["MaxRank"] = 75,
						["Rank"] = 1,
					},
				},
				["Prof2"] = "Mining",
				["lastUpdate"] = 1471225165,
				["Prof1"] = "Blacksmithing",
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
			},
			["Default.Sargeras.Falriçk"] = {
				["Professions"] = {
					["Tailoring"] = {
						["FullLink"] = "|cffffd000|Htrade:Player-76-08C3D1D4:158758:197|h[Tailoring]|h|r",
						["Crafts"] = {
							"1|Reagents and Research", -- [1]
							675343, -- [2]
							704235, -- [3]
							"1|Armor", -- [4]
							675411, -- [5]
							675415, -- [6]
							675419, -- [7]
							"1|Materials", -- [8]
							11855, -- [9]
							"1|Pants", -- [10]
							48179, -- [11]
							"1|Cloaks", -- [12]
							9551, -- [13]
							"1|Shirts", -- [14]
							15663, -- [15]
							9575, -- [16]
						},
						["MaxRank"] = 700,
						["Rank"] = 22,
					},
					["Fishing"] = {
						["MaxRank"] = 700,
						["Rank"] = 60,
					},
					["First Aid"] = {
						["FullLink"] = "|cffffd000|Htrade:Player-76-08C3D1D4:158741:129|h[First Aid]|h|r",
						["Crafts"] = {
							"0|Cures of Draenor", -- [1]
							690163, -- [2]
							690171, -- [3]
							690167, -- [4]
							690159, -- [5]
							"1|Bandages", -- [6]
							13103, -- [7]
						},
						["MaxRank"] = 700,
						["Rank"] = 1,
					},
					["Cooking"] = {
						["FullLink"] = "|cffffd000|Htrade:Player-76-08C3D1D4:158765:185|h[Cooking]|h|r",
						["Crafts"] = {
							"1|Meat Dishes", -- [1]
							643851, -- [2]
							"1|Fish Dishes", -- [3]
							643919, -- [4]
							644011, -- [5]
							644007, -- [6]
							"0|Old World Recipes", -- [7]
							10155, -- [8]
							34419, -- [9]
							10163, -- [10]
							151347, -- [11]
						},
						["MaxRank"] = 700,
						["Rank"] = 11,
					},
					["Archaeology"] = {
						["MaxRank"] = 75,
						["Rank"] = 1,
					},
					["Enchanting"] = {
						["FullLink"] = "|cffffd000|Htrade:Player-76-08C3D1D4:158716:333|h[Enchanting]|h|r",
						["Crafts"] = {
							"1|Reagents and Research", -- [1]
							676371, -- [2]
							708175, -- [3]
							676367, -- [4]
							"1|Weapon", -- [5]
							636947, -- [6]
							"1|Ring", -- [7]
							635631, -- [8]
							635635, -- [9]
							635639, -- [10]
							635647, -- [11]
							"1|Toys", -- [12]
							651793, -- [13]
							"1|Weapons", -- [14]
							30983, -- [15]
							31154, -- [16]
							"1|Bracers", -- [17]
							31118, -- [18]
							29829, -- [19]
							29712, -- [20]
							29672, -- [21]
							"1|Chest", -- [22]
							30994, -- [23]
							29705, -- [24]
							29680, -- [25]
							"1|Cloak", -- [26]
							31086, -- [27]
							"1|Rods", -- [28]
							29684, -- [29]
							"1|Wands", -- [30]
							59230, -- [31]
							57172, -- [32]
						},
						["MaxRank"] = 700,
						["Rank"] = 122,
					},
				},
				["Prof2"] = "Enchanting",
				["lastUpdate"] = 1471240396,
				["Prof1"] = "Tailoring",
			},
		},
	},
}
