
Postal3DB = {
	["global"] = {
		["BlackBook"] = {
			["alts"] = {
				"Falridan|Sargeras|Alliance|98|DEMONHUNTER", -- [1]
				"Falriçk|Sargeras|Alliance|100|HUNTER", -- [2]
				"Varyen|Sargeras|Alliance|36|PALADIN", -- [3]
			},
		},
	},
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["profiles"] = {
		["Varyen - Sargeras"] = {
		},
		["Falriçk - Sargeras"] = {
			["BlackBook"] = {
				["recent"] = {
					"Varyen|Sargeras|Alliance", -- [1]
				},
			},
		},
		["Falridan - Sargeras"] = {
		},
	},
}
