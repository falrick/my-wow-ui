
RecountDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["profiles"] = {
		["Varyen - Sargeras"] = {
			["MainWindow"] = {
				["Position"] = {
					["y"] = 405.999816894531,
					["h"] = 111.99991607666,
					["w"] = 225.999755859375,
					["x"] = -362.999969482422,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["MainWindowHeight"] = 111.999977111816,
			["CurDataSet"] = "OverallData",
			["MainWindowWidth"] = 225.999755859375,
			["LastInstanceName"] = "Dire Maul",
		},
		["Falriçk - Sargeras"] = {
			["MainWindow"] = {
				["Position"] = {
					["y"] = 408.999877929688,
					["x"] = -353.999420166016,
					["w"] = 255.999786376953,
					["h"] = 110.000053405762,
				},
			},
			["MainWindowHeight"] = 110.000053405762,
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["MainWindowVis"] = false,
			["LastInstanceName"] = "Blackwing Descent",
			["CurDataSet"] = "OverallData",
			["MainWindowWidth"] = 255.999801635742,
		},
		["Falridan - Sargeras"] = {
			["MainWindow"] = {
				["Position"] = {
					["y"] = 398,
					["h"] = 119.999908447266,
					["w"] = 264.000030517578,
					["x"] = -313.999908447266,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["MainWindowHeight"] = 119.999908447266,
			["MainWindowWidth"] = 264.000030517578,
			["CurDataSet"] = "OverallData",
		},
	},
}
