
DataStore_CurrenciesDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Reference"] = {
			["Currencies"] = {
				"Warlords of Draenor|", -- [1]
				"Apexis Crystal|Interface\\Icons\\inv_apexis_draenor", -- [2]
				"Garrison Resources|Interface\\Icons\\inv_garrison_resource", -- [3]
				"Oil|Interface\\Icons\\garrison_oil", -- [4]
				"Seal of Inevitable Fate|Interface\\Icons\\achievement_battleground_templeofkotmogu_02_green", -- [5]
				"Seal of Tempered Fate|Interface\\Icons\\ability_animusorbs", -- [6]
				"Dungeon and Raid|", -- [7]
				"Timewarped Badge|Interface\\Icons\\pvecurrency-justice", -- [8]
				"Valor|Interface\\Icons\\pvecurrency-valor", -- [9]
				"Mists of Pandaria|", -- [10]
				"Lesser Charm of Good Fortune|Interface\\Icons\\inv_misc_coin_18", -- [11]
				"Legion|", -- [12]
				"Nethershard|Interface\\Icons\\inv_datacrystal01", -- [13]
				"Cataclysm|", -- [14]
				"Mote of Darkness|Interface\\Icons\\spell_shadow_sealofkings", -- [15]
				"Miscellaneous|", -- [16]
				"Darkmoon Prize Ticket|Interface\\Icons\\inv_misc_ticket_darkmoon_01", -- [17]
			},
			["CurrencyTextRev"] = {
				["Nethershard"] = 13,
				["Valor"] = 9,
				["Dungeon and Raid"] = 7,
				["Seal of Inevitable Fate"] = 5,
				["Mists of Pandaria"] = 10,
				["Mote of Darkness"] = 15,
				["Garrison Resources"] = 3,
				["Warlords of Draenor"] = 1,
				["Miscellaneous"] = 16,
				["Timewarped Badge"] = 8,
				["Apexis Crystal"] = 2,
				["Darkmoon Prize Ticket"] = 17,
				["Cataclysm"] = 14,
				["Oil"] = 4,
				["Legion"] = 12,
				["Lesser Charm of Good Fortune"] = 11,
				["Seal of Tempered Fate"] = 6,
			},
		},
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471225090,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["Currencies"] = {
					25, -- [1]
					40090, -- [2]
					33, -- [3]
					1826, -- [4]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[390] = "0-0-0-0",
					[823] = "0-0-0-0",
					[392] = "0-0-0-0",
					[824] = "0-0-0-0",
					[994] = "0-0-0-0",
				},
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470810260,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["Currencies"] = {
					25, -- [1]
					8602, -- [2]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[390] = "0-0-0-0",
					[823] = "0-0-0-0",
					[392] = "0-0-0-0",
					[824] = "0-0-0-0",
					[994] = "0-0-0-0",
				},
			},
			["Default.Sargeras.Falriçk"] = {
				["Currencies"] = {
					25, -- [1]
					17178, -- [2]
					15, -- [3]
					51856, -- [4]
					32018, -- [5]
					3, -- [6]
					1942916, -- [7]
					715782, -- [8]
					339848, -- [9]
					10, -- [10]
					12, -- [11]
					21, -- [12]
					278, -- [13]
					29, -- [14]
					30, -- [15]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					23, -- [4]
					67, -- [5]
					4, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1471240396,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[390] = "0-0-0-0",
					[823] = "15179-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-20",
					[824] = "5592-0-0-10000",
				},
			},
		},
	},
}
