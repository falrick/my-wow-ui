
DataStore_GarrisonsDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["lastUpdate"] = 1471220422,
			},
			["Default.Sargeras.Falridan"] = {
				["lastUpdate"] = 1470800848,
			},
			["Default.Sargeras.Falriçk"] = {
				["numRareFollowers"] = 5,
				["avgArmoriLevel"] = 602.181818181818,
				["lastUpdate"] = 1471240827,
				["numFollowersAtiLevel630"] = 1,
				["ActiveMissionsInfo"] = {
					{
						["startTime"] = 1471238131,
					}, -- [1]
					{
						["startTime"] = 1471238216,
					}, -- [2]
				},
				["Buildings"] = {
					["TradingPost"] = {
						["id"] = 144,
						["rank"] = 2,
					},
					["LunarfallInn"] = {
						["id"] = 35,
						["rank"] = 2,
					},
					["EnchantersStudy"] = {
						["id"] = 125,
						["rank"] = 2,
					},
					["FishingShack"] = {
						["id"] = 64,
						["rank"] = 1,
					},
					["Menagerie"] = {
						["id"] = 42,
						["rank"] = 1,
					},
					["Storehouse"] = {
						["id"] = 142,
						["rank"] = 2,
					},
					["DwarvenBunker"] = {
						["id"] = 9,
						["rank"] = 2,
					},
					["LunarfallExcavation"] = {
						["id"] = 62,
						["rank"] = 2,
					},
					["HerbGarden"] = {
						["id"] = 137,
						["rank"] = 3,
					},
					["TownHall"] = {
						["id"] = 0,
					},
					["TailoringEmporium"] = {
						["id"] = 127,
						["rank"] = 2,
					},
					["Barracks"] = {
						["id"] = 28,
						["rank"] = 3,
					},
				},
				["AbilityCounters"] = {
					3, -- [1]
					2, -- [2]
					1, -- [3]
					3, -- [4]
					nil, -- [5]
					6, -- [6]
					2, -- [7]
					1, -- [8]
					3, -- [9]
					2, -- [10]
				},
				["Followers"] = {
					[209] = {
						["link"] = "|cff1eff00|Hgarrfollower:209:2:100:600:114:0:0:0:60:0:0:0:0|h[Abu'gar]|h|r",
						["xp"] = 7367,
						["levelXP"] = 60000,
					},
					[263] = {
						["link"] = "|cff1eff00|Hgarrfollower:263:2:100:600:134:0:0:0:79:0:0:0:0|h[Ilaniel Pine]|h|r",
						["xp"] = 39759,
						["levelXP"] = 60000,
					},
					[186] = {
						["link"] = "|cff1eff00|Hgarrfollower:186:2:99:600:125:0:0:0:4:0:0:0:0|h[Vindicator Onaala]|h|r",
						["xp"] = 5027,
						["levelXP"] = 6000,
					},
					[34] = {
						["link"] = "|cff1eff00|Hgarrfollower:34:2:100:600:108:0:0:0:58:0:0:0:0|h[Qiana Moonshadow]|h|r",
						["xp"] = 51212,
						["levelXP"] = 60000,
					},
					[183] = {
						["link"] = "|cffa335ee|Hgarrfollower:183:4:97:600:157:156:0:0:55:221:256:0:0|h[Rulkan]|h|r",
						["xp"] = 1531,
						["levelXP"] = 4000,
					},
					[289] = {
						["link"] = "|cff1eff00|Hgarrfollower:289:2:96:600:170:0:0:0:59:0:0:0:0|h[Esmund Brightshield]|h|r",
						["xp"] = 2900,
						["levelXP"] = 3500,
					},
					[452] = {
						["link"] = "|cff0070dd|Hgarrfollower:452:3:100:600:183:0:0:0:236:7:0:0:0|h[Sprynt Starkflange]|h|r",
						["xp"] = 17379,
						["levelXP"] = 120000,
					},
					[468] = {
						["link"] = "|cffa335ee|Hgarrfollower:468:4:100:607:174:179:0:0:303:80:36:0:0|h[Oronok Torn-heart]|h|r",
						["xp"] = 0,
						["levelXP"] = 0,
					},
					[180] = {
						["link"] = "|cff0070dd|Hgarrfollower:180:3:100:600:11:0:0:0:53:256:0:0:0|h[Fiona]|h|r",
						["xp"] = 75900,
						["levelXP"] = 120000,
					},
					[153] = {
						["link"] = "|cff1eff00|Hgarrfollower:153:2:93:600:161:0:0:0:54:0:0:0:0|h[Bruma Swiftstone]|h|r",
						["xp"] = 1100,
						["levelXP"] = 1600,
					},
					[327] = {
						["link"] = "|cff0070dd|Hgarrfollower:327:3:92:600:125:0:0:0:256:45:0:0:0|h[Haagios]|h|r",
						["xp"] = 600,
						["levelXP"] = 1200,
					},
					[445] = {
						["link"] = "|cff0070dd|Hgarrfollower:445:3:92:600:143:0:0:0:256:68:0:0:0|h[Master Tengbai]|h|r",
						["xp"] = 835,
						["levelXP"] = 1200,
					},
					[455] = {
						["link"] = "|cffa335ee|Hgarrfollower:455:4:100:633:172:170:0:0:69:314:44:0:0|h[Millhouse Manastorm]|h|r",
						["xp"] = 0,
						["levelXP"] = 0,
					},
					[248] = {
						["link"] = "|cff1eff00|Hgarrfollower:248:2:90:600:137:0:0:0:256:0:0:0:0|h[Ilspeth Hollander]|h|r",
						["xp"] = 100,
						["levelXP"] = 400,
					},
					[338] = {
						["link"] = "|cff1eff00|Hgarrfollower:338:2:100:600:131:0:0:0:42:0:0:0:0|h[Loranea]|h|r",
						["xp"] = 34329,
						["levelXP"] = 60000,
					},
					[426] = {
						["link"] = "|cff1eff00|Hgarrfollower:426:2:100:600:100:0:0:0:68:0:0:0:0|h[Fargo Flintlocke]|h|r",
						["xp"] = 6749,
						["levelXP"] = 60000,
					},
					[195] = {
						["link"] = "|cff1eff00|Hgarrfollower:195:2:98:600:160:0:0:0:55:0:0:0:0|h[Weldon Barov]|h|r",
						["xp"] = 4439,
						["levelXP"] = 5400,
					},
					[216] = {
						["link"] = "|cff0070dd|Hgarrfollower:216:3:96:600:115:0:0:0:231:41:0:0:0|h[Delvar Ironfist]|h|r",
						["xp"] = 672,
						["levelXP"] = 3500,
					},
					[463] = {
						["link"] = "|cff1eff00|Hgarrfollower:463:2:100:607:148:0:0:0:41:0:0:0:0|h[Daleera Moonfang]|h|r",
						["xp"] = 25513,
						["levelXP"] = 60000,
					},
					[182] = {
						["link"] = "|cff1eff00|Hgarrfollower:182:2:100:600:160:0:0:0:60:0:0:0:0|h[Shelly Hamby]|h|r",
						["xp"] = 20030,
						["levelXP"] = 60000,
					},
				},
				["AvailableMissions"] = {
					[380] = {
						["durationSeconds"] = 14400,
						["type"] = "Combat",
						["cost"] = 15,
						["level"] = 100,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[176] = {
						["durationSeconds"] = 2700,
						["type"] = "Combat",
						["cost"] = 0,
						["level"] = 93,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[169] = {
						["durationSeconds"] = 5400,
						["type"] = "Combat",
						["cost"] = 10,
						["level"] = 99,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[132] = {
						["durationSeconds"] = 21600,
						["type"] = "Combat",
						["cost"] = 0,
						["level"] = 100,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[170] = {
						["durationSeconds"] = 7200,
						["type"] = "Combat",
						["cost"] = 10,
						["level"] = 99,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[148] = {
						["durationSeconds"] = 3600,
						["type"] = "Combat",
						["cost"] = 10,
						["level"] = 92,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[266] = {
						["durationSeconds"] = 14400,
						["type"] = "Combat",
						["cost"] = 15,
						["level"] = 100,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[136] = {
						["durationSeconds"] = 1800,
						["type"] = "Combat",
						["cost"] = 10,
						["level"] = 90,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[159] = {
						["durationSeconds"] = 5400,
						["type"] = "Combat",
						["cost"] = 10,
						["level"] = 96,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[276] = {
						["durationSeconds"] = 36000,
						["type"] = "Patrol",
						["cost"] = 25,
						["level"] = 100,
						["typeAtlas"] = "GarrMission_MissionIcon-Patrol",
						["iLevel"] = 0,
					},
					[499] = {
						["durationSeconds"] = 36000,
						["type"] = "Combat",
						["cost"] = 25,
						["level"] = 100,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[131] = {
						["durationSeconds"] = 14400,
						["type"] = "Combat",
						["cost"] = 15,
						["level"] = 100,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
				},
				["Traits"] = {
					[58] = 1,
					[59] = 1,
					[60] = 2,
					[236] = 1,
					[221] = 1,
					[68] = 2,
					[36] = 1,
					[303] = 1,
					[41] = 2,
					[42] = 1,
					[44] = 1,
					[45] = 1,
					[80] = 1,
					[4] = 1,
					[314] = 1,
					[69] = 1,
					[231] = 1,
					[53] = 1,
					[54] = 1,
					[55] = 2,
					[79] = 1,
					[7] = 1,
					[256] = 5,
				},
				["lastResourceCollection"] = 1471192849,
				["numFollowers"] = 20,
				["Abilities"] = {
					[114] = 1,
					[170] = 2,
					[174] = 1,
					[148] = 1,
					[183] = 1,
					[156] = 1,
					[160] = 2,
					[115] = 1,
					[137] = 1,
					[172] = 1,
					[125] = 2,
					[157] = 1,
					[161] = 1,
					[100] = 1,
					[143] = 1,
					[179] = 1,
					[108] = 1,
					[131] = 1,
					[134] = 1,
					[11] = 1,
				},
				["ActiveMissions"] = {
					[368] = {
						["durationSeconds"] = 36000,
						["type"] = "Combat",
						["cost"] = 20,
						["followers"] = {
							426, -- [1]
							248, -- [2]
							445, -- [3]
						},
						["level"] = 90,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[223] = {
						["durationSeconds"] = 2700,
						["type"] = "Patrol",
						["cost"] = 10,
						["followers"] = {
							338, -- [1]
							289, -- [2]
						},
						["level"] = 92,
						["typeAtlas"] = "GarrMission_MissionIcon-Patrol",
						["iLevel"] = 0,
					},
					[224] = {
						["durationSeconds"] = 2700,
						["type"] = "Patrol",
						["cost"] = 10,
						["followers"] = {
							209, -- [1]
							186, -- [2]
						},
						["level"] = 93,
						["typeAtlas"] = "GarrMission_MissionIcon-Patrol",
						["iLevel"] = 0,
					},
					[157] = {
						["durationSeconds"] = 1800,
						["type"] = "Combat",
						["cost"] = 10,
						["followers"] = {
							263, -- [1]
						},
						["level"] = 95,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[150] = {
						["durationSeconds"] = 3600,
						["type"] = "Combat",
						["cost"] = 10,
						["followers"] = {
							153, -- [1]
						},
						["level"] = 93,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[188] = {
						["durationSeconds"] = 1350,
						["type"] = "Combat",
						["cost"] = 0,
						["followers"] = {
							183, -- [1]
						},
						["level"] = 93,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[151] = {
						["durationSeconds"] = 1800,
						["type"] = "Combat",
						["cost"] = 10,
						["followers"] = {
							34, -- [1]
						},
						["level"] = 93,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[664] = {
						["durationSeconds"] = 14400,
						["type"] = "Combat",
						["cost"] = 15,
						["followers"] = {
							463, -- [1]
							468, -- [2]
							455, -- [3]
						},
						["level"] = 100,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
					[147] = {
						["durationSeconds"] = 3600,
						["type"] = "Combat",
						["cost"] = 10,
						["followers"] = {
							452, -- [1]
						},
						["level"] = 92,
						["typeAtlas"] = "GarrMission_MissionIcon-Combat",
						["iLevel"] = 0,
					},
				},
				["numFollowersAtiLevel615"] = 1,
				["avgWeaponiLevel"] = 606.545454545455,
				["numEpicFollowers"] = 3,
				["numFollowersAtLevel100"] = 11,
			},
		},
		["Reference"] = {
			["FollowerNamesToID"] = {
				["Garona Halforcen"] = 466,
				["Harrison Jones"] = 465,
				["Phylarch the Evergreen"] = 194,
				["Cleric Maluuf"] = 459,
				["Rangari Kaalya"] = 159,
				["Hulda Shadowblade"] = 453,
				["Rulkan"] = 183,
				["Kimzee Pinchwhistle"] = 192,
				["Weldon Barov"] = 195,
				["Loranea"] = 338,
				["Master Tengbai"] = 445,
				["Lantresor of the Blade"] = 157,
				["Aknor Steelbringer"] = 225,
				["Goldmane the Skinner"] = 170,
				["Ilaniel Pine"] = 263,
				["Haagios"] = 327,
				["Dagg"] = 32,
				["Shelly Hamby"] = 182,
				["Pitfighter Vaandaam"] = 176,
				["Fargo Flintlocke"] = 426,
				["Leeroy Jenkins"] = 178,
				["Tormmok"] = 193,
				["Fen Tao"] = 467,
				["Bruma Swiftstone"] = 153,
				["Sprynt Starkflange"] = 452,
				["Ziri'ak"] = 168,
				["Pallas"] = 580,
				["Croman"] = 177,
				["Ilspeth Hollander"] = 248,
				["Professor Felblast"] = 460,
				["Abu'gar"] = 209,
				["Oronok Torn-heart"] = 468,
				["Miall"] = 155,
				["Meatball"] = 203,
				["Soulbinder Tuulani"] = 205,
				["Rangari Erdanii"] = 212,
				["Dowser Bigspark"] = 581,
				["Vindicator Onaala"] = 186,
				["Esmund Brightshield"] = 289,
				["Defender Illona"] = 207,
				["Leorajh"] = 219,
				["Daleera Moonfang"] = 463,
				["Pleasure-Bot 8000"] = 171,
				["Dawnseeker Rukaryx"] = 462,
				["Rangari Chel"] = 185,
				["Fiona"] = 180,
				["Thisalee Crow"] = 217,
				["Talon Guard Kurekk"] = 224,
				["Qiana Moonshadow"] = 34,
				["Blook"] = 189,
				["Artificer Romuul"] = 179,
				["Talonpriest Ishaal"] = 218,
				["Vindicator Heluun"] = 458,
				["Admiral Taylor"] = 204,
				["Nat Pagle"] = 202,
				["Apprentice Artificer Andren"] = 184,
				["Magister Serena"] = 154,
				["Glirin"] = 211,
				["Image of Archmage Vargoth"] = 190,
				["Ahm"] = 208,
				["Solar Priest Vayx"] = 582,
				["Millhouse Manastorm"] = 455,
				["Ariok"] = 474,
				["Delvar Ironfist"] = 216,
			},
		},
	},
}
