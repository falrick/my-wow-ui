
DataStore_InventoryDB = {
	["profileKeys"] = {
		["Varyen - Sargeras"] = "Varyen - Sargeras",
		["Falriçk - Sargeras"] = "Falriçk - Sargeras",
		["Falridan - Sargeras"] = "Falridan - Sargeras",
	},
	["global"] = {
		["Characters"] = {
			["Default.Sargeras.Varyen"] = {
				["Inventory"] = {
					"|cff00ccff|Hitem:122245::::::::41:66::::::|h[Polished Helm of Valor]|h|r", -- [1]
					"|cff0070dd|Hitem:51996::::::-71:1174863882:41:66::1::::|h[Tumultuous Necklace of the Bandit]|h|r", -- [2]
					"|cff00ccff|Hitem:122388::::::::41:66::::::|h[Burnished Pauldrons of Might]|h|r", -- [3]
					"|cff1eff00|Hitem:2575::::::::41:66::11::::|h[Red Linen Shirt]|h|r", -- [4]
					"|cff00ccff|Hitem:122381::::::::41:66::::::|h[Polished Breastplate of Valor]|h|r", -- [5]
					"|cff1eff00|Hitem:58911::::::::41:66::11::::|h[Tightly Cinched Belt]|h|r", -- [6]
					"|cff00ccff|Hitem:122251::::::::41:66::::::|h[Polished Legplates of Valor]|h|r", -- [7]
					"|cff0070dd|Hitem:65991::::::::41:66::11::::|h[Boots of the Noble Path]|h|r", -- [8]
					"|cff1eff00|Hitem:14759::::::::41:66::1::::|h[Enduring Bracers]|h|r", -- [9]
					"|cff1eff00|Hitem:58917::::::::41:66::11::::|h[Saldean's Working Gloves]|h|r", -- [10]
					nil, -- [11]
					nil, -- [12]
					"|cff0070dd|Hitem:17744::::::::41:66::1::::|h[Heart of Noxxion]|h|r", -- [13]
					"|cff0070dd|Hitem:18370::::::::41:66::1::::|h[Vigilance Charm]|h|r", -- [14]
					"|cff00ccff|Hitem:122260::::::::41:66::::::|h[Worn Stoneskin Gargoyle Cape]|h|r", -- [15]
					"|cff00ccff|Hitem:122389::::::::41:66::::::|h[Bloodsoaked Skullforge Reaver]|h|r", -- [16]
					"|cff00ccff|Hitem:122391::::::::41:66::::::|h[Flamescarred Draconian Deflector]|h|r", -- [17]
				},
				["averageItemLvl"] = 33.5,
				["overallAIL"] = 34.125,
				["lastUpdate"] = 1471225090,
			},
			["Default.Sargeras.Falridan"] = {
				["Inventory"] = {
					"|cff0070dd|Hitem:138970::::::::100:577::11::::|h[Helm of Reaffirmed Purpose]|h|r", -- [1]
					"|cff0070dd|Hitem:128945::::::::100:577::::::|h[Inquisitor's Glowering Eye]|h|r", -- [2]
					"|cff0070dd|Hitem:138168::::::::100:577:512:9:1:3387:100:::|h[Felshroud Shoulders]|h|r", -- [3]
					nil, -- [4]
					"|cffa335ee|Hitem:128952::::::::100:577::11:1:665:::|h[Torment Ender's Chestguard]|h|r", -- [5]
					"|cff0070dd|Hitem:138169::::::::100:577:512:9:1:3387:100:::|h[Felshroud Belt]|h|r", -- [6]
					"|cffa335ee|Hitem:128951::::::::100:577::11:1:665:::|h[Leggings of Sacrifice]|h|r", -- [7]
					"|cffa335ee|Hitem:128953::::::::100:577::11:1:665:::|h[Treads of Illidari Supremacy]|h|r", -- [8]
					"|cff0070dd|Hitem:128947::::::::100:577::::::|h[Pit Lord's Cuffs]|h|r", -- [9]
					"|cff0070dd|Hitem:128954::::::::100:577::11::::|h[Power Handler's Gloves]|h|r", -- [10]
					"|cff0070dd|Hitem:128948::::::::100:577::::::|h[Nefarious Ring]|h|r", -- [11]
					"|cff1eff00|Hitem:112460::::::::100:577::::::|h[Illidari Band]|h|r", -- [12]
					"|cffa335ee|Hitem:128959::::::::100:577::11:1:665:::|h[Seal of House Wrynn]|h|r", -- [13]
					"|cff0070dd|Hitem:133580::::::::100:577::::::|h[Brutarg's Sword Tip]|h|r", -- [14]
					"|cff0070dd|Hitem:128944::::::::100:577::::::|h[Voras' Silk Drape]|h|r", -- [15]
					"|cff0070dd|Hitem:128956::::::::100:577::::::|h[Fel-Etched Glaive]|h|r", -- [16]
					"|cff0070dd|Hitem:132243::::::::100:577::::::|h[Fel-Etched Glaive]|h|r", -- [17]
				},
				["averageItemLvl"] = 686.5625,
				["overallAIL"] = 686.5625,
				["lastUpdate"] = 1470810260,
			},
			["Default.Sargeras.Falriçk"] = {
				["Inventory"] = {
					"|cff0070dd|Hitem:138176::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Helm]|h|r", -- [1]
					"|cffa335ee|Hitem:124222::::::::100:254:4:5:3:1798:564:1487:531:::|h[World Ender's Gorget]|h|r", -- [2]
					"|cff0070dd|Hitem:138178::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Spaulders]|h|r", -- [3]
					nil, -- [4]
					"|cff0070dd|Hitem:138179::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Hauberk]|h|r", -- [5]
					"|cff0070dd|Hitem:138172::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Cinch]|h|r", -- [6]
					"|cff0070dd|Hitem:138177::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Leggings]|h|r", -- [7]
					"|cff0070dd|Hitem:138175::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Boots]|h|r", -- [8]
					"|cff0070dd|Hitem:138173::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Bracers]|h|r", -- [9]
					"|cff0070dd|Hitem:138174::::::::100:254:512:9:1:3387:100:::|h[Fel-Chain Grips]|h|r", -- [10]
					"|cff0070dd|Hitem:138162::::::::100:254::14::::|h[Legion Bound Ring]|h|r", -- [11]
					"|cff0070dd|Hitem:138450:5297:::::::100:254:512:11:1:3387:100:::|h[Signet of Stormwind]|h|r", -- [12]
					"|cffa335ee|Hitem:124619::::::::100:254:4::3:605:764:652:529:::|h[Saberblade Emblem]|h|r", -- [13]
					"|cff0070dd|Hitem:128959::::::::100:254::11::::|h[Seal of House Wrynn]|h|r", -- [14]
					"|cff0070dd|Hitem:138188::::::::100:254::14::::|h[Demon Commander's Drape]|h|r", -- [15]
					"|cff0070dd|Hitem:141601::::::::100:254::9:3:3447:1815:1817:::|h[Hellfury Longbow]|h|r", -- [16]
				},
				["averageItemLvl"] = 700.9375,
				["overallAIL"] = 700.9375,
				["lastUpdate"] = 1471240445,
			},
		},
		["Guilds"] = {
			["Default.Sargeras.One Hundred Proof"] = {
				["Members"] = {
					["Cptnwaffles"] = {
						["averageItemLvl"] = 652,
					},
					["Dendrixia"] = {
						["averageItemLvl"] = 742,
					},
					["Falriçk"] = {
						["averageItemLvl"] = 699,
					},
					["Zaeck"] = {
						["averageItemLvl"] = 11,
					},
					["Boqq"] = {
						["averageItemLvl"] = 702,
					},
					["Adrelliar"] = {
						["averageItemLvl"] = 684,
					},
					["Revbill"] = {
						["averageItemLvl"] = 640,
					},
					["Bryin"] = {
						["averageItemLvl"] = 706,
					},
					["Kelnig"] = {
						["averageItemLvl"] = 56,
					},
					["Xuvius"] = {
						["averageItemLvl"] = 702,
					},
					["Falridan"] = {
						["averageItemLvl"] = 686,
					},
					["Gandawf"] = {
						["averageItemLvl"] = 483,
					},
				},
			},
			["Default.Sargeras.TheLegion"] = {
				["Members"] = {
					["Saebera"] = {
						["averageItemLvl"] = 683,
					},
					["Falriçk"] = {
						["averageItemLvl"] = 696,
					},
					["Cronal"] = {
						["averageItemLvl"] = 655,
					},
					["Leafyice"] = {
						["averageItemLvl"] = 690,
					},
					["Muffet"] = {
						["averageItemLvl"] = 700,
					},
					["Shavied"] = {
						["averageItemLvl"] = 703,
					},
					["Crazytown"] = {
						["averageItemLvl"] = 692,
					},
					["Redbloodemon"] = {
						["averageItemLvl"] = 694,
					},
					["Trinkkah"] = {
						["averageItemLvl"] = 680,
					},
					["Rmd"] = {
						["averageItemLvl"] = 702,
					},
					["Muhree"] = {
						["averageItemLvl"] = 681,
					},
					["Spawntheory"] = {
						["averageItemLvl"] = 700,
					},
					["Nightwachman"] = {
						["averageItemLvl"] = 708,
					},
					["Chrisceni"] = {
						["averageItemLvl"] = 702,
					},
					["Fallenslayer"] = {
						["averageItemLvl"] = 705,
					},
					["Skulldebo"] = {
						["averageItemLvl"] = 696,
					},
					["Legennd"] = {
						["averageItemLvl"] = 706,
					},
					["Crazyfela"] = {
						["averageItemLvl"] = 688,
					},
					["Newfiemonk"] = {
						["averageItemLvl"] = 688,
					},
					["Zartanis"] = {
						["averageItemLvl"] = 220,
					},
					["Laviny"] = {
						["averageItemLvl"] = 641,
					},
					["Tarehnis"] = {
						["averageItemLvl"] = 687,
					},
					["Shadynaga"] = {
						["averageItemLvl"] = 640,
					},
					["Athennae"] = {
						["averageItemLvl"] = 694,
					},
					["Frostyfreez"] = {
						["averageItemLvl"] = 681,
					},
					["Hakeber"] = {
						["averageItemLvl"] = 639,
					},
					["Krÿsis"] = {
						["averageItemLvl"] = 689,
					},
					["Avversione"] = {
						["averageItemLvl"] = 696,
					},
					["Saitohiraga"] = {
						["averageItemLvl"] = 673,
					},
					["Samoonfana"] = {
						["averageItemLvl"] = 696,
					},
					["Blindfatty"] = {
						["averageItemLvl"] = 686,
					},
					["Duk"] = {
						["averageItemLvl"] = 706,
					},
					["Hezaal"] = {
						["averageItemLvl"] = 699,
					},
					["Velah"] = {
						["averageItemLvl"] = 697,
					},
				},
			},
			["Default.Sargeras.Tainted Crusaders"] = {
				["Members"] = {
					["Berke"] = {
						["averageItemLvl"] = 691,
					},
					["Varyen"] = {
						["averageItemLvl"] = 28,
					},
					["Mílfhuntér"] = {
						["averageItemLvl"] = 669,
					},
				},
			},
		},
		["Reference"] = {
			["AppearancesCounters"] = {
				["HUNTER"] = {
					"62/354", -- [1]
					"56/335", -- [2]
					"159/730", -- [3]
					"66/396", -- [4]
					"2/82", -- [5]
					"5/76", -- [6]
					"50/337", -- [7]
					"61/378", -- [8]
					"53/348", -- [9]
					"67/383", -- [10]
					"51/378", -- [11]
					nil, -- [12]
					"36/294", -- [13]
					"54/441", -- [14]
					nil, -- [15]
					"79/498", -- [16]
					"22/143", -- [17]
					nil, -- [18]
					"38/302", -- [19]
					"30/224", -- [20]
					"31/237", -- [21]
					nil, -- [22]
					"100/523", -- [23]
					"24/180", -- [24]
					"27/188", -- [25]
					"35/180", -- [26]
					"20/108", -- [27]
				},
				["PALADIN"] = {
					"15/340", -- [1]
					"15/339", -- [2]
					"154/733", -- [3]
					"13/380", -- [4]
					"2/82", -- [5]
					"5/76", -- [6]
					"6/355", -- [7]
					"9/354", -- [8]
					"11/366", -- [9]
					"18/351", -- [10]
					"10/391", -- [11]
					[24] = "23/180",
					[13] = "36/293",
					[14] = "53/443",
					[15] = "13/448",
					[18] = "18/450",
					[19] = "38/302",
					[20] = "30/224",
					[21] = "29/237",
					[22] = "4/221",
				},
				["DEMONHUNTER"] = {
					"12/325", -- [1]
					"10/307", -- [2]
					"146/726", -- [3]
					"17/377", -- [4]
					"2/82", -- [5]
					"5/73", -- [6]
					"6/317", -- [7]
					"13/347", -- [8]
					"10/325", -- [9]
					"13/332", -- [10]
					"9/355", -- [11]
					nil, -- [12]
					"29/293", -- [13]
					"49/441", -- [14]
					[19] = "33/302",
					[28] = "2/3",
					[17] = "19/143",
					[16] = "71/498",
				},
			},
		},
	},
}
